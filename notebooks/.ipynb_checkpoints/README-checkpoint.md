# hipFG example notebooks
hipFG normalizes functional genomics data. This means that free-form BED/TSV/txt files are processed into standard BED formats. In this output the column order is standardized, some missing fields are calculated/imputed, and additional corrections may be included for specific data types (e.g., QTLs). This requires two inputs from the user: the File Config and MDT.
  
* **File Config** (txt): maps input columns to fields in the standard output. Also describes other attributes of the input data (e.g., does it have a header row?)
* Minimal descriptor table (**MDT**) (tsv): A table describing for each input, at minimum, its path, output directories, and genome build. This is also used to describe biological, source, and assay information for a standard and complete output metadata.
  
Use these notebooks to:
* Practice constructing hipFG inputs for usage
* Prepare hipFG inputs for your own data

## Before running these notebooks
Follow the local install instructions in the documentation here. This notebook should be executed on a Linux machine, MacOS, or on WSL on Windows. Running the Bash kernal is difficult on a normal Windows OS, so using WSL is highly recommended.
  
## Notebook links
Download the Jupyter notebooks using the links above or the following. A notebook is provided for each of the major input data types to hipFG: intervals, interactions, and QTLs. Additionally, there is a notebook explaining some of the more advanced uses of hipFG, such as splitting input columns by a specified delimiter. 
* [intervals](https://bitbucket.org/wanglab-upenn/hipfg/raw/508c66e3bd1e29981a949fcddace7a1a9dee15c1/notebooks/hipFG_on_intervals.ipynb)
* [QTLs](https://bitbucket.org/wanglab-upenn/hipfg/raw/508c66e3bd1e29981a949fcddace7a1a9dee15c1/notebooks/hipFG_on_QTLs.ipynb)
* [interactions](https://bitbucket.org/wanglab-upenn/hipfg/raw/508c66e3bd1e29981a949fcddace7a1a9dee15c1/notebooks/hipFG_on_interactions.ipynb)
* [Advanced Usage](https://bitbucket.org/wanglab-upenn/hipfg/raw/508c66e3bd1e29981a949fcddace7a1a9dee15c1/notebooks/advanced_usage.ipynb)
