# hipFG example notebooks
hipFG normalizes functional genomics data. This means that free-form BED/TSV/txt files are processed into standard BED formats. In this output the column order is standardized, some missing fields are calculated/imputed, and additional corrections may be included for specific data types (e.g., QTLs). This requires two inputs from the user: the File Config and MDT.
  
* **File Config** (txt): maps input columns to fields in the standard output. Also describes other attributes of the input data (e.g., does it have a header row?)  
* Minimal descriptor table (**MDT**) (tsv): A table describing for each input, at minimum, its path, output directories, and genome build. This is also used to describe biological, source, and assay information for a standard and complete output metadata.  
  
Use these notebooks to:  
  
* Practice constructing hipFG inputs for usage  
* Prepare hipFG inputs for your own data  

## Before running these notebooks
Follow the local install instructions in the documentation here. This notebook should be executed on a Linux machine, MacOS, or on Windows Subsystem for Linux (WSL) on Windows. Running the Bash kernal is difficult on a normal Windows OS, so using WSL is highly recommended.  
  
If you do not have Jupyter notebooks on your system already, follow the Jupyter install instructions [here](https://jupyter.org/install). Also, the Bash kernal will need to be installed. One such kernel is available [here](https://github.com/takluyver/bash_kernel).  
  
Once these are installed and working, execute this
```
jupyter notebook
``` 
and navigate to the `hipFG/notebook` directory via the Jupyter browser. You may need to copy the link provided in the jupyter output into your internet browser to view the notebook e.g.:
```
    To access the server, open this file in a browser:
        file:///home/jcifello/.local/share/jupyter/runtime/jpserver-75-open.html
    Or copy and paste one of these URLs:
        http://localhost:8888/tree?token=e5f06050e55f6f19c43eb5e2850f52985c78cb608a4ade52
        http://127.0.0.1:8888/tree?token=e5f06050e55f6f19c43eb5e2850f52985c78cb608a4ade52
```
The notebooks should then be ready to run.  
  
## Notebook links
Download the Jupyter notebooks using the links above or the following. A notebook is provided for each of the major input data types to hipFG: intervals, interactions, and QTLs. Additionally, there is a notebook explaining some of the more advanced uses of hipFG, such as splitting input columns by a specified delimiter.  
  
* [Intervals Demo Notebook](https://tf.lisanwanglab.org/GADB/download/hipFG/jupyter_notebooks/hipFG_on_intervals.ipynb)  
* [eQTLs Demo Notebook](https://tf.lisanwanglab.org/GADB/download/hipFG/jupyter_notebooks/hipFG_on_eQTLs.ipynb)  
* [Interactions Demo Notebook](https://tf.lisanwanglab.org/GADB/download/hipFG/jupyter_notebooks/hipFG_on_interactions.ipynb)  
* [Advanced Usage](https://tf.lisanwanglab.org/GADB/download/hipFG/jupyter_notebooks/hipFG_advanced_usage.ipynb)  
