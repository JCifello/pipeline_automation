chrom=variant_id-split1
chromStart=variant_id-split2
chromEnd=
pval=pval_nominal
ref=variant_id-split3
alt=variant_id-split4
target=phenotype_id
beta_non_ref=slope
beta_se_non_ref=slope_se
FDR=
tested_allele=variant_id-split4
other_allele=variant_id-split3

SPLIT_CHARACTERS_1=_
SPLIT_CHARACTERS_2=:

SEP_BY_SIGNIF=True
HAS_HEADER=True
IS_BASE_ONE=True
LOOKUP_COL=phenotype_id-split5
LOOKUP_TYPE=ensembl_id_no_ver

MULTIPLE_VARIANT_TYPES=True

TARGET_HAS_SPAN=True

LOOKUP_TARGET_SPAN=false
TARGET_CHROM=phenotype_id-split1
TARGET_START=phenotype_id-split2
TARGET_END=phenotype_id-split3

