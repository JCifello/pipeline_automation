{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f123f475-f4c6-4ffb-bbf5-e0c5af8a063b",
   "metadata": {},
   "source": [
    "## Table of Contents\n",
    "* [Constructing the File Config](#constructing-the-file-config)\n",
    "* [Constructing the MDT](#constructing-the-mdt)\n",
    "* [Calling hipFG - script generation](#calling-hipfg-script-generation)\n",
    "* [Calling hipFG - script execution](#calling-hipfg-script-execution)\n",
    "* [Reviewing hipFG outputs](#reviewing-hipfg-outputs)\n",
    "* [FAQ/issues](#faq)\n",
    "* [File Config templates](#templates)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8dab927e-afa3-4966-bc63-414b0160acb2",
   "metadata": {},
   "source": [
    "## eQTL File Config\n",
    "The File Config has two sections: input field definitions and the file descriptor fields.  \n",
    "\n",
    "These are described here. To practice filling out these inputs, continue to the \"**Usage Example**\" sections.\n",
    "### File Config - input fields\n",
    "To create a File Config for QTLs, assign input columns to the following fields:\n",
    "```\n",
    "chrom=\n",
    "chromStart=\n",
    "pval=\n",
    "target=\n",
    "beta_non_ref=\n",
    "beta_se_non_ref=\n",
    "tested_allele=\n",
    "other_allele=\n",
    "ref=\n",
    "alt=\n",
    "FDR=\n",
    "```\n",
    "Note some exceptions:\n",
    "* Use `ref` for the alelle that always matches the genome reference. If neither allele matches consistently, use `A1` and `A2` instead of `ref` and `alt`.\n",
    "* If `beta_non_ref` or `beta_se_non_ref` is not available, replace it with `z_score_non_ref`. Two of these three must be present except in special cases (see hipFG advanced usage -- Hard-coded columns).\n",
    "\n",
    "Some additional fields may be assigned:\n",
    "* `EAF`: effect allele frequency.\n",
    "* `ac`: alternate allele count. If `an` is also defined, set `CALC_AF=True` to calculate EAF on-the-fly.\n",
    "* `an`: allele number\n",
    "\n",
    "### File Config - file descriptor fields\n",
    "In addition to the always required file descriptors `HAS_HEADER` and `IS_ONE_BASED`, QTL runs require the following to be specified:\n",
    "* `LOOKUP_COL`: The input field describing the target to look up in GENCODE for finding ensembl_target_id, gene symbol, and gene coordinates.\n",
    "* `LOOKUP_TYPE`: One of the following that best describes the QTL target: `hgnc`, `ensembl_id_no_ver`, `ensembl_gene_id`, `symbol`, `refseq_accession`, `ucsc_id`, `uniprot_id`. Note `ensemble_id_no_ver` is useful for ensembl_ids with outdated or missing version numbers.\n",
    "* `MULTIPLE_VARIANT_TYPES`: True if the QTLs are both SNPs and INDELs, or False if only one.\n",
    "\n",
    "There are a number of optional fields to provide context:\n",
    "* `CALC_FDR`: True to calculate the false-discovery rate from the input `pval`.\n",
    "* `CALC_AF`: Calculate the frequency of the effect allele using `ac` and `an` defined above.\n",
    "* `SEP_BY_SIGNIF`: If true, also output filtered QTLs with FDR < 0.05 if available.\n",
    "\n",
    "Setting `LOOKUP_COL` and `TARGET_CHROM` allows hipFG to run the different types of QTLs:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e9b92455-8aad-43f5-aefb-6802d3d62265",
   "metadata": {},
   "source": [
    "|QTL type|target|example target|LOOKUP_TYPE|Set TARGET_CHROM, TARGET_START?|\n",
    "|---|---|---|---|---|\n",
    "|eQTL|gene|ENSG00000015171|ensembl_gene_id, symbol, or ensembl_id_no_ver|No|\n",
    "|sQTL|splicing segments|chr1:497299:498399:clu_51878:ENSG00000237094.11|ensembl_gene_id or ensembl_id_no_ver|Yes|\n",
    "|pQTL|protein|P0C0L5|uniprot_id|No|\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a406fc7a-ab8c-4dd2-b9f0-731d22ada396",
   "metadata": {},
   "source": [
    "For examples of File Configs for each QTL-type, see examples provided at the end of this Notebook.  \n",
    "\n",
    "hipFG will support QTLs targeting epigenetic markers like methylation, but this usage is in development. Use the sQTL File Config for reference, but leave `LOOKUP_COL` blank and set `LOOKUP_TARGET_SPAN=True`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "841f1ff3-d62b-43bc-874d-640208eb62eb",
   "metadata": {},
   "source": [
    "# hipFG Input Examples <a class=\"anchor\" id=\"hipfg-input-examples\"></a>\n",
    "## Constructing the File Config <a class=\"anchor\" id=\"constructing-the-file-config\"></a>\n",
    "The following eQTL example illustrates how to complete the File Config.  \n",
    "\n",
    "Use this process to execute hipFG on the example data or your own inputs.\n",
    "\n",
    "First, navigate to this notebook's directory and print the input data (**update the `cd` path with your own path to this notebook folder**):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3d48af42-a3ce-4e74-a1d6-8db9c8b1d878",
   "metadata": {},
   "outputs": [],
   "source": [
    "cd hipFG/notebooks\n",
    "zcat inputs/eqtl_example.txt.gz | head "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c4cf0aaa-9ae0-4cab-b7b1-95e2f4710f8e",
   "metadata": {},
   "source": [
    "While some of these fields are relevant to the QTL normalization, some are not needed or unclear.  \n",
    "\n",
    "### Filling in the File Config  \n",
    "Fill out the partial File Config below to map the input fields of the example above to the standard hipFG columns. For example, map the standard field `ref` to the best-corresponding input column, which is in this case also \"ref\".  \n",
    "\n",
    "A pre-filled version is available for reference below.  \n",
    "  \n",
    "Assume that the `alt` allele is the tested allele."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "3a3dcb20-926f-4cd4-9d73-21e1fc1ca5e0",
   "metadata": {},
   "outputs": [],
   "source": [
    "echo \"chrom=chromosome\n",
    "chromStart=\n",
    "pval=\n",
    "target=\n",
    "beta_non_ref=\n",
    "beta_se_non_ref=\n",
    "tested_allele=\n",
    "other_allele=\n",
    "ref=\n",
    "alt=\n",
    "FDR=\n",
    "\n",
    "\" > new_file_config_input_fields.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4b658249-f594-4b11-aae3-68b666439031",
   "metadata": {},
   "source": [
    "Now complete the second section, which describes some attributes of the input. Use the table above to set `LOOKUP_TYPE` for these eQTLs.  \n",
    "`LOOKUP_COL` can be the same as your `target` input field above.  \n",
    "`IS_BASE_ONE` can be determined by searching one of the input rsids on dbSNP and comparing the input position.\n",
    "Check for the correct answer to `MULTIPLE_VARIANT_TYPES` by using `zless` or extending the `zcat` head above. If there are both INDELS and SNPs, set this to `True`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "d4b3c14c-f73a-4d55-8e7a-2b08e2f3bc2c",
   "metadata": {},
   "outputs": [],
   "source": [
    "echo \"HAS_HEADER=\n",
    "IS_BASE_ONE=\n",
    "LOOKUP_COL=\n",
    "LOOKUP_TYPE=\n",
    "MULTIPLE_VARIANT_TYPES=\n",
    "\" > new_file_config_file_descriptors.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "31e9f268-99cf-4bdf-8f81-69d7434a5d0d",
   "metadata": {},
   "source": [
    "Combine these into `new_file_config.txt`. This is your File Config."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "414d42cd-1743-4b60-b313-3ffa6a49439c",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat new_file_config_input_fields.txt new_file_config_file_descriptors.txt > new_file_config.txt\n",
    "cat new_file_config.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0e5aeaa5-4613-4d10-a207-f9731cdec893",
   "metadata": {},
   "source": [
    "**Stuck or finished?** Check your File Config sections against the premade version for reference:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "30545464-3188-467f-a28c-9af5a767d84f",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat premade_examples/qtls/premade_file_config_combined.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8435cbdc-de16-4317-a876-8ba1e6cf0568",
   "metadata": {},
   "source": [
    "## Constructing the MDT <a class=\"anchor\" id=\"constructing-the-mdt\"></a>\n",
    "The minimal descriptor table (MDT) provides hipFG with biological, source, and file information. \n",
    "This table typically requires 20 fields to provide a full metadata annotation, but a simplified version can be used for this example. \n",
    "\n",
    "Replacing the second row of the TSV with the specified values. Here are how some fields can be filled:\n",
    "* Set genome build to hg38\n",
    "* Create new directories of your choice for output and temp\n",
    "* Use the custom File Config above for File Config\n",
    "* The FileID is an arbitrary unique identifier. Avoid spaces\n",
    "* Set Init Config to `./hipFG.ini`. You may need to edit this file to correct filepaths (e.g., `python` is assumed to be `/usr/bin/python3/`)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "738e3897-c1b3-489d-8cc9-623b058b1c97",
   "metadata": {},
   "outputs": [],
   "source": [
    "echo -e \"FileID\\tInput File\\tOutput Directory\\tTemp Directory\\tFile Config\\tGenome build\\tInit Config\n",
    "_fileid_\\t_in_file_\\t_out_dir_\\t_temp_dir_\\t_file_config_\\t_genome_build_\\t_init_config_\" > mdt_simple.tsv\n",
    "cat mdt_simple.tsv"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8706d278-c0b5-47f6-b061-c6bbeebf6b9f",
   "metadata": {},
   "source": [
    "**Stuck or finished?** Check your MDT against a pre-made one here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "7244ec1b-5eb6-4cfb-bfd0-5b6740d31b9c",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat premade_examples/qtls/premade_mdt_simple.tsv"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dfbd549b-689e-405d-82c1-e9252eceacbe",
   "metadata": {},
   "source": [
    "To run hipFG on the premade File Config: assign it in the premade MDT. Otherwise, let it point to your `new_file_config.txt`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2d94b1be-1058-4d77-8038-76bc3f1170cf",
   "metadata": {},
   "outputs": [],
   "source": [
    "sed -i 's/new_file_config/premade_file_config_combined/' premade_mdt_simple.tsv"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a864941a-837d-45dd-8cf8-908cdd9d3af2",
   "metadata": {},
   "source": [
    "## Calling hipFG - script generation <a class=\"anchor\" id=\"calling-hipfg-script-generation\"></a>\n",
    "hipFG is run with a single Bash script that points to the MDT constructed above.\n",
    "Enable the option to use a simplified MDT and stop hipFG after script generation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "79d84d74-8ebc-4fc2-be6a-2279d2fb9075",
   "metadata": {},
   "outputs": [],
   "source": [
    "bash ../hipFG.sh --mdt premade_examples/qtls/premade_mdt_simple.tsv \\\n",
    "    --run_now false \\\n",
    "    --simple_mdt true"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d0f1c2d9-b0f7-4843-9247-46ecccab5a3f",
   "metadata": {},
   "source": [
    "Running the Bash script `hipFG.sh` here yields two important Bash scripts in the temporary directory:  \n",
    "* `auto_pipeline_<fileid>.sh`: A Bash script to normalize a single input file. This will save output data (and metadata) but will not Giggle index the outputs nor combine output metadatas. This is useful for testing and troubleshooting individual steps of the constructed pipeline.\n",
    "* `hipFG_group_command.sh`: This command executes the `auto_pipeline` scripts sequentially or in parallel and Giggle indexes the results. The output metadata, if applicable, are combined in the output directory in a single file called `master_project_metadata.tsv`.\n",
    "\n",
    "To carry out the normalization itself, these scripts will be executed in the following section.  \n",
    "  \n",
    "**Stuck or finished?** Replace `mdt_simple.tsv` above with `premade_mdt_simple.tsv` to demonstrate the expected output."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a2eefb8c-3895-4803-ae46-de4103fe66b6",
   "metadata": {},
   "source": [
    "## Calling hipFG - script execution <a class=\"anchor\" id=\"calling-hipfg-script-execution\"></a>\n",
    "While this example only has one input file, we can execute `hipFG_group_command.sh` as instructed in the output above to carry out all steps of hipFG normalization.  \n",
    "Execute the following to run normalization:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "66a1de0e-73a5-4808-91ec-8f83d5c63e20",
   "metadata": {},
   "outputs": [],
   "source": [
    "bash temp_dir/hipFG_group_command.sh"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ff5b6e6c-d00c-4393-ab04-c632f99394ef",
   "metadata": {},
   "source": [
    "The normalized eQTL outputs can now be viewed:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e8ea2b06-257e-4691-99b5-589216a4bbd5",
   "metadata": {},
   "outputs": [],
   "source": [
    "ls out_dir/SNP/define_assay/bed3plus17_qtl/hg38/formatted_output_my_id_SNP.bed.gz\n",
    "zcat out_dir/SNP/define_assay/bed3plus17_qtl/hg38/formatted_output_my_id_SNP.bed.gz | wc -l\n",
    "zcat out_dir/SNP/define_assay/bed3plus17_qtl/hg38/formatted_output_my_id_SNP.bed.gz | head -n 5"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "58e3d2f4-17a2-416d-8c53-4873fa040d28",
   "metadata": {},
   "source": [
    "Note some attributes of the output:\n",
    "* new gene symbols have been provided via the ensembl_id mapping\n",
    "* target strands have been determined\n",
    "* output file is now searchable via bedtools etc..\n",
    "* the output directory contains a **Giggle index** for rapid searchability:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b133b04d-5b3b-44cf-ae0d-041951ca8193",
   "metadata": {},
   "outputs": [],
   "source": [
    "## setting variables to access giggle from Init Config\n",
    "hipFG_root=../\n",
    "declare -x hipFG_root\n",
    "source hipFG.ini\n",
    "\n",
    "## use -l option to LIST files in giggle index\n",
    "$Giggle search -i out_dir/SNP/define_assay/bed3plus17_qtl/hg38/giggle_index/ -l"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4c9990cb-a543-474c-9ec2-e94ada8ea0fc",
   "metadata": {},
   "source": [
    "We can print this as a table to show what hte output looks like"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e1663d88-882c-4d9e-988e-8709f3065f3e",
   "metadata": {},
   "source": [
    "## Reviewing hipFG output <a class=\"header\" id=\"reviewing-hipfg-output\"></a>\n",
    "hipFG eQTL outputs follow the bed 3+17 qtl format. This has the following fields:\n",
    "```\n",
    "chrom;chromStart;chromEnd;variant_id;pval;target_strand;ref;alt;target_gene_symbol;target_ensembl_id;target;z_score_non_ref;beta_non_ref;beta_se_non_ref;FDR;non_ref_af;qtl_dist_to_target;QC_info;target_info;user_input\n",
    "```\n",
    "So the output can be viewed as follows:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b3652eac-f3cd-4e91-98a9-389c9ef30cf3",
   "metadata": {},
   "outputs": [],
   "source": [
    "zcat out_dir/SNP/define_assay/bed3plus17_qtl/hg38/formatted_output_my_id_SNP.bed.gz | head -n2 | awk 'BEGIN{FS=\"\\t\"}NR==1{\n",
    "for (i=1;i<=NF;i++) {\n",
    "    col_names[i]=$i\n",
    "}\n",
    "}\n",
    "NR==2{\n",
    "for (i=1;i<=NF;i++) {\n",
    "    print \"----\"\n",
    "    print i\" \"col_names[i]\": \"$i\n",
    "}\n",
    "}'"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "fff71fef-b538-4131-b6c5-a4161a3df382",
   "metadata": {},
   "source": [
    "Note the fields `QC_info`, `target_info`, and `user_info` which are semicolon-delimited:\n",
    "* QC_info: after joining the input with dbSNP, hipFG saves here any corrections that took place and why they were made\n",
    "* target_info: the `LOOKUP_COL` field is searched using the provided `LOOKUP_TYPE` to obtain the target strand, coordinated and gene symbol.\n",
    "* user_info: any remaining columns provided by the user but not used in hipFG are saved here\n",
    "\n",
    "With this standardized output format, QTLs are directly searchable/indexible by tools such as tabix, bedtools, and Giggle. Additionally, the alleles are made clear with `ref` and `alt` indicators, avoiding ambiguous A1/A2 notation. "
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f4e18ba5-3b08-4411-a97a-c10c857b976e",
   "metadata": {},
   "source": [
    "## FAQ <a class=\"header\" id=\"faq\"></a>\n",
    "* I have installed pandas but the cell above indicates \"No module named `pandas`\". How can this be resolved?  \n",
    "    When running `pip install`, make sure it points to the same python as the one indicated in `hipFG.ini`. Find out your default python using `which python` and set python_use in hipFG.ini to this. Alternatively, choose the python interpreter to update using `pip --python <your/python3> install pandas statsmodels`."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6c9d7354-235d-44b6-bf42-b806963954bc",
   "metadata": {},
   "source": [
    "## File Config templates / examples <a class=\"header\" id=\"templates\"></a>\n",
    "For convenience, File Config templates are saved here for expression, protein, and sQTLs. The notes in the first section indicate when deviations from these templates should be made.\n",
    "\n",
    "These Configs have been used for previous integrations, so they are already filled out. You can copy these and edit the field definitions to apply to other datasets.\n",
    "\n",
    "**eQTLs**\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2d6b200b-7ddb-48b0-b408-31d16cbb559a",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat premade_examples/config_eqtls.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8f8c07a7-16b4-46c1-8139-aed7ecb90eef",
   "metadata": {},
   "source": [
    "**pQTLs**  \n",
    "This File Config is the same as for eQTLs, except the `LOOKUP_TYPE` should be `uniprot_id`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "2aa8ac33-ae45-4e7d-87e0-b687bdee8e32",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat premade_examples/config_pqtls.txt"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f0db5bc-f851-4951-8115-2941355c71fa",
   "metadata": {},
   "source": [
    "**sQTLs**  \n",
    "sQTL file configs are slightly longer and often contain `SPLIT_CHARACTERS_#` fields.\n",
    "Note that `TARGET_CHROM`, `TARGET_START`, and `TARGET_END` are defined for sQTLs.\n",
    "`TARGET_HAS_SPAN` indicates whether the target already has a genomic region, i.e. `chr1:100-200:clu_001:ENSG00001`. If `False`, the hipFG will join the target with genomic position information."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "051cf8fe-d808-4bdd-b86b-989ddbca60e0",
   "metadata": {},
   "outputs": [],
   "source": [
    "cat premade_examples/config_sqtls.txt"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Bash",
   "language": "bash",
   "name": "bash"
  },
  "language_info": {
   "codemirror_mode": "shell",
   "file_extension": ".sh",
   "mimetype": "text/x-sh",
   "name": "bash"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
