#!/bin/bash

hipFG_root=$(cd "$(dirname "$0")/../.." && pwd)
bash $hipFG_root/hipFG.sh --mdt mdt.tsv --run_now true

# Calculate the md5 checksum based on availability of md5 or md5sum
calculate_md5() {
    local file=$1
    local md5_command

    if command -v md5 >/dev/null 2>&1; then
        md5_command="md5 -q"
    elif command -v md5sum >/dev/null 2>&1; then
        md5_command="md5sum"
    else
        echo "Error: Neither md5 nor md5sum command is available." >&2
        exit 1
    fi

    gunzip -c "$file" | $md5_command | awk '{print $1}'
}

out_files_dir="out_files"
expected_output_dir="expected_output/out_files"

echo ""

# Find the matching files and compare their md5 checksums
for file_path in $out_files_dir/ChIP-seq/narrowPeak/hg38/*gz; do
    base_name=$(basename "$file_path")
    expected_file_path="$expected_output_dir/${file_path#$out_files_dir/}"
    if [[ -f "$expected_file_path" ]]; then
		echo "checking $expected_file_path"
        # Calculate md5 checksums
        md5_out_files=$(calculate_md5 "$file_path")
        md5_expected_output=$(calculate_md5 "$expected_file_path")

        # Compare the checksums
        if [[ "$md5_out_files" == "$md5_expected_output" ]]; then
			continue
        else
			echo "ERROR: Run did not match expected output."
			exit 1
        fi
    fi
done

echo "Success! All files match."

