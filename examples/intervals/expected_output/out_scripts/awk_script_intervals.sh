#!/bin/script
LC_ALL=C awk 'BEGIN{FS="\t";OFS="\t"}{chrom=$1; chromStart=$2-1; chromEnd=$3; name=$4; score=$5; strand=$6; signalValue=$7; pValue=$8; qValue=$9; peak=$10
print chrom,chromStart,chromEnd,name,score,strand,signalValue,pValue,qValue,peak}' $1 > $2
