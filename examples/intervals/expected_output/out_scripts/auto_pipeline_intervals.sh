#!/bin/bash
init_config=/mnt/data/jeffrey/FILER/scripts/pipeline_automation/hipFG.ini
hipFG_root=/mnt/data/jeffrey/FILER/scripts/pipeline_automation    
declare -x hipFG_root
source $init_config    
genome_build=hg38
output_type="peaks"

if [ "$output_type" = "QTLs" ]; then
    if [ $genome_build = "hg19" ]; then
        reference_genome="$reference_genome_hg19"
        dbSNP_ref_file="$dbSNP_hg19"
        hgnc_ref_file="$hgnc_gencode_hg19"
    elif [ $genome_build = "hg38" ]; then
        reference_genome="$reference_genome_hg38"
        dbSNP_ref_file="$dbSNP_hg38"
        hgnc_ref_file="$hgnc_gencode_hg38"
    fi
fi 
 
declare -x python_use
declare -x tabix_use
declare -x Giggle

tag=intervals
output_dir=/mnt/data/jeffrey/FILER/scripts/pipeline_automation/examples/intervals/out_files
temp_dir=/mnt/data/jeffrey/FILER/scripts/pipeline_automation/examples/intervals/out_scripts
scripts_dir=$hipFG_root/src
MAX_RAM=${MAX_RAM:-5000}

declare -x scripts_dir
declare -x MAX_RAM
echo -e "Running sample $tag with hipFG version $(cat $hipFG_root/VERSION).\n"
start_time=$(date +%s)
call_script () {
    # before running the script, record the time and 
    in_script=$1
    date
    echo All arguments: "$@"
	
    prestep_time=$(date +%s)
	day_dur=$(echo "($prestep_time-$start_time)/(3600*24)" | bc) # automatically rounds down
    hr_dur=$(echo "(($prestep_time-$start_time)/3600)%24" | bc | xargs printf "%02d")
    min_dur=$(echo "(($prestep_time-$start_time)/60)%60" | bc | xargs printf "%02d")
    sec_dur=$(echo "(($prestep_time-$start_time))%60" | bc | xargs printf "%02d")
    echo PRE-STEP TOTAL ELAPSED TIME: $day_dur-$hr_dur:$min_dur:$sec_dur

	## print description for this script
	grep SCRIPT_DESCRIPTION $in_script
	
    ## run the script with args as intended.
    bash "$@"
    
    post_step_time=$(date +%s)
	day_dur=$(echo "($post_step_time-$prestep_time)/(3600*24)" | bc) # automatically rounds down
    hr_dur=$(echo "(($post_step_time-$prestep_time)/3600)%24" | bc | xargs printf "%02d")
    min_dur=$(echo "(($post_step_time-$prestep_time)/60)%60" | bc | xargs printf "%02d")
    sec_dur=$(echo "(($post_step_time-$prestep_time))%60" | bc | xargs printf "%02d")
    echo THIS STEP ELAPSED TIME: $day_dur-$hr_dur:$min_dur:$sec_dur
    
	day_dur=$(echo "($post_step_time-$start_time)/(3600*24)" | bc) # automatically rounds down
    hr_dur=$(echo "(($post_step_time-$start_time)/3600)%24" | bc | xargs printf "%02d")
    min_dur=$(echo "(($post_step_time-$start_time)/60)%60" | bc | xargs printf "%02d")
    sec_dur=$(echo "(($post_step_time-$start_time))%60" | bc | xargs printf "%02d")
    echo POST-STEP TOTAL ELAPSED TIME: $day_dur-$hr_dur:$min_dur:$sec_dur
    
    echo
    echo
}
call_script $scripts_dir/unzip_input.sh /mnt/data/jeffrey/FILER/scripts/pipeline_automation/examples/intervals/inputs/interval_data.tsv $tag $temp_dir True

call_script $temp_dir/awk_script_${tag}.sh $temp_dir/.tmp_unzipped_input_${tag}.bed $temp_dir/formatted_output_${tag}.bed
rm $temp_dir/.tmp_unzipped_input_${tag}.bed


call_script $scripts_dir/wrapup_scripts/final_sort.sh $temp_dir/formatted_output_${tag}.bed $temp_dir 6 $temp_dir/formatted_output_${tag}.bed "chrom+chromStart+chromEnd+name+score+strand+signalValue+pValue+qValue+peak"

call_script $scripts_dir/wrapup_scripts/gzip_final.sh $temp_dir/formatted_output_${tag}.bed $temp_dir/formatted_output_${tag}.bed.gz

call_script $scripts_dir/wrapup_scripts/tabix_on_zipped.sh $temp_dir/formatted_output_${tag}.bed.gz

rm $temp_dir/formatted_output_${tag}.bed

mkdir -p $output_dir
mkdir -p $output_dir/ChIP-seq
mkdir -p $output_dir/ChIP-seq/narrowPeak
mkdir -p $output_dir/ChIP-seq/narrowPeak/hg38

mv $temp_dir/formatted_output_${tag}.bed.gz.tbi $output_dir/ChIP-seq/narrowPeak/hg38
mv $temp_dir/formatted_output_${tag}.bed.gz $output_dir/ChIP-seq/narrowPeak/hg38
bash $scripts_dir/wrapup_scripts/get_metadata_row.sh "$output_dir/ChIP-seq/narrowPeak/hg38/formatted_output_intervals.bed.gz" /mnt/data/jeffrey/FILER/scripts/pipeline_automation/examples/intervals/inputs/interval_data.tsv $tag $temp_dir "$output_dir/ChIP-seq/narrowPeak/hg38" $celltypes_dict $data_categories $scripts_dir "NGFDC" "0000001" "FakeDataConsortium" "peaks" "hg38" "CD8+ alpha-beta T cell" "primary cell" "" "" "" "" "" "ChIP-seq" "narrowPeak" "7/19/2022" "5/19/2022" "fakedataproject.com" "" "Original Cell Type Name=CD8-positive primary T cells;pubmed_id=37162864" "Adult" "false"


current_time=$(date +%s)
elapsed=$(echo "($current_time-$start_time)" | bc)
day_dur=$(echo "($current_time-$start_time)/(3600*24)" | bc) # automatically rounds down
hr_dur=$(echo "(($current_time-$start_time)/3600)%24" | bc | xargs printf "%02d")
min_dur=$(echo "(($current_time-$start_time)/60)%60" | bc | xargs printf "%02d")
sec_dur=$(echo "(($current_time-$start_time))%60" | bc | xargs printf "%02d")

## print the formatted time calculated above
echo ***FINAL ELAPSED TIME: $day_dur-$hr_dur:$min_dur:$sec_dur
