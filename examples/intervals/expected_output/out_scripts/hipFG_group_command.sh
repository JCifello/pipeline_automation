#!/bin/bash
set -e
do_parallel="false"
num_jobs=0
temp_dir=/mnt/data/jeffrey/FILER/scripts/pipeline_automation/examples/intervals/out_scripts
scripts_dir=/mnt/data/jeffrey/FILER/scripts/pipeline_automation/src
hipFG_root=/mnt/data/jeffrey/FILER/scripts/pipeline_automation

## Functions to simulate associative array functionality. 
# Function to access key-value pair value.
get_value() {
  local key=$1
  local array=("${@:2}")
  
  for ((i = 0; i < ${#array[@]}; i += 2)); do
    if [[ "${array[i]}" == "$key" ]]; then
      echo "${array[i+1]}"
      return
    fi
  done
  
  echo "Key not found: $key"
}


# Function to assign a key-value pair
assign_value() {
  local key=$1
  local value=$2
  local array=("${@:3}")
  
  # Check if the key already exists
  for ((i = 0; i < ${#array[@]}; i += 2)); do
    if [[ "${array[i]}" == "$key" ]]; then
      array[i+1]=$value
      echo ${array[@]}
	  return
    fi
  done
  
  # Key not found, add a new key-value pair
  array+=("$key" "$value")
  echo "${array[@]}"
}

declare -x hipFG_root
source /mnt/data/jeffrey/FILER/scripts/pipeline_automation/hipFG.ini # run the initial config
declare -x Giggle

mkdir -p $temp_dir/sample_logs
if [ $do_parallel = "true" ]; then
    parallel -a $temp_dir/command_list.txt --joblog $temp_dir/command_list_out.txt --jobs $num_jobs "{}"
else
    bash $temp_dir/command_list.txt
fi

out_dirs=($(tail -n +2 /mnt/data/jeffrey/FILER/scripts/pipeline_automation/examples/intervals/mdt.tsv | cut -f5 |  sort | uniq))
source_ids=($(tail -n +2 /mnt/data/jeffrey/FILER/scripts/pipeline_automation/examples/intervals/mdt.tsv | cut -f1,14 |  sort | uniq | sed 's/\t/my_long_delimiter/g'))

short_id_counter=()
short_id_mapper=()
id_list=()

for short_id in ${source_ids[@]}; do
	my_fileid=$(echo $short_id | awk 'BEGIN{FS="my_long_delimiter"}{print $1}')
	my_id=$(echo $short_id | awk 'BEGIN{FS="my_long_delimiter"}{print $NF}')
	short_id_mapper=($(assign_value $my_fileid $my_id ${short_id_mapper[@]}))
	short_id_counter=($(assign_value $my_id 1 ${short_id_counter[@]}))
	id_list[${#id_list[@]}+1]=$my_fileid
done

leaf_dirs=()
for out_dir in ${out_dirs[@]}; do
    # print all files in output recursively. Keep if it is a folder with contents. Keep if the lowest level is a genome build. 
    abs_out_dir=$(cd $out_dir && pwd)
    new_leaf_dirs=($(ls -lR $abs_out_dir | grep "^/" | cut -d ':' -f1 | awk 'BEGIN{FS="/"}{if (($NF=="hg19")||($NF=="hg38")){print $0}}'))
    leaf_dirs=(${leaf_dirs[@]} ${new_leaf_dirs[@]})
done

leaf_dirs=($(for i in ${leaf_dirs[@]}; do echo "$i"; done | sort -u))

echo -n "" > $temp_dir/giggle_input_dirs.txt

for out_dir in ${leaf_dirs[@]}; do
	for fileid in ${id_list[@]}; do
		for md_file in $(find $out_dir -type f -name "metadata_row_${fileid}.tsv"); do
			short_id=$(get_value $fileid ${short_id_mapper[@]})
			short_id_num=$(get_value $short_id ${short_id_counter[@]})
			short_id_counter=($(assign_value $short_id $(echo $short_id_num+1 | bc) ${short_id_counter[@]}))
			new_id=$(printf ${short_id}%05d $short_id_num | sed 's/[a-z]/\U&/g')
			combination_out_dir=$(grep "$fileid.*$short_id" /mnt/data/jeffrey/FILER/scripts/pipeline_automation/examples/intervals/mdt.tsv | cut -f5)

			awk 'BEGIN{FS="\t"}{ printf "%s\t","'$new_id'"; for (i=2;i<NF;i++){printf "%s\t",$i}; print $NF}' $md_file >> $combination_out_dir/master_project_metadata.tsv
            rm $md_file
		done
	done
    echo "Will run Giggle indexing of directory: $out_dir"
    echo $out_dir >> $temp_dir/giggle_input_dirs.txt
done

if [ "false" == "true" ]; then
    echo -e "\nRunning in metadata_only mode. Breaking."
    exit
fi

if [ $do_parallel == "true" ]; then
    parallel -a $temp_dir/giggle_input_dirs.txt --joblog $temp_dir/giggle_parallel_out.txt --jobs $num_jobs "bash $scripts_dir/wrapup_scripts/giggle_script.sh {}"
else
    for giggle_dir in $(cat $temp_dir/giggle_input_dirs.txt); do
        bash $scripts_dir/wrapup_scripts/giggle_script.sh $giggle_dir
    done
fi

sample_logs=$(cat $temp_dir/command_list.txt | rev | cut -d ' ' -f 1 | rev)
for i in $sample_logs; do
    awk '{if ($0 ~ /does not map to a known/) { print $0; exit } }' $i
done

echo -e "\nStandardized data outputs saved in:"
for out_dir in ${out_dirs[@]}; do
    echo $out_dir
done | sort -u 

echo ""

