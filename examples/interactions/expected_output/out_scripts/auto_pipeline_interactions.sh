#!/bin/bash
init_config=/mnt/data/jeffrey/FILER/scripts/pipeline_automation/examples/interactions/hipFG_tmp.ini
hipFG_root=/mnt/data/jeffrey/FILER/scripts/pipeline_automation    
declare -x hipFG_root
source $init_config    
genome_build=hg38
output_type="Chromatin Interactions"

if [ "$output_type" = "QTLs" ]; then
    if [ $genome_build = "hg19" ]; then
        reference_genome="$reference_genome_hg19"
        dbSNP_ref_file="$dbSNP_hg19"
        hgnc_ref_file="$hgnc_gencode_hg19"
    elif [ $genome_build = "hg38" ]; then
        reference_genome="$reference_genome_hg38"
        dbSNP_ref_file="$dbSNP_hg38"
        hgnc_ref_file="$hgnc_gencode_hg38"
    fi
fi 
 
declare -x python_use
declare -x tabix_use
declare -x Giggle

tag=interactions
output_dir=/mnt/data/jeffrey/FILER/scripts/pipeline_automation/examples/interactions/out_files
temp_dir=/mnt/data/jeffrey/FILER/scripts/pipeline_automation/examples/interactions/out_scripts
scripts_dir=$hipFG_root/src
MAX_RAM=${MAX_RAM:-5000}

declare -x scripts_dir
declare -x MAX_RAM
echo -e "Running sample $tag with hipFG version $(cat $hipFG_root/VERSION).\n"
start_time=$(date +%s)
call_script () {
    # before running the script, record the time and 
    in_script=$1
    date
    echo All arguments: "$@"
	
    prestep_time=$(date +%s)
	day_dur=$(echo "($prestep_time-$start_time)/(3600*24)" | bc) # automatically rounds down
    hr_dur=$(echo "(($prestep_time-$start_time)/3600)%24" | bc | xargs printf "%02d")
    min_dur=$(echo "(($prestep_time-$start_time)/60)%60" | bc | xargs printf "%02d")
    sec_dur=$(echo "(($prestep_time-$start_time))%60" | bc | xargs printf "%02d")
    echo PRE-STEP TOTAL ELAPSED TIME: $day_dur-$hr_dur:$min_dur:$sec_dur

	## print description for this script
	grep SCRIPT_DESCRIPTION $in_script
	
    ## run the script with args as intended.
    bash "$@"
    
    post_step_time=$(date +%s)
	day_dur=$(echo "($post_step_time-$prestep_time)/(3600*24)" | bc) # automatically rounds down
    hr_dur=$(echo "(($post_step_time-$prestep_time)/3600)%24" | bc | xargs printf "%02d")
    min_dur=$(echo "(($post_step_time-$prestep_time)/60)%60" | bc | xargs printf "%02d")
    sec_dur=$(echo "(($post_step_time-$prestep_time))%60" | bc | xargs printf "%02d")
    echo THIS STEP ELAPSED TIME: $day_dur-$hr_dur:$min_dur:$sec_dur
    
	day_dur=$(echo "($post_step_time-$start_time)/(3600*24)" | bc) # automatically rounds down
    hr_dur=$(echo "(($post_step_time-$start_time)/3600)%24" | bc | xargs printf "%02d")
    min_dur=$(echo "(($post_step_time-$start_time)/60)%60" | bc | xargs printf "%02d")
    sec_dur=$(echo "(($post_step_time-$start_time))%60" | bc | xargs printf "%02d")
    echo POST-STEP TOTAL ELAPSED TIME: $day_dur-$hr_dur:$min_dur:$sec_dur
    
    echo
    echo
}
call_script $scripts_dir/unzip_input.sh /mnt/data/jeffrey/FILER/scripts/pipeline_automation/examples/interactions/inputs/interaction_data.tsv $tag $temp_dir True

call_script $scripts_dir/column_imputing_scripts/complete_interact.sh $temp_dir/.tmp_unzipped_input_${tag}.bed $temp_dir/.tmp_remaining_fields_${tag}.bed "9" "10" "7" "11" "12" "13" "14" "15" "16" "FakeDataConsortium" "CD8+ alpha-beta T cell" "log"

genome_partition_ref=$genome_partition/bed6/hg38/giggle_index/
call_script $scripts_dir/column_imputing_scripts/complete_interact_2.sh $temp_dir/.tmp_remaining_fields_${tag}.bed $temp_dir/.tmp_add_anchor_genes_${tag}.bed $genome_partition_ref $tag 1 2 3 4 5 6
rm $temp_dir/.tmp_unzipped_input_${tag}.bed
rm $temp_dir/.tmp_remaining_fields_${tag}.bed

call_script $temp_dir/awk_script_${tag}.sh $temp_dir/.tmp_add_anchor_genes_${tag}.bed $temp_dir/formatted_output_${tag}.bed
rm $temp_dir/.tmp_add_anchor_genes_${tag}.bed

call_script $scripts_dir/column_imputing_scripts/reformat_interactions.sh $temp_dir/formatted_output_${tag}.bed $temp_dir/formatted_output_${tag}.bed_2
mv $temp_dir/formatted_output_${tag}.bed_2 $temp_dir/formatted_output_${tag}.bed

call_script $scripts_dir/wrapup_scripts/final_sort.sh $temp_dir/formatted_output_${tag}.bed $temp_dir NR $temp_dir/formatted_output_${tag}.bed "anchorChrom+anchorStart+anchorEnd+anchorName+interactionAttr+chrom+chromStart+chromEnd+name+score+value+exp+color+sourceChrom+sourceStart+sourceEnd+sourceName+sourceStrand+targetChrom+targetStart+targetEnd+targetName+targetStrand"

call_script $scripts_dir/wrapup_scripts/gzip_final.sh $temp_dir/formatted_output_${tag}.bed $temp_dir/formatted_output_${tag}.bed.gz

call_script $scripts_dir/wrapup_scripts/tabix_on_zipped.sh $temp_dir/formatted_output_${tag}.bed.gz

rm $temp_dir/formatted_output_${tag}.bed

mkdir -p $output_dir
mkdir -p $output_dir/3C
mkdir -p $output_dir/3C/bed4plus19_interact
mkdir -p $output_dir/3C/bed4plus19_interact/hg38

mv $temp_dir/formatted_output_${tag}.bed.gz.tbi $output_dir/3C/bed4plus19_interact/hg38
mv $temp_dir/formatted_output_${tag}.bed.gz $output_dir/3C/bed4plus19_interact/hg38
bash $scripts_dir/wrapup_scripts/get_metadata_row.sh "$output_dir/3C/bed4plus19_interact/hg38/formatted_output_interactions.bed.gz" /mnt/data/jeffrey/FILER/scripts/pipeline_automation/examples/interactions/inputs/interaction_data.tsv $tag $temp_dir "$output_dir/3C/bed4plus19_interact/hg38" $celltypes_dict $data_categories $scripts_dir "NGFDC" "0000001" "FakeDataConsortium" "Chromatin Interactions" "hg38" "CD8+ alpha-beta T cell" "primary cell" "" "" "" "" "" "3C" "bed4+19 interact" "7/19/2022" "5/19/2022" "fakedataconsortium.org" "" "Original Cell Type Name=CD3+, CD8+ thymus derived cells;pubmed_id=37162864;value_description=interaction significance" "Adult" "false"


current_time=$(date +%s)
elapsed=$(echo "($current_time-$start_time)" | bc)
day_dur=$(echo "($current_time-$start_time)/(3600*24)" | bc) # automatically rounds down
hr_dur=$(echo "(($current_time-$start_time)/3600)%24" | bc | xargs printf "%02d")
min_dur=$(echo "(($current_time-$start_time)/60)%60" | bc | xargs printf "%02d")
sec_dur=$(echo "(($current_time-$start_time))%60" | bc | xargs printf "%02d")

## print the formatted time calculated above
echo ***FINAL ELAPSED TIME: $day_dur-$hr_dur:$min_dur:$sec_dur
