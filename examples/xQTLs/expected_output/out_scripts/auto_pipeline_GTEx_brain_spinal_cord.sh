#!/bin/bash
init_config=/mnt/data/jeffrey/FILER/scripts/pipeline_automation/examples/xQTLs/hipFG_tmp.ini
hipFG_root=/mnt/data/jeffrey/FILER/scripts/pipeline_automation    
declare -x hipFG_root
source $init_config    
genome_build=hg38
output_type="QTLs"

if [ "$output_type" = "QTLs" ]; then
    if [ $genome_build = "hg19" ]; then
        reference_genome="$reference_genome_hg19"
        dbSNP_ref_file="$dbSNP_hg19"
        hgnc_ref_file="$hgnc_gencode_hg19"
    elif [ $genome_build = "hg38" ]; then
        reference_genome="$reference_genome_hg38"
        dbSNP_ref_file="$dbSNP_hg38"
        hgnc_ref_file="$hgnc_gencode_hg38"
    fi
fi 
 
declare -x python_use
declare -x tabix_use
declare -x Giggle

tag=GTEx_brain_spinal_cord
output_dir=/mnt/data/jeffrey/FILER/scripts/pipeline_automation/examples/xQTLs/out_files
temp_dir=/mnt/data/jeffrey/FILER/scripts/pipeline_automation/examples/xQTLs/out_scripts
scripts_dir=$hipFG_root/src
MAX_RAM=${MAX_RAM:-5000}

declare -x scripts_dir
declare -x MAX_RAM
echo -e "Running sample $tag with hipFG version $(cat $hipFG_root/VERSION).\n"
start_time=$(date +%s)
call_script () {
    # before running the script, record the time and 
    in_script=$1
    date
    echo All arguments: "$@"
	
    prestep_time=$(date +%s)
	day_dur=$(echo "($prestep_time-$start_time)/(3600*24)" | bc) # automatically rounds down
    hr_dur=$(echo "(($prestep_time-$start_time)/3600)%24" | bc | xargs printf "%02d")
    min_dur=$(echo "(($prestep_time-$start_time)/60)%60" | bc | xargs printf "%02d")
    sec_dur=$(echo "(($prestep_time-$start_time))%60" | bc | xargs printf "%02d")
    echo PRE-STEP TOTAL ELAPSED TIME: $day_dur-$hr_dur:$min_dur:$sec_dur

	## print description for this script
	grep SCRIPT_DESCRIPTION $in_script
	
    ## run the script with args as intended.
    bash "$@"
    
    post_step_time=$(date +%s)
	day_dur=$(echo "($post_step_time-$prestep_time)/(3600*24)" | bc) # automatically rounds down
    hr_dur=$(echo "(($post_step_time-$prestep_time)/3600)%24" | bc | xargs printf "%02d")
    min_dur=$(echo "(($post_step_time-$prestep_time)/60)%60" | bc | xargs printf "%02d")
    sec_dur=$(echo "(($post_step_time-$prestep_time))%60" | bc | xargs printf "%02d")
    echo THIS STEP ELAPSED TIME: $day_dur-$hr_dur:$min_dur:$sec_dur
    
	day_dur=$(echo "($post_step_time-$start_time)/(3600*24)" | bc) # automatically rounds down
    hr_dur=$(echo "(($post_step_time-$start_time)/3600)%24" | bc | xargs printf "%02d")
    min_dur=$(echo "(($post_step_time-$start_time)/60)%60" | bc | xargs printf "%02d")
    sec_dur=$(echo "(($post_step_time-$start_time))%60" | bc | xargs printf "%02d")
    echo POST-STEP TOTAL ELAPSED TIME: $day_dur-$hr_dur:$min_dur:$sec_dur
    
    echo
    echo
}
call_script $scripts_dir/unzip_input.sh /mnt/data/jeffrey/FILER/scripts/pipeline_automation/examples/xQTLs/inputs/GTEx_ge_brain_spinal_cord.all.tsv.gz $tag $temp_dir True

call_script $scripts_dir/column_imputing_scripts/get_chr_pos_col.sh $temp_dir/.tmp_unzipped_input_${tag}.bed $tag $temp_dir 2 3 True True
rm $temp_dir/.tmp_unzipped_input_${tag}.bed

call_script $scripts_dir/column_imputing_scripts/sort_columns.sh $temp_dir/.unzipped_input_w_chr_pos_${tag}.bed $temp_dir 20 $temp_dir/.unzipped_input_w_chr_pos_${tag}.bed

call_script $scripts_dir/QC_scripts/separate_by_var_type.sh $temp_dir/.unzipped_input_w_chr_pos_${tag}.bed $tag 4 5 $temp_dir

rm $temp_dir/.unzipped_input_w_chr_pos_${tag}.bed

call_script $scripts_dir/QC_scripts/compare_ref_to_dbSNP_chrPos.sh $temp_dir/.unzipped_input_w_chr_pos_${tag}_SNP.bed $scripts_dir ${tag}_SNP $temp_dir $dbSNP_ref_file 20 1 4 5 "NR" 5 4 "4" "5" "false" "$samtools_use" "$reference_genome" "true"
rm $temp_dir/.unzipped_input_w_chr_pos_${tag}_SNP.bed
call_script $scripts_dir/QC_scripts/compare_ref_to_dbSNP_chrPos.sh $temp_dir/.unzipped_input_w_chr_pos_${tag}_OTHER.bed $scripts_dir ${tag}_OTHER $temp_dir $dbSNP_ref_file 20 1 4 5 "NR" 5 4 "4" "5" "false" "$samtools_use" "$reference_genome" "true"
rm $temp_dir/.unzipped_input_w_chr_pos_${tag}_OTHER.bed

call_script $scripts_dir/column_imputing_scripts/join_with_hgnc_info.sh $temp_dir/.tmp_new_refs_alts_${tag}_SNP.bed ${tag}_SNP $temp_dir $hgnc_ref_file $hipFG_root/resources/hgnc_joined_cols.txt 1 "ensembl_id_no_ver" 2 "3-1" "chr" 1
call_script $scripts_dir/column_imputing_scripts/join_with_hgnc_info.sh $temp_dir/.tmp_new_refs_alts_${tag}_OTHER.bed ${tag}_OTHER $temp_dir $hgnc_ref_file $hipFG_root/resources/hgnc_joined_cols.txt 1 "ensembl_id_no_ver" 2 "3-1" "chr" 1

call_script $scripts_dir/column_imputing_scripts/get_FDRs.sh $temp_dir/.tmp_new_refs_alts_${tag}_SNP.bed ${tag}_SNP $scripts_dir 9 1 "NR" $temp_dir
call_script $scripts_dir/column_imputing_scripts/get_FDRs.sh $temp_dir/.tmp_new_refs_alts_${tag}_OTHER.bed ${tag}_OTHER $scripts_dir 9 1 "NR" $temp_dir


call_script $scripts_dir/column_imputing_scripts/join_multi_cols.sh $temp_dir/.tmp_new_refs_alts_${tag}_SNP.bed $temp_dir/.new_hgnc_cols_${tag}_SNP.bed $temp_dir/.tmp_all_fdrs_${tag}_SNP.txt $temp_dir/.unzipped_input_preformat_${tag}_SNP.bed
rm $temp_dir/.tmp_new_refs_alts_${tag}_SNP.bed
rm $temp_dir/.tmp_all_fdrs_${tag}_SNP.txt
rm $temp_dir/.new_hgnc_cols_${tag}_SNP.bed

call_script $scripts_dir/column_imputing_scripts/join_multi_cols.sh $temp_dir/.tmp_new_refs_alts_${tag}_OTHER.bed $temp_dir/.new_hgnc_cols_${tag}_OTHER.bed $temp_dir/.tmp_all_fdrs_${tag}_OTHER.txt $temp_dir/.unzipped_input_preformat_${tag}_OTHER.bed
rm $temp_dir/.tmp_new_refs_alts_${tag}_OTHER.bed
rm $temp_dir/.tmp_all_fdrs_${tag}_OTHER.txt
rm $temp_dir/.new_hgnc_cols_${tag}_OTHER.bed

call_script $temp_dir/awk_script_${tag}.sh $temp_dir/.unzipped_input_preformat_${tag}_SNP.bed $temp_dir/formatted_output_${tag}_SNP.bed
rm $temp_dir/.unzipped_input_preformat_${tag}_SNP.bed
call_script $temp_dir/awk_script_${tag}.sh $temp_dir/.unzipped_input_preformat_${tag}_OTHER.bed $temp_dir/formatted_output_${tag}_OTHER.bed
rm $temp_dir/.unzipped_input_preformat_${tag}_OTHER.bed

call_script $scripts_dir/wrapup_scripts/filter_by_fdr.sh $temp_dir/formatted_output_${tag}_SNP.bed $temp_dir "GTEx_brain_spinal_cord_SNP" 5
call_script $scripts_dir/wrapup_scripts/filter_by_fdr.sh $temp_dir/formatted_output_${tag}_OTHER.bed $temp_dir "GTEx_brain_spinal_cord_OTHER" 5

call_script $scripts_dir/wrapup_scripts/final_sort.sh $temp_dir/formatted_output_${tag}_SNP_all.bed $temp_dir 22 $temp_dir/formatted_output_${tag}_SNP_all.bed "chrom+chromStart+chromEnd+variant_id+pval+target_strand+ref+alt+target_gene_symbol+target_ensembl_id+target+z_score_non_ref+beta_non_ref+beta_se_non_ref+FDR+non_ref_af+qtl_dist_to_target+QC_info+target_info+user_input"
call_script $scripts_dir/wrapup_scripts/final_sort.sh $temp_dir/formatted_output_${tag}_OTHER_all.bed $temp_dir 22 $temp_dir/formatted_output_${tag}_OTHER_all.bed "chrom+chromStart+chromEnd+variant_id+pval+target_strand+ref+alt+target_gene_symbol+target_ensembl_id+target+z_score_non_ref+beta_non_ref+beta_se_non_ref+FDR+non_ref_af+qtl_dist_to_target+QC_info+target_info+user_input"
call_script $scripts_dir/wrapup_scripts/final_sort.sh $temp_dir/formatted_output_${tag}_SNP_significant.bed $temp_dir 22 $temp_dir/formatted_output_${tag}_SNP_significant.bed "chrom+chromStart+chromEnd+variant_id+pval+target_strand+ref+alt+target_gene_symbol+target_ensembl_id+target+z_score_non_ref+beta_non_ref+beta_se_non_ref+FDR+non_ref_af+qtl_dist_to_target+QC_info+target_info+user_input"
call_script $scripts_dir/wrapup_scripts/final_sort.sh $temp_dir/formatted_output_${tag}_OTHER_significant.bed $temp_dir 22 $temp_dir/formatted_output_${tag}_OTHER_significant.bed "chrom+chromStart+chromEnd+variant_id+pval+target_strand+ref+alt+target_gene_symbol+target_ensembl_id+target+z_score_non_ref+beta_non_ref+beta_se_non_ref+FDR+non_ref_af+qtl_dist_to_target+QC_info+target_info+user_input"

call_script $scripts_dir/wrapup_scripts/gzip_final.sh $temp_dir/formatted_output_${tag}_SNP_all.bed $temp_dir/formatted_output_${tag}_SNP_all.bed.gz
call_script $scripts_dir/wrapup_scripts/gzip_final.sh $temp_dir/formatted_output_${tag}_OTHER_all.bed $temp_dir/formatted_output_${tag}_OTHER_all.bed.gz
call_script $scripts_dir/wrapup_scripts/gzip_final.sh $temp_dir/formatted_output_${tag}_SNP_significant.bed $temp_dir/formatted_output_${tag}_SNP_significant.bed.gz
call_script $scripts_dir/wrapup_scripts/gzip_final.sh $temp_dir/formatted_output_${tag}_OTHER_significant.bed $temp_dir/formatted_output_${tag}_OTHER_significant.bed.gz

call_script $scripts_dir/wrapup_scripts/tabix_on_zipped.sh $temp_dir/formatted_output_${tag}_SNP_all.bed.gz
call_script $scripts_dir/wrapup_scripts/tabix_on_zipped.sh $temp_dir/formatted_output_${tag}_OTHER_all.bed.gz
call_script $scripts_dir/wrapup_scripts/tabix_on_zipped.sh $temp_dir/formatted_output_${tag}_SNP_significant.bed.gz
call_script $scripts_dir/wrapup_scripts/tabix_on_zipped.sh $temp_dir/formatted_output_${tag}_OTHER_significant.bed.gz

rm $temp_dir/formatted_output_${tag}_SNP_all.bed
rm $temp_dir/formatted_output_${tag}_OTHER_all.bed
rm $temp_dir/formatted_output_${tag}_SNP_significant.bed
rm $temp_dir/formatted_output_${tag}_OTHER_significant.bed

mkdir -p $output_dir
mkdir -p $output_dir/all
mkdir -p $output_dir/significant
mkdir -p $output_dir/all/SNP
mkdir -p $output_dir/all/OTHER
mkdir -p $output_dir/significant/SNP
mkdir -p $output_dir/significant/OTHER
mkdir -p $output_dir/all/SNP/eQTL
mkdir -p $output_dir/all/OTHER/eQTL
mkdir -p $output_dir/significant/SNP/eQTL
mkdir -p $output_dir/significant/OTHER/eQTL
mkdir -p $output_dir/all/SNP/eQTL/bed3plus17_qtl
mkdir -p $output_dir/all/OTHER/eQTL/bed3plus17_qtl
mkdir -p $output_dir/significant/SNP/eQTL/bed3plus17_qtl
mkdir -p $output_dir/significant/OTHER/eQTL/bed3plus17_qtl
mkdir -p $output_dir/all/SNP/eQTL/bed3plus17_qtl/hg38
mkdir -p $output_dir/all/OTHER/eQTL/bed3plus17_qtl/hg38
mkdir -p $output_dir/significant/SNP/eQTL/bed3plus17_qtl/hg38
mkdir -p $output_dir/significant/OTHER/eQTL/bed3plus17_qtl/hg38

mv $temp_dir/formatted_output_${tag}_SNP_all.bed.gz $output_dir/all/SNP/eQTL/bed3plus17_qtl/hg38
mv $temp_dir/formatted_output_${tag}_SNP_all.bed.gz.tbi $output_dir/all/SNP/eQTL/bed3plus17_qtl/hg38
mv $temp_dir/formatted_output_${tag}_OTHER_all.bed.gz $output_dir/all/OTHER/eQTL/bed3plus17_qtl/hg38
mv $temp_dir/formatted_output_${tag}_OTHER_all.bed.gz.tbi $output_dir/all/OTHER/eQTL/bed3plus17_qtl/hg38
mv $temp_dir/formatted_output_${tag}_SNP_significant.bed.gz $output_dir/significant/SNP/eQTL/bed3plus17_qtl/hg38
mv $temp_dir/formatted_output_${tag}_SNP_significant.bed.gz.tbi $output_dir/significant/SNP/eQTL/bed3plus17_qtl/hg38
mv $temp_dir/formatted_output_${tag}_OTHER_significant.bed.gz $output_dir/significant/OTHER/eQTL/bed3plus17_qtl/hg38
mv $temp_dir/formatted_output_${tag}_OTHER_significant.bed.gz.tbi $output_dir/significant/OTHER/eQTL/bed3plus17_qtl/hg38
bash $scripts_dir/wrapup_scripts/get_metadata_row.sh "$output_dir/all/SNP/eQTL/bed3plus17_qtl/hg38/formatted_output_GTEx_brain_spinal_cord_SNP_all.bed.gz,$output_dir/all/OTHER/eQTL/bed3plus17_qtl/hg38/formatted_output_GTEx_brain_spinal_cord_OTHER_all.bed.gz,$output_dir/significant/SNP/eQTL/bed3plus17_qtl/hg38/formatted_output_GTEx_brain_spinal_cord_SNP_significant.bed.gz,$output_dir/significant/OTHER/eQTL/bed3plus17_qtl/hg38/formatted_output_GTEx_brain_spinal_cord_OTHER_significant.bed.gz" /mnt/data/jeffrey/FILER/scripts/pipeline_automation/examples/xQTLs/inputs/GTEx_ge_brain_spinal_cord.all.tsv.gz $tag $temp_dir "$output_dir/all/SNP/eQTL/bed3plus17_qtl/hg38,$output_dir/all/OTHER/eQTL/bed3plus17_qtl/hg38,$output_dir/significant/SNP/eQTL/bed3plus17_qtl/hg38,$output_dir/significant/OTHER/eQTL/bed3plus17_qtl/hg38" $celltypes_dict $data_categories $scripts_dir "NGEQC" "0000005" "eQTL_Catalogue" "QTLs" "hg38" "Spinal cord cervical c-1" "tissue" "" "" "" "" "" "eQTL" "bed3+17 qtl" "6/3/2022" "4/1/2022" "https://www.ebi.ac.uk/eqtl/Data_access/" "http://ftp.ebi.ac.uk/pub/databases/spot/eQTL/sumstats/ROSMAP/ge/ROSMAP_ge_brain_naive.all.tsv.gz" "Original Cell Type Name=brain spinal cord;pubmed_id=32913098" "Adult" "false"


current_time=$(date +%s)
elapsed=$(echo "($current_time-$start_time)" | bc)
day_dur=$(echo "($current_time-$start_time)/(3600*24)" | bc) # automatically rounds down
hr_dur=$(echo "(($current_time-$start_time)/3600)%24" | bc | xargs printf "%02d")
min_dur=$(echo "(($current_time-$start_time)/60)%60" | bc | xargs printf "%02d")
sec_dur=$(echo "(($current_time-$start_time))%60" | bc | xargs printf "%02d")

## print the formatted time calculated above
echo ***FINAL ELAPSED TIME: $day_dur-$hr_dur:$min_dur:$sec_dur
