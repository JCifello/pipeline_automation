#!/bin/script
SCRIPT_DESCRIPTION="This awk script is generated via hipFG_root/src/write_awk.py. It functions to rearrange columns to match a desired output. For QTLs, it additionally will ensure test statistics and allele frequencies always correspond to the test allele."

echo -n "" > $2
LC_ALL=C awk 'BEGIN{FS="\t";OFS="\t"}{chromStart=$3-1; og_ref=$4; og_alt=$5; chrom="chr" $2; chromEnd=chromStart+1; variant_id="NR"; pval=$9; target_strand=$22; ref=$4; alt=$5; target_gene_symbol=$24; target_ensembl_id=$26; target=$1; z_score_non_ref=$10/$11; beta_non_ref=$10; beta_se_non_ref=$11; FDR=$27; non_ref_af=$13/$14; qtl_dist_to_target=$25; QC_info=$21; user_input=("variant=" $6 ";ma_samples=" $7 ";maf=" $8 ";type=" $12 ";ac=" $13 ";an=" $14 ";r2=" $15 ";molecular_trait_object_id=" $16 ";gene_id=" $17 ";median_tpm=" $18 ";rsid=" $19); tested_allele=$5; other_allele=$4; EAF="NR"; A1="NR"; A2="NR"; TARGET_CHROM="NR"; TARGET_START="NR"; TARGET_END="NR"; chr_colon_pos=$20; target_info=("target_gene_coord=" $23 ";target_gene_symbol=" $24)

if (QC_info ~ /EFFECT_STATS_SIGN_CHANGED/) {
    beta_non_ref = -1 * beta_non_ref
    z_score_non_ref = -1 * z_score_non_ref
}

# get the new_ref
if (match(QC_info,/new_ref=[ACGT,]+/)){
    ref=substr(QC_info,RSTART+8,RLENGTH-8)
}
# get the new_alt
if (match(QC_info,/new_alt=[ACGT,]+/)){
    alt=substr(QC_info,RSTART+8,RLENGTH-8)
}

# get the new rsid
if (match(QC_info,/new_rsid=rs[0-9]+/)){
    variant_id=substr(QC_info,RSTART+9,RLENGTH-9)
}

## save a default variant identifier if no valid rsid is selected
if (variant_id !~ /rs[0-9]+/) {
    variant_id=(chrom ":" chromStart "-" chromEnd ":" ref ">" alt)
}

print chrom,chromStart,chromEnd,variant_id,pval,target_strand,ref,alt,target_gene_symbol,target_ensembl_id,target,z_score_non_ref,beta_non_ref,beta_se_non_ref,FDR,non_ref_af,qtl_dist_to_target,QC_info,target_info,user_input}' $1 >> $2
