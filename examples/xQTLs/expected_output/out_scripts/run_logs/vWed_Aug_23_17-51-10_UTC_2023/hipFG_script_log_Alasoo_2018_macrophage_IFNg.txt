filer_xqtl.py	 08/23/23 - 17:51:10 - INFO - Running hipFG v1.0.5
filer_xqtl.py	 08/23/23 - 17:51:10 - INFO - hipFG run-date=Wed Aug 23 17:51:10 UTC 2023
filer_xqtl.py	 08/23/23 - 17:51:10 - INFO - Running for FileID=Alasoo_2018_macrophage_IFNg
filer_xqtl.py	 08/23/23 - 17:51:10 - INFO - bed3+17 qtl automatically determined as the best format.
write_awk.py	 08/23/23 - 17:51:10 - INFO - Column chromEnd left not assigned in new data.
write_awk.py	 08/23/23 - 17:51:10 - INFO - Column FDR left not assigned in new data.
write_awk.py	 08/23/23 - 17:51:10 - INFO - Adding 1 for chromEnd, since it is undefined!
write_awk.py	 08/23/23 - 17:51:10 - INFO - Autocalculating z-score in QTLs from beta and beta_se.
write_awk.py	 08/23/23 - 17:51:10 - INFO - Scripts required: [chr_colon_pos, dbSNP, HGNC, target_gene_symbol, FDR]
select_scripts.py	 08/23/23 - 17:51:10 - INFO - Splitting by var-type; SNPs and INDELs will be in separate output files.
select_scripts.py	 08/23/23 - 17:51:10 - INFO - Splitting by significance!
select_scripts.py	 08/23/23 - 17:51:10 - INFO - Splitting by significance!
select_scripts.py	 08/23/23 - 17:51:10 - INFO - Adding pubmed_id as new key-value pair in Track Descriptions!
select_scripts.py	 08/23/23 - 17:51:10 - INFO - dataset unique ID=NGEQC0000001
