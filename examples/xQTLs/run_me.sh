:#!/bin/bash

hipFG_root=$(cd $(dirname $0)/../.. && pwd)

## Changing reference files to toy-example references for this example (~60GB --> <1 GB)
sed "s@dbSNP_hg38.*@dbSNP_hg38=\"\$hipFG_root/examples/xQTLs/reference/dbSNP_custom_example_reference.txt\"@" $hipFG_root/hipFG.ini > $hipFG_root/examples/xQTLs/hipFG_tmp.ini
sed -i.bak "s@hgnc_gencode_hg38.*@hgnc_gencode_hg38=\"\$hipFG_root/examples/xQTLs/reference/hg38_reference_genes.txt\"@" $hipFG_root/examples/xQTLs/hipFG_tmp.ini && rm $hipFG_root/examples/xQTLs/hipFG_tmp.ini.bak
sed -i.bak "s@reference_genome_hg38.*@reference_genome_hg38=\"\$hipFG_root/examples/xQTLs/reference/hg38_short_reference_genome.fa\"@" $hipFG_root/examples/xQTLs/hipFG_tmp.ini && rm $hipFG_root/examples/xQTLs/hipFG_tmp.ini.bak

bash $hipFG_root/hipFG.sh \
	--mdt mdt.tsv \
	--run_now true \
	--replace_metadata true

rm $hipFG_root/examples/xQTLs/hipFG_tmp.ini

# Calculate the md5 checksum based on availability of md5 or md5sum
calculate_md5() {
    local file=$1
    local md5_command

    if command -v md5 >/dev/null 2>&1; then
        md5_command="md5 -q"
    elif command -v md5sum >/dev/null 2>&1; then
        md5_command="md5sum"
    else
        echo "Error: Neither md5 nor md5sum command is available." >&2
        exit 1
    fi

    gunzip -c "$file" | $md5_command | awk '{print $1}'
}

out_files_dir="out_files"
expected_output_dir="expected_output/out_files"

echo ""

# Find the matching files and compare their md5 checksums
for file_path in $out_files_dir/*/*/eQTL/bedxQTL/hg38/*gz; do
    base_name=$(basename "$file_path")
    expected_file_path="$expected_output_dir/${file_path#$out_files_dir/}"

    if [[ -f "$expected_file_path" ]]; then
		echo "checking $expected_file_path"
        # Calculate md5 checksums
        md5_out_files=$(calculate_md5 "$file_path")
        md5_expected_output=$(calculate_md5 "$expected_file_path")

        # Compare the checksums
        if [[ "$md5_out_files" == "$md5_expected_output" ]]; then
			continue
        else
			echo "ERROR: Run did not match expected output."
			exit 1
        fi
    fi
done

echo "Success! All files match."

