#!/bin/bash

set -e

hipFG_root=$(dirname $(realpath $0))
declare -x hipFG_root

ini_file=$hipFG_root/hipFG.ini
source $ini_file

echo "Checking executables..."
ls -x $python_use
ls -x $Giggle
ls -x $tabix_use
ls -x $samtools_use

## get the LHS of the ini file.
expected_vars=($(awk '{if (match($0, /.*=/)) {print substr($0,RSTART,RLENGTH-1)}}' $ini_file))

# echo ${expected_vars[@]}

for i in $(seq 0 ${#expected_vars[@]}); do
	if [ $i -eq ${#expected_vars[@]} ]; then
		break
	fi
	
	if [ "$i"  == "MAX_RAM" ]; then
		continue
	fi

	my_var=${expected_vars[$i]}

	if test -f "${!my_var}" || test -d "${!my_var}"; then
		x=1
	else
		echo "ERROR: ${my_var} location not valid. Ensure that hipFG.ini is specifying absolute paths to YOUR tools and resources. For unneeded resources, specify a period"
		ls ${!my_var}
	fi
done


echo "Success: Provided paths appear to exist." 
