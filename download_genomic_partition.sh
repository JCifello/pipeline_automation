#!/bin/bash

output_dir=${1:-"./"}

mkdir -p $output_dir

wget --mirror --no-parent --cut-dirs=3 https://tf.lisanwanglab.org/GADB/download/hipFG/inferno_genomic_partition/ -nH

if [ "$output_dir" != "./" ] && [ "$output_dir" != "." ]; then
    mv ./inferno_genomic_partition $output_dir
fi
