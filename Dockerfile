FROM ubuntu:22.04

ARG DEBIAN_FRONTEND=noninteractive

WORKDIR /app

RUN apt-get update && apt-get install -y \
    libbz2-dev \
    liblzma-dev \
    lbzip2 \
    bc \
    gcc \
    make \
    autoconf \
    zlib1g-dev \
    libbz2-dev \
    libcurl4-openssl-dev \
    libssl-dev \
    ruby \
    python3 \
    python3-pip \
    wget \
    make \
    tar \
    git \
    gawk \
    vim \
    file 

RUN pip install pandas statsmodels

RUN wget https://github.com/samtools/htslib/releases/download/1.17/htslib-1.17.tar.bz2 && \
    tar -xvf htslib-1.17.tar.bz2 && \
    cd htslib-1.17/ && \
    ./configure && \
    make && \
    make install && \
    cd ..

RUN wget https://github.com/samtools/samtools/releases/download/1.17/samtools-1.17.tar.bz2 && \
    tar -xvf samtools-1.17.tar.bz2 && \
    cd samtools-1.17/ && \
    ./configure --without-curses && \
    make && \
    make install && \
    cd ..

RUN git clone https://github.com/pkuksa/FILER_giggle.git && \
	cd FILER_giggle && \
	make && \
	cd ..

RUN git clone https://JCifello@bitbucket.org/wanglab-upenn/hipfg.git hipFG

COPY hipFG_docker.ini .

CMD ["/bin/bash"]

