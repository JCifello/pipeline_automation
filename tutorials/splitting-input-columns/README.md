# Splitting Input Columns
## Introduction and Instructions
To split input columns on-the-fly, the user must specify two elements:
  
1. A "-split#" suffix in an output field definition
2. A SPLIT_CHARACTERS_(#) field in the file information section, this can be repeated for each input field to be split. These **must** be numbered in order of appearance in the original data.  
  
As a simple example, suppose an input column was called ```variant_information``` and was formatted as follows: ```chr:position:allele1_allele2```. We would thus want to split by both '_' and ':'. Among the other expected fields of the file config, we would also find the following:  
```
chr=variant_information-split1
chrStart=variant_information-split2
A1=variant_information-split3
A2=variant_information-split4

SPLIT_CHARACTERS_1=:_
```
Note this option is currently only available for QTL datasets.  

## Example
A similar example to above can be found in data from the [MetaBrain](https://doi.org/10.1038/s41588-023-01300-6) dataset. The first 9 elements of some MetaBrain columns are as follows:  
![metabrain_cut1-5_head](MetaBrain_head_portion.PNG)  
While the other columns are not shown here, SNP is the only column containing rsid's in this dataset. Thus, to extract them, this column can be split on the fly. Among the other columns required in a file config, we would include:  
```
chr=SNP-split1
chrStart=SNP-split2
variant_id=SNP-split3
A1=SNP-split4
A2=SNP-split5
```
Note that all elements of the split column do not need to be used. Additionally, the order of defined fields does not need to be in the same order as the split indexes.  
  
However, to indicate to hipFG that a split is taking place, the splitting characters must be defined. This can be carried out with:
```
SPLIT_CHARACTERS_1=:_
```  
Being included somewhere within the file config.  

## Example for multiple split fields
Including more MetaBrain columns reveals the `SNPAlleles` column which provides the alleles in a from/to format:  
![metabrain_cut_f1-10_head](MetaBrain_twosplit_example.PNG).  
  
The File Config can be used to split both the `SNP` and `SNPAlleles` columns.  
  
This would resemble the above example, but with additional specifications for the additional columns and their respective split-characters:
```
chr=SNP-split1
chrStart=SNP-split2
variant_id=SNP-split3
A1=SNP-split4
A2=SNP-split5
tested_allele=SNPAlleles-split1
other_allele=SNPAlleles-split2

SPLIT_CHARACTERS_1=:_
SPLIT_CHARACTERS_2=/
```
