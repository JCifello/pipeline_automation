[TOC]  
  
# Applying hipFG
hipFG has two inputs:  
  
1. Minimal Descriptors Table (MDT)  
2. File Configuration  

While these inputs can be created from scratch, it is often faster and more practical to prepare a hipFG run by copying and modifying the inputs from the `hipFG/examples/` folder. These existing config files and input tables can provide us with templates to carry out hipFG. This tutorial will include examples for running xQTLs.  
  
[TOC]
  
# File Config
## Output Fields Section
Assembling the config begins with selecting the desired output format from [resources/BED_schemas.tsv](https://bitbucket.org/wanglab-upenn/hipfg/src/check_allele_algorithm/resources/BED_schemas.tsv) based on the available input columns and best matching Schema.  
For example, the `bedxQTL` format has the following required columns:
```
chr;chrStart;pval;target;beta_non_ref;tested_allele
```
As well as some optional ones:
```
ref;alt;A1;A2;other_allele;EAF;ac;an
```
  
So the Output Fields section of the File Config for QTLs would look like:   
```
chr=
chrStart=
chrEnd=
pval=
target=
A1=
A2=
beta_non_ref=
beta_se_non_ref=
FDR=
tested_allele=
other_allele=
ac=
an=
```
  
Once mapped to the headers of input columns, the File Config output (for a GTEx example) yields:
```
chr=variant_id-split1
chrStart=variant_id-split2
chrEnd=
pval=pval_nominal
target=gene_id
A1=variant_id-split3
A2=variant_id-split4
beta_non_ref=slope
beta_se_non_ref=slope_se
FDR=
other_allele=variant_id-split3
tested_allele=variant_id-split4
```
Note that some fields are not defined and will be calculated, such as `FDR` and `EAF`.  
  
While many input files have distinct columns for chromosome, starting position, etc., this example combines these fields under ```variant_id``` as '_'-delimited. These can be separated with the splitting feature, which is described in more detail in [Splitting Input Columns](https://bitbucket.org/wanglab-upenn/hipfg/src/main/tutorials/splitting-input-columns). 
  
## File Information Section
The file information fields do not describe specific columns of the output, but allow users to describe the input data. Many of these only apply to QTLs.  
The left-hand-side of settings for file information, as copied from examples/xQTLs/config.txt:  
```
HAS_HEADER=
IS_BASE_ONE=
CALC_AF=
SEP_BY_SIGNIF=
LOOKUP_COL=
LOOKUP_TYPE=
MULTIPLE_VARIANT_TYPES=
```
These fields are described, beginning with the two non-QTL-specific fields:    
  
1. HAS_HEADER: Whether the input data has named columns. True/False.  
2. IS_BASE_ONE: Whether genomic positions are 0- or 1-indexed. True/False.  
3. CALC_AF: (QTL only) Whether to calculate the effect-allele frequency for the output. Must have ac + an defined above. True/False.  
4. SEP_BY_SIGNIF: (QTL only) Whether to filter and separate outputs by FDR. True/False.  
5. LOOKUP_COL: (QTL only) Which column should be used for providing gene information? May be blank or 'target'.  
6. LOOKUP_TYPE: (QTL only) What kind of information is the target, for the purpose of providing gene context? Select a field from [resources/target_lookup_types.txt](https://bitbucket.org/wanglab-upenn/hipfg/src/main/resources/target_lookup_types.txt).  
7. MULTIPLE_VARIANT_TYPES: (QTL only) Whether both SNPs and INDELs are in the input data. True/False.  
8. SPLITTING_CHARACTERS_(#): (QTL only) A field not provided in the `examples/xQTLs/mdt.tsv`. As described in [Splitting Input Columns](https://bitbucket.org/wanglab-upenn/hipfg/src/main/tutorials/splitting-input-columns), the columns to use for the split must be defined. We only do this once, so this field will be `SPLIT_CHARACTERS_1`.  
  
## Full File Config Example
A complete example File Config for example [GTEx input](https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_Analysis_v8_eQTL.tar):  
```
chr=variant_id-split1
chrStart=variant_id-split2
chrEnd=
pval=pval_nominal
target=gene_id
A1=variant_id-split3
A2=variant_id-split4
beta_non_ref=slope
beta_se_non_ref=slope_se
FDR=
other_allele=variant_id-split3
tested_allele=variant_id-split4

CALC_AF=False
SEP_BY_SIGNIF=True
HAS_HEADER=True
IS_BASE_ONE=True
LOOKUP_COL=target
LOOKUP_TYPE=ensembl_gene_id
MULTIPLE_VARIANT_TYPES=True
SPLIT_CHARACTERS=_
```
  
# MDT (.tsv)
The input table provides biological context, path information, and source information for all input files. After copying the xQTL [example input table](https://bitbucket.org/wanglab-upenn/hipfg/src/main/examples/xQTLs/input_df.tsv), we can empty the rows and use the remaining column names as a template.  
  
The input table is best conceptualized as having six types of field, each listed below:
  
1. Path information. Paths to inputs, outputs, and more. While relative paths will run correctly from the right directory, it is highly recommended to use absolute paths. `input_file temp_directory output_directory file_config init_config`.  
2. Biological information. The biological context of the sample(s) described. These are an open vocabulary, but will yield empty values in the final metadata if filled out unexpectedly. `cell_type biosample_type life_stage`
3. Measurement information. What sort of data is being provided, and what does it describe? `genome_build assay output_type` 
4. Source information.  
5. Other necessary hipFG information. `FileID ID Prefix`  
6. Additional custom fields. Any additional fields that may be relevant will be included in the final metadata as a key-term pair under `Track Description`. 

These columns/fields will be handled in turn to allow hipFG to run and provide a harmonized metadata output:  
**FileID**  
A unique identifier associated with each input file. For this example, we can use gtex_amygdala.  
**Cell type**  
Consult the input documentation/associated publication to determine which Cell Type type from [resources/cell_types.txt](https://bitbucket.org/wanglab-upenn/hipfg/src/main/resources/cell_types.txt) best describes the sample. Novel/not included cell types may be selected, but tissue and system categories will not be mapped.   
**Biosample Type**  
Whether the input file is a cell, cell line, tissue, etc.. Consult the input documentation/associated publication to determine which biosample type from [resources/biosample_types.txt](https://bitbucket.org/wanglab-upenn/hipfg/src/main/resources/biosample_types.txt) best describes the sample.  
**Input File**  
A single input file. Absolute path recommended, but relative paths from MDT will work.  
**Output Directory**  
Where to store the output file hierarchy and carry out the Giggle indexing. Absolute path recommended as above. Symbolic links OK.  
**Temp Directory**  
Where to store intermediate files and auto-generated scripts. Absolute path recommended as above, ideally a location with fast I/O.  
**Init Config**  
An absolute path to the hipFG.ini file, defining tools etc.. Alternate name OK.  
**File Config**  
An absolute path to the File Config constructed above.  
**Genome build**  
hg19 or hg38.  
**Assay**  
Consult the input documentation/associated publication to determine which assay from [resources/assay_format_data_category.txt](https://bitbucket.org/wanglab-upenn/hipfg/src/main/resources/assay_format_data_category.txt) best describes the sample. eQTL.  
**Data Source**  
GTEx  
**Output type**  
Consult the input documentation/associated publication to determine which output type from [resources/output_types.txt](https://bitbucket.org/wanglab-upenn/hipfg/src/main/resources/output_types.txt) best describes the sample. This output type will be slightly modified during the hipFG metadata step.  
**Download Date**  
The date this data was downloaded/accessed.  
**Release Date**  
The date these data were published, use "NA" for private data.  
**ID Prefix**  
This prefix is for a unique identifier in a final summary metadata of multiple datasets. Output files will be given identifiers such as myid00001, myid00002, etc..  
**DOI**  
Digital Object Identifier (DOI) associated with data if relevant, otherwise "NA".
**Project URL**  
URL for project page associated with data. E.g., GTEx data would have https://gtexportal.org/home/.    
**Raw File URL**  
If available, a URL for accessing the raw file via `wget` command. Otherwise, use "Private".  
**Original Cell Type Name**  
The original, given cell type regardless of whether it fits into [resources/cell_types.txt](https://bitbucket.org/wanglab-upenn/hipfg/src/main/resources/cell_types.txt) or not.  
**Life Stage**  
Consult the input documentation/associated publication to determine which life stage from [resources/life_stages.txt](https://bitbucket.org/wanglab-upenn/hipfg/src/main/resources/life_stage.txt) best describes the sample.  
  
## MDT first line example
An example MDT for some GTEx data would be:  
```
FileID	Cell type	Biosample Type	Input File	Output Directory	Temp Directory	Init Config	File Config	Genome build	Assay	Data Source	Output type	Download Date	Release Date	ID Prefix	DOI	Project URL	Raw File URL	Original Cell Type Name	Life Stage
gtex_amygdala	Amygdala	tissue	GTEx_Analysis_v8_eQTL/Brain_Amygdala.v8.signif_variant_gene_pairs.txt.gz  	hipfg_out	hipfg_temp	/your/absolute/path/hipFG.ini	/your/absolute/path/config.txt	hg38	eQTL	GTEx	QTLs	3/20/2023	9/11/2020	hfggtx	10.1126/science.aaz1776	https://gtexportal.org/home/	https://storage.googleapis.com/gtex_analysis_v8/single_tissue_qtl_data/GTEx_Analysis_v8_eQTL.tar	Brain Amygdala	Adult
```  
