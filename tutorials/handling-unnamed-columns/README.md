## Handling unnamed-columns
The file config is meant to map existing input columns to desired standard columns. The field definitions section of a typical file config could look like:
```
#DEFINE COLS
value=Confidence_Score2
sourceChrom=InteractorAChr
sourceStart=InteractorAStart
sourceEnd=InteractorAEnd
targetChrom=InteractorBChr
targetStart=InteractorBStart
targetEnd=InteractorBEnd
```
However, some datasets may lack column headers and look like this:
```
chr1    0       713600  Quies   0       .       0       713600  255,255,255
chr1    713600  713800  EnhA2   0       .       713600  713800  255,195,77
chr1    713800  714800  TssA    0       .       713800  714800  255,0,0
chr1    714800  715800  EnhA2   0       .       714800  715800  255,195,77
chr1    715800  762000  Quies   0       .       715800  762000  255,255,255
chr1    762000  762200  TssFlnk 0       .       762000  762200  255,69,0
chr1    762200  763000  TssA    0       .       762200  763000  255,0,0
chr1    763000  763200  EnhA2   0       .       763000  763200  255,195,77
chr1    763200  805000  Quies   0       .       763200  805000  255,255,255
chr1    805000  805200  EnhWk   0       .       805000  805200  255,255,0
```
In this case, `HAS_HEADER` must be set to False in the config. Additionally, column definitions must be made with place holder columns `V0`, `V1`, etc.. Note that `V` is capitalized and counting starts at 0.  
  
So, a config for this file in the bed9 format would look like:
```
chrom=V0
chromStart=V1
chromEnd=V2
name=V3
score=V4
strand=V5
thickStart=V6
thickEnd=V7
itemRgb=V8

HAS_HEADER=False
IS_BASE_ONE=False
```  
These `V` fields do not need to all be used, and can be used in any order. 
