"""
Use the input table to generate a new file, to put in respective temporary directories, which can be called to run the pipeline for all samples.
"""
import pandas as pd, os, argparse
import csv
from subprocess import call

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("input_table")
    parser.add_argument("run_samples_parallel")
    parser.add_argument("num_jobs")
    parser.add_argument("run_now")
    parser.add_argument("metadata_only")
    parser.add_argument("replace_metadata")
    parser.add_argument("simple_mdt")
    parser.add_argument("testing")
    args = parser.parse_args()

    if args.run_samples_parallel.lower() in ["t", "true"]:
        args.run_samples_parallel = "true"

    input_df = pd.read_table(args.input_table, sep="\t", quoting=csv.QUOTE_NONE)
    input_df.columns = [i.lower().replace(" ", "_") for i in input_df.columns]
    input_df["temp_directory"] = [os.path.abspath(i) for i in input_df["temp_directory"]]
    
    temp_dirs = pd.unique(input_df["temp_directory"]).tolist()
    
    #
    outdirs_column = [ i for i in range(len(input_df.columns)) if input_df.columns[i]=="output_directory" ][0]
    fileid_column = [ i for i in range(len(input_df.columns)) if input_df.columns[i]=="fileid" ][0]
    if args.simple_mdt.lower() == "false":
        shortid_column = [ i for i in range(len(input_df.columns)) if input_df.columns[i]=="id_prefix" ][0]
    else:
        shortid_column=fileid_column # for simplified mdt, dont look for short IDs
        
    # write command list
    lists_written=set()
    for temp_dir in temp_dirs:
        out_path = os.path.join(temp_dir, "command_list.txt")
        with open(out_path, "w") as f:
            fileids = input_df.loc[(input_df["temp_directory"]==temp_dir),"fileid"].tolist()
            
            for fileid in fileids:
                f.write("bash {} > {}/sample_logs/samplelog_{}.txt\n".format(os.path.join(temp_dir, "auto_pipeline_"+fileid+".sh"), temp_dir, fileid))
                
                if args.testing.lower() in ("t", "true"):
                    break

        lists_written.add(out_path)
        
        with open(os.path.join(temp_dir, "hipFG_group_command.sh"), "w") as f:
            scripts_dir=os.path.split(__file__)[0]
            full_script="""#!/bin/bash
set -e
do_parallel="{}"
num_jobs={}
temp_dir={}
scripts_dir={}
hipFG_root={}
simple_mdt={}

## Functions to simulate associative array functionality. 
# Function to access key-value pair value.
get_value() {{
  local key=$1
  local array=("${{@:2}}")
  
  for ((i = 0; i < ${{#array[@]}}; i += 2)); do
    if [[ "${{array[i]}}" == "$key" ]]; then
      echo "${{array[i+1]}}"
      return
    fi
  done
  
  echo "Key not found: $key"
}}


# Function to assign a key-value pair
assign_value() {{
  local key=$1
  local value=$2
  local array=("${{@:3}}")
  
  # Check if the key already exists
  for ((i = 0; i < ${{#array[@]}}; i += 2)); do
    if [[ "${{array[i]}}" == "$key" ]]; then
      array[i+1]=$value
      echo ${{array[@]}}
	  return
    fi
  done
  
  # Key not found, add a new key-value pair
  array+=("$key" "$value")
  echo "${{array[@]}}"
}}

declare -x hipFG_root
source {} # run the initial config
declare -x Giggle

mkdir -p $temp_dir/sample_logs
if [ $do_parallel = "true" ]; then
    parallel -a $temp_dir/command_list.txt --joblog $temp_dir/command_list_out.txt --jobs $num_jobs "{{}}"
else
    bash $temp_dir/command_list.txt
fi

out_dirs=($(tail -n +2 {} | cut -f{} | {} sort | uniq))
source_ids=($(tail -n +2 {} | cut -f{},{} | {} sort | uniq | sed 's/\\t/my_long_delimiter/g'))

short_id_counter=()
short_id_mapper=()
id_list=()

for short_id in ${{source_ids[@]}}; do
    my_fileid=$(echo $short_id | awk 'BEGIN{{FS="my_long_delimiter"}}{{print $1}}')
    id_list[${{#id_list[@]}}+1]=$my_fileid
    if [ $simple_mdt == "false" ]; then
        my_id=$(echo $short_id | awk 'BEGIN{{FS="my_long_delimiter"}}{{print $NF}}')
        short_id_mapper=($(assign_value $my_fileid $my_id ${{short_id_mapper[@]}}))
        short_id_counter=($(assign_value $my_id 1 ${{short_id_counter[@]}}))
    fi
done

leaf_dirs=()
for out_dir in ${{out_dirs[@]}}; do
    # print all files in output recursively. Keep if it is a folder with contents. Keep if the lowest level is a genome build. 
    abs_out_dir=$(cd $out_dir && pwd)
    new_leaf_dirs=($(ls -lR $abs_out_dir | grep "^/" | cut -d ':' -f1 | awk 'BEGIN{{FS="/"}}{{if (($NF=="hg19")||($NF=="hg38")){{print $0}}}}'))
    leaf_dirs=(${{leaf_dirs[@]}} ${{new_leaf_dirs[@]}})
done

leaf_dirs=($(for i in ${{leaf_dirs[@]}}; do echo "$i"; done | sort -u))

echo -n "" > $temp_dir/giggle_input_dirs.txt
{}
for out_dir in ${{leaf_dirs[@]}}; do
    for fileid in ${{id_list[@]}}; do
        if [ $simple_mdt == "false" ]; then
            for md_file in $(find $out_dir -type f -name "metadata_row_${{fileid}}.tsv"); do
                short_id=$(get_value $fileid ${{short_id_mapper[@]}})
                short_id_num=$(get_value $short_id ${{short_id_counter[@]}})
                short_id_counter=($(assign_value $short_id $(echo $short_id_num+1 | bc) ${{short_id_counter[@]}}))
                new_id=$(printf ${{short_id}}%05d $short_id_num | sed 's/[a-z]/\\U&/g')
                combination_out_dir=$(grep "$fileid.*$short_id" {} | cut -f{})

                awk 'BEGIN{{FS="\\t"}}{{ printf "%s\\t","'$new_id'"; for (i=2;i<NF;i++){{printf "%s\\t",$i}}; print $NF}}' $md_file >> $combination_out_dir/master_project_metadata.tsv
                rm $md_file
            done
         else
            echo -e "\\nUsing a simplified MDT. Skipping metadata combination for $fileid."
         fi
    done
    {}
    echo $out_dir >> $temp_dir/giggle_input_dirs.txt
done

if [ "{}" == "true" ]; then
    echo -e "\\nRunning in metadata_only mode. Breaking."
    exit
fi

if [ $do_parallel == "true" ]; then
    parallel -a $temp_dir/giggle_input_dirs.txt --joblog $temp_dir/giggle_parallel_out.txt --jobs $num_jobs "bash $scripts_dir/wrapup_scripts/giggle_script.sh {{}}"
else
    for giggle_dir in $(cat $temp_dir/giggle_input_dirs.txt); do
        bash $scripts_dir/wrapup_scripts/giggle_script.sh $giggle_dir
    done
fi

sample_logs=$(cat $temp_dir/command_list.txt | rev | cut -d ' ' -f 1 | rev)
for i in $sample_logs; do
    awk '{{if ($0 ~ /does not map to a known/) {{ print $0; exit }} }}' $i
done

echo -e "\\nStandardized data outputs saved in:"
for out_dir in ${{out_dirs[@]}}; do
    echo $out_dir
done | sort -u 

echo ""

""".format(args.run_samples_parallel, args.num_jobs, temp_dir, scripts_dir, os.path.dirname(scripts_dir),
args.simple_mdt.lower(), os.path.abspath(input_df["init_config"][0]),
args.input_table, outdirs_column+1, 
"head -n 1 | " if args.testing.lower() in ("t","true") else "",
args.input_table, fileid_column+1, shortid_column+1,
"head -n 1 | " if args.testing.lower() in ("t","true") else "",
"for md_out_dir in $(tail -n +2 {} | cut -d $'\\t' -f {}); do\n\techo -n \"\" > $md_out_dir/master_project_metadata.tsv\ndone".format(args.input_table, outdirs_column+1) if args.replace_metadata.lower() == "true" else "",
args.input_table, outdirs_column+1,
"echo \"Will run Giggle indexing of directory: $out_dir\"" if not (args.metadata_only.lower() in ("t", "true")) else "",
"true" if args.metadata_only.lower() in ("t", "true") else "false")

            f.write(full_script)

        print("************\nScript generation complete.\nCheck run logs in {} for messages, warnings, and log output.\n".format(os.path.join(temp_dir, "run_logs")))
        
        if args.metadata_only.lower() == "true": # if metadata_only is specified, comment out all rows not related to the metadata. Keep definitions and timers. 
            with open(out_path, "r") as f:
                all_auto_scripts = [ i.split(" ")[1] for i in f.readlines() if os.path.exists(i.split(" ")[1])]
                for auto_script in all_auto_scripts:
                        my_command="x=$(grep -n \"call_script ()\" {0} | cut -d ':' -f 1); let x=x+35; y=$(grep -n metadata_row.sh {0} | cut -d ':' -f 1); let y=y-1; sed -i.bak \"${{x}},${{y}}s/^/#/g\" {0} && rm {0}.bak".format(auto_script)
                        call(my_command, shell=True)
        
        if args.testing == True:
            print("Running in testing mode. Only printing/executing one command.")
        
        if args.run_now.lower() == "true":
            print("Executing now. \nSaving temporary files in {}.\n".format(temp_dir))
            call("bash " + os.path.join(temp_dir, "hipFG_group_command.sh"), shell=True)
        else:    
            print("Execute the hipFG group command, which is in the temporary directory, to run hipFG:\n\nbash {}\n\n".format(os.path.join(temp_dir, "hipFG_group_command.sh")))
        
        if args.testing == True:
            break
