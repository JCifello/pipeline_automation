"""
After joining the input QTL data with dbSNP, go through resulting lines and:
    * remove rows if it appears twice and only with different rsids. If this happens, only keep the row with an rsid matching dbSNP.
    * save whether the input ref/alt combination matches dbSNP. If it doesn't, check whether they are reversed. Also check whether strand could have changed. 
    * based on strand/order flip, save correct ref/alt and mark whether sign (effect) or strand flips.

"""
import argparse
import re, sys, os
from collections import defaultdict
from subprocess import check_output

def get_complement(in_seq):
    """Get the complementary sequence to the input"""
    out_seq = ""
    comp_vals={"A":"T",
        "T":"A",
        "C":"G",
        "G":"C"}
    for nt in in_seq:
        out_seq += comp_vals.get(nt,",")
    return out_seq

def lens_compatible(old_a, old_b, new_a, new_b, is_snp):
    """Look at two pairs of alleles and check whether the lengths are compatible.
    This will also check if alleles lengths are compatible with the allele order flipped. 
    """
    if is_snp:
        aa_compat = get_lens(old_a)[0] in get_lens(new_a)
        bb_compat = get_lens(old_b)[0] in get_lens(new_b)
        ab_compat = get_lens(old_a)[0] in get_lens(new_b)
        ba_compat = get_lens(old_b)[0] in get_lens(new_a)
        return (aa_compat and bb_compat) or (ab_compat and ba_compat)

    else:
        ## for INDELs, allow for one to match and the other one to be a diff len, as long as it's not len=1
        aa_compat = get_lens(old_a)[0] in get_lens(new_a)
        bb_compat = get_lens(old_b)[0] in get_lens(new_b)
        ab_compat = get_lens(old_a)[0] in get_lens(new_b)
        ba_compat = get_lens(old_b)[0] in get_lens(new_a)
        
        indel_perfect_match = (aa_compat and bb_compat) or (ab_compat and ba_compat)
        if indel_perfect_match:
            return True

        a_is_long = len([i for i in get_lens(new_a) if i>1])>0
        b_is_long = len([i for i in get_lens(new_b) if i>1])>0
        
        if (bb_compat or ab_compat) and a_is_long:
            return True

        if (aa_compat or ba_compat) and b_is_long:
            return True

        return False

def get_lens(my_allele):
    """Return the length of an allele as a list. This list may have only one element."""
    if "," in my_allele:
        return [len(i) for i in my_allele.split(",")]
    else:
        return [len(my_allele)]

def check_match(old_val, new_val):
    """Check whether the old ref/alt can match the value from dbSNP"""
    if "," in new_val: 
        match1 = re.search("^{},".format(old_val), new_val)
        match2 = re.search(",{}$".format(old_val), new_val)
        match3 = re.search(",{},".format(old_val), new_val)
        if match1 or match2 or match3:
            return True
    else:
        return old_val==new_val
    return False

def check_cluster_match(cluster_split, allele_A_col, allele_B_col, new_ref_col, 
                new_alt_col, old_rsid_col, new_rsid_col, ref_col, alt_col, tested_col, force_run, line_num, is_snp):
    """take in a row and determine whether it can be beta-normalized.
    If the provided REF>ALT is seen as ALT>REF in dbSNP, note that the statistics should be flipped. Return the proper order. 
    TODO: turn into a for-loop or switch-like mechanism so there's less repeated code???
    Possibilities:
    1. allele A matches reference AND allele B matches alt. 
    2. allele A matches alternate AND allele B matches ref.
    3. allele A matches complement of ref AND allele B matches complement of alt.
    4. allele A matches complement of alt AND allele B matches complement of ref.
    
    Signs get flipped for 2/4 OR 1/3 if the reference is tested. 
    
    """
    ## check whether the rsid matches, allowing for case where rsid is not provided. 
    if old_rsid_col=="NR":
        rsid_match=False
    else:
        try:
            rsid_match=(cluster_split[old_rsid_col]==cluster_split[new_rsid_col])
        except IndexError:
            print(cluster_split)
            raise IndexError
            
    ### do available columns mean dbSNP matches to ref are sign flipping or no?
    A_is_ref_sign = 1 # 1: yes, -1: no
    A_is_tested_sign = -1 # 1: yes, -1: no. Default behavior assumes A=ref. Will be corrected if this is not true. 
    if ref_col != "NR":
        if ref_col == allele_A_col:
            A_is_ref_sign = 1 # sign adjustment when allele A is the dbSNP reference. 
        if ref_col == allele_B_col:
            A_is_ref_sign = -1

    if alt_col != "NR":
        if alt_col == allele_A_col:
            A_is_ref_sign = -1 # if allele A matches the dbSNP ref, but the user says A is an alt allele then the sign gets flipped.
        if alt_col == allele_B_col:
            A_is_ref_sign = 1

    if tested_col != "NR": # not comparing just column numbers since the tested allele column may be its own column. 
        if cluster_split[tested_col] == cluster_split[allele_A_col]:
            A_is_tested_sign = 1 # if A is the dbSNP reference allele, then A == test means the reference is being tested. We want non_ref to always be test allele. 
        if cluster_split[tested_col] == cluster_split[allele_B_col]:
            A_is_tested_sign = -1
            
        if cluster_split[tested_col] not in [cluster_split[allele_A_col], cluster_split[allele_B_col]]:
            print(cluster_split[tested_col], cluster_split[allele_A_col], cluster_split[allele_B_col])
            print("Error: Neither input allele matches the tested allele! Were the correct columns selected to be ref/alt/tested the alleles?")
            sys.exit(1)

    ### defaults for each check. 
    match_found=False
    norm_factor=1
    wrong_strand=False
    match_ref, match_alt = "NR", "NR"
    ref_match = False
    novel_alt=False
    len_match=lens_compatible(cluster_split[allele_A_col], cluster_split[allele_B_col], cluster_split[new_ref_col], cluster_split[new_alt_col], is_snp)
    
    ## before doing any other checks, so whether the allele lengths are compatible. Break if incompatible.
    if len_match==False:
        return match_found, norm_factor, rsid_match, len_match, wrong_strand, match_ref, match_alt, ref_match, novel_alt
    
    comp_A = get_complement(cluster_split[allele_A_col])
    comp_new_ref = get_complement(cluster_split[new_ref_col])
    comp_B = get_complement(cluster_split[allele_B_col])
    comp_new_alt = get_complement(cluster_split[new_alt_col])
    comp_test = get_complement(cluster_split[tested_col])

    checks_count=1
    check_pairs = (((allele_A_col, new_ref_col), (allele_B_col, new_alt_col)), # normal match, if A ref/alt tested.
    ((allele_A_col, new_alt_col), (allele_B_col, new_ref_col)), # flip sign
    ((allele_A_col, comp_new_ref), (allele_B_col, comp_new_alt)), # wrong strand
    ((allele_A_col, comp_new_alt), (allele_B_col, comp_new_ref))) # wrong strand + flip

    while match_found==False and checks_count < (2 if ref_col != "NR" else 5): # don't allow complementary/swapped matches if ref/alt provided. 
        pair1 = check_pairs[checks_count-1][0]
        pair2 = check_pairs[checks_count-1][1]
        check1 = check_match(cluster_split[pair1[0]], cluster_split[pair1[1]] if type(pair1[1])==int else pair1[1]) # check alleleA against something
        check2 = check_match(cluster_split[pair2[0]], cluster_split[pair2[1]] if type(pair2[1])==int else pair2[1]) # check alleleB against something
        
        if checks_count==1 and ref_col != "NR": # if a reference is assigned, allow new alleles for the other one. 
            if A_is_ref_sign == 1:
                if check2 == False:
                    novel_alt=True
                check2 = True
            if A_is_ref_sign == -1:
                if check1 == False:
                    novel_alt=True
                check1 = True
        
        if check1 and check2:
            match_found=True
            ## do ref and alt only match to complements of the dbSNP?
            if checks_count in (1,2):
                wrong_strand=False
            if checks_count in (3,4):
                wrong_strand=True
            if checks_count in (1,3):
                ref_match = True

            ## output refs and matches to use
            # if provided ref or A1 matches the alt, flip the order.
            if checks_count == 1: # order, strand OK
                match_ref = cluster_split[pair1[0]]
                match_alt = cluster_split[pair2[0]]
            if checks_count == 2: # order backwards, strand OK
                match_ref = cluster_split[pair2[0]]
                match_alt = cluster_split[pair1[0]]
            if checks_count == 3: # order OK, strand flipped
                match_ref = get_complement(cluster_split[pair1[0]])
                match_alt = get_complement(cluster_split[pair2[0]])
            if checks_count == 4: # order backwards, strand flipped
                match_ref = get_complement(cluster_split[pair2[0]])
                match_alt = get_complement(cluster_split[pair1[0]])

            if checks_count in (1,2):
                norm_factor = 1 if (match_alt == cluster_split[tested_col]) else -1
            else:
                norm_factor = 1 if (match_alt == get_complement(cluster_split[tested_col])) else -1

        checks_count += 1   
    
    return match_found, norm_factor, rsid_match, len_match, wrong_strand, match_ref, match_alt, ref_match, novel_alt

def set_best_check(best_norm, best_strand, best_rsid, best_matches, best_alt, new_norm, new_strand, new_rsid, new_matches, alt_matches):
    """Compare the current normalization factor, strand, and rsid. If something matches that didn't before, approve it."""
    norm_better, strand_better, rsid_better, alt_better = False, False, False, False
    norm_worse, strand_worse, alt_worse = False, False, False
    if best_norm==-1 and new_norm==1:
        norm_better=True
    if best_strand==True and new_strand==False: # want strand_flipping to equal false. 
        strand_better=True
    if best_rsid==False and new_rsid:
        rsid_better=True
    if best_alt==False and alt_matches:
        alt_better=True
    
    if best_norm==1 and new_norm==-1:
        norm_worse=True
    if best_strand==False and new_strand==True:
        strand_worse=True
    if alt_matches==False and best_alt:
        alt_worse=True

    if best_matches==False and new_matches:
        return True, new_norm, new_strand, new_rsid, new_matches
    
    if best_matches and new_matches and (alt_worse==False): # if multiple options match, must improve in one of these ways to keep it
        if alt_better or norm_better or strand_better or ((norm_worse==False and strand_worse==False and alt_worse==False) and rsid_better):
            return True, new_norm, new_strand, new_rsid, new_matches
            
    return False, new_norm, new_strand, new_rsid, new_matches

def get_next_ref(in_file):
    """Take in the pipe from the human genome lookup and return the full next reference sequence."""
    new_current_line = in_file.readline() # first bit of sequence
    tot_seq=""
    try:
        while new_current_line[0] != ">":
            tot_seq+=new_current_line.rstrip().upper()
            new_current_line = in_file.readline()
    except IndexError:
        pass

    # print("Reading, got", tot_seq)
    return tot_seq

def genomic_reference_samtools_check(cluster_start_split, sam_ref1, sam_ref2, A_allele_col, B_allele_col, tested_col, is_snp):
    """Take in the provided alleles and the genomic allele(s) to determine which allele matches, and whether there is a match at all.
    The second genomic reference (), may be an empty string when only one genomic reference is provided (SNPs).
    """
    best_ref, best_alt, best_norm, flags = "NR", "NR", 1, []
    ### if the reference allele is defined
    if ref_allele_col != "NR":
        samtools_match, allele_found = sam_ref1==cluster_start_split[ref_allele_col], sam_ref1

    ## if A1 and A2 need to be mapped to ref/alt. 
    # Do any alleles match the reference???
    if ref_allele_col == "NR":
        samtools_match, allele_found = (True,sam_ref1) if sam_ref1==cluster_start_split[A_allele_col] else (False,None)
        if is_snp: # if snp, check B against first genomic reference. 
            samtools_match, allele_found = (True,sam_ref1) if sam_ref1==cluster_start_split[B_allele_col] else (samtools_match, allele_found)
        else:
            old_sam_match=samtools_match
            samtools_match, allele_found = (True,sam_ref2) if sam_ref2==cluster_start_split[B_allele_col] else (samtools_match, allele_found)
            if samtools_match and old_sam_match:
                flags.append("BOTH_ALLELES_MATCH_REFERENCE_GENOME")
            
    ## figure out normalizing factor based on match(es) and the test allele
    # Is the matching genomic allele the tested allele?
    best_norm=1
    if samtools_match:
        if allele_found == cluster_start_split[tested_col]:
            best_norm = -1
       
        best_ref = allele_found
        best_alt = cluster_start_split[A_allele_col] if (cluster_start_split[B_allele_col]==allele_found) else cluster_start_split[B_allele_col] # simply assign the NON-REFERENCE allele to be the alternate.
            
    return samtools_match, best_ref, best_alt, best_norm, flags

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("input_joined")
    parser.add_argument("samtools_output_1", help="The output file after looking up the JOINED input file in the genome reference.")
    parser.add_argument("samtools_output_2", help="The output file after looking up the JOINED input file in the genome reference.")
    parser.add_argument("output_joined")
    parser.add_argument("target_col")
    parser.add_argument("A_allele_col", type=int, help="Column number in the tab-delimited file with the prejoin A allele. May be ref, alt, or unknown. base-1.")
    parser.add_argument("B_allele_col", type=int)
    parser.add_argument("old_rsid_col", default="NR", help="Column number if an old RSID is provided.")
    parser.add_argument("new_ref_col", type=int, help="The column of the reference allele from the join with dbSNP.")
    parser.add_argument("joined_length", type=int, help="Maximum length of the joined file.")
    parser.add_argument("--force_run", action="store_true", help="")
    parser.add_argument("--ref_allele_col", default="NR", help="Column number for the reference allele. Must be the same as A or B allele.")
    parser.add_argument("--alt_allele_col", default="NR", help="Column number for the alternate allele. Must be the same as A or B allele.")
    parser.add_argument("--tested_allele_col", default="NR", help="Column number for the tested allele. Must be the same as A or B allele.")
    parser.add_argument("--other_allele_col", default="NR", help="Column number for the other, non-tested allele. Must be the same as A or B allele.")
    args = parser.parse_args()
    
    ###### preprocess or obtain all necessary input columns. Subtract one from all provided columns to force 0-based indexing.
    for i in [k[0] for k in args._get_kwargs() if "col" in k[0]]:
        provided_val = args.__dict__[i]
        try:
            args.__dict__[i] = int(provided_val)-1
        except ValueError:
            pass
    
    if (args.ref_allele_col=="NR") and (args.alt_allele_col=="NR") and (args.tested_allele_col=="NR"):
        print("ERROR: At least ONE of ref, alt, or tested allele must be assigned.")
        sys.exit()

    try:
        target_col = int(args.target_col)
    except ValueError:
        target_col = args.target_col

    A_allele_col = args.A_allele_col
    B_allele_col = args.B_allele_col
    old_rsid_col = args.old_rsid_col
    new_ref_col = args.new_ref_col
    joined_length = args.joined_length
    ref_allele_col = args.ref_allele_col
    alt_allele_col = args.alt_allele_col
    tested_allele_col = args.tested_allele_col
    other_allele_col = args.other_allele_col
    
    new_alt_col = new_ref_col+1
    new_rsid_col = new_ref_col-1
    last_original_col = new_ref_col-4

    # throw error in case wrong columns specified
    if last_original_col < min([i for i in [old_rsid_col, new_ref_col, ref_allele_col, alt_allele_col, tested_allele_col, other_allele_col] if i!="NR"]):
        print("Invalid new reference column specified!")
        sys.exit(1)
    
    ## non-normalized file:
    out_dir, out_file = os.path.split(args.output_joined)
    my_tag = "_".join(out_file.split("_")[4:])[:-4]
    non_norm_path = os.path.join(out_dir, "joined_lines_not_normalized_{}.bed".format(my_tag))
    all_nonnorm = set()

    ###### move through the input line by line, creating clusters along the way when duplicate locations are found.
    line_counter = defaultdict(int)
    same_cluster_count=0
    last_match=False
    
    try: # a second samtools file only exists if there are multiple variant types (INDELS have diff. lengths)
        f4 = open(args.samtools_output_2, "r")
    except FileNotFoundError:
        f4 = None
    
    with open(args.input_joined, "r") as f1, open(args.output_joined, "w") as f2, open(args.samtools_output_1, "r") as f3:
        line = f1.readline()
        _ = f3.readline() # get started on reading so get_next_ref starts on the first reference line
        if f4 != None:
            _ = f4.readline()
        else:
            sam_next2 = ""

        is_snp = True if (len(line.split("\t")[A_allele_col])==1) and (len(line.split("\t")[B_allele_col])==1) else False # this file is all snps or all INDELs. Which is this?
        
        sam_ref1_next = get_next_ref(f3)
        sam_ref2_next = get_next_ref(f4) if (f4 != None) else ""
        
        while line:
            best_ref, best_alt, best_full_ref, best_full_alt = "NR", "NR", "NR", "NR"
            best_rsid, best_strand, best_norm, cluster_matches = "NR", False, 1, False
            samtools_match, best_source, best_len_match = False, "NR", False
            best_rsid_matches, matches, best_alt_matches, best_novel_alt = False, False, False, False
            rsid_defined_as_variant = False
            genome_flags = []

            cluster_start = line.rstrip()
            
            new_line=""
            
            cluster_start_split = cluster_start.split("\t")
            best_cluster_list = cluster_start_split
            line_sections = len(cluster_start_split)
                
            sam_ref1 = sam_ref1_next
            sam_ref2 = sam_ref2_next
            if line_sections == args.joined_length: # if dbsnp match made, parse through rsid's and contents for best match
                try:
                    cs_no_rsid="|".join([cluster_start_split[i] for i in (0,target_col,A_allele_col,B_allele_col)])
                except TypeError:#skip gene col when fixed (provided as a str)
                    cs_no_rsid=cluster_start_split[0]+"|"+cluster_start_split[A_allele_col]+"|"+cluster_start_split[B_allele_col]
                
                if len(line.split("\t"))>1:
                    line_counter["lines viewed"] += 1
                line = f1.readline()

                cluster_next = line
                cluster_next_split = cluster_next.split("\t")
                sam_ref1_next = get_next_ref(f3)
                sam_ref2_next = get_next_ref(f4) if (f4 != None) else ""
                
                try:
                    cn_no_rsid="|".join([cluster_next_split[i] for i in (0,target_col,A_allele_col,B_allele_col)])
                except IndexError:    
                    cn_no_rsid=""
                except TypeError:#skip gene col when fixed (provided as a str)
                    try:
                        cn_no_rsid=cluster_next_split[0]+"|"+cluster_next_split[A_allele_col]+"|"+cluster_next_split[B_allele_col]
                    except IndexError:
                        cn_no_rsid=""
                
                matches, norm_factor, rsid_matches, len_matches, strand_switched, my_ref, my_alt, ref_match, novel_alt = check_cluster_match(cluster_start_split, A_allele_col, B_allele_col, \
                                                new_ref_col, new_alt_col, old_rsid_col, new_rsid_col, ref_allele_col, alt_allele_col, tested_allele_col, args.force_run, line_counter["lines viewed"], is_snp)

                set_better, norm_check, strand_check, rsid_check, matches_check = set_best_check(None, None, None, None, None, 
                    norm_factor, strand_switched, rsid_matches, matches, my_alt==cluster_start_split[new_alt_col])
                
                if matches:
                    cluster_matches, best_len_match = True, True
                    best_norm = norm_factor
                    best_ref, best_alt = my_ref, my_alt
                    best_full_ref, best_full_alt = cluster_start_split[new_ref_col], cluster_start_split[new_alt_col]
                    best_rsid, best_strand = cluster_start_split[new_rsid_col], strand_switched
                    best_rsid_matches = rsid_matches
                    best_ref_match = ref_match
                    best_novel_alt = novel_alt
                    best_alt_matches = my_alt==cluster_start_split[new_alt_col]
                
                cluster_lines=0

                while (cs_no_rsid == cn_no_rsid): # or (cs_no_rsid_swap_refalt == cn_no_rsid) or (cs_no_rsid_comp==cn_no_rsid) or (cs_no_rsid_comp_swap_refalt==cn_no_rsid): # if this line and next match
                    
                    sam_ref1 = sam_ref1_next
                    sam_ref2 = sam_ref2_next
                    
                    line_counter["lines from join with dbSNP but not kept"] += 1
                    cluster_lines += 1
                    try:
                        matches, norm_factor, rsid_matches, len_matches, strand_switched, my_ref, my_alt, ref_match, novel_alt = check_cluster_match(cluster_next_split, A_allele_col, B_allele_col, \
                                                    new_ref_col, new_alt_col, old_rsid_col, new_rsid_col, ref_allele_col, alt_allele_col, tested_allele_col, args.force_run, line_counter["lines viewed"]+cluster_lines, is_snp)
                    except IndexError:
                        matches, norm_factor, rsid_matches, len_match = False, 1, False, False
                    
                    set_better, norm_check, strand_check, rsid_check, matches_check = set_best_check(best_norm, best_strand, best_rsid_matches, cluster_matches, best_alt_matches,
                        norm_factor, strand_switched, rsid_matches, matches, my_alt==cluster_next_split[new_alt_col])
                    
                    if set_better:
                        cluster_matches, best_len_match = True, True
                        best_norm = norm_factor
                        best_ref, best_alt = my_ref, my_alt
                        best_full_ref, best_full_alt = cluster_next_split[new_ref_col], cluster_next_split[new_alt_col]
                        best_strand = strand_switched
                        best_rsid = cluster_next_split[new_rsid_col]# if is_snp else "NR"
                        best_rsid_matches = rsid_matches
                        best_ref_match = ref_match
                        best_cluster_list = cluster_next_split
                        best_novel_alt = novel_alt
                        best_alt_matches = my_alt==cluster_next_split[new_alt_col]
                    
                    ## get ready for next step in loop, if necessary
                    line = f1.readline()
                    cluster_next = line
                    cluster_next_split = cluster_next.split("\t")
                    
                    sam_ref1_next = get_next_ref(f3)
                    sam_ref2_next = get_next_ref(f4) if (f4 != None) else ""
                    
                    try:
                        cn_no_rsid="|".join([cluster_next_split[i] for i in (0,target_col,A_allele_col,B_allele_col)])
                    except IndexError:    
                        cn_no_rsid=""
                    except TypeError:#skip gene col when fixed (provided as a str)
                        try:
                            cn_no_rsid=cluster_next_split[0]+"|"+cluster_next_split[A_allele_col]+"|"+cluster_next_split[B_allele_col]
                        except IndexError:
                            cn_no_rsid=""

                line_counter["lines viewed"] += cluster_lines
                
                ### this executes after the loop.
                ## write using the saved best variables
                no_first_to_end = "\t".join(best_cluster_list[1:last_original_col] + [cluster_start_split[0]])
                if cluster_matches:
                    best_source = "dbSNP"
                    
                    line_counter["lines mapped with non-ref allele not found in dbSNP"] += 1 if best_novel_alt else 0
                    line_counter["lines resolved via only flip sign"] += 1 if (best_norm==-1 and best_strand==False) else 0
                    line_counter["lines resolved via only flip strand"] += 1 if (best_strand and best_norm==1) else 0
                    line_counter["lines resolved via flip strand and sign"] += 1 if (best_strand and best_norm==-1) else 0
                    line_counter["lines with correct alleles but rsid mismatch/no rsid provided"] += 1 if best_rsid_matches==False else 0
                    line_counter["lines with no changes required"] += 1 if (best_strand==False and best_norm==1 and best_rsid_matches==True) else 0
                    line_counter["lines normalized via dbSNP"] += 1
                    best_source = "dbSNP"
                else:
                    samtools_match = False
                    cluster_start_split = cluster_start.split("\t")
                    
                    ## check against the genomic reference(s)
                    samtools_match, best_ref, best_alt, best_norm, genome_flags = genomic_reference_samtools_check(cluster_start_split, sam_ref1, sam_ref2, A_allele_col, B_allele_col, tested_allele_col, is_snp)
                    if samtools_match:
                        best_source = "genomic reference"
                        best_full_ref, best_full_alt = best_ref, best_alt

                    line_counter["variants not found in dbSNP"] += 1 if best_len_match==False else 0
                    line_counter["lines not normalized"] += 1 if samtools_match==False else 0
                    line_counter["lines normalized via reference genome (missing in dbSNP)"] += 1 if samtools_match else 0
                
                # update this...
                new_base_cols="\t{}\t{}\t{}\t{}".format(best_ref, best_alt, best_full_ref, best_full_alt)
                new_cols_str = new_base_cols+"\t{}\t{}\t{}\t{}\n".format(best_rsid, best_strand, best_norm, cluster_matches)
                new_line = no_first_to_end.replace("\n", "") + new_cols_str
                                       
            else: # if the line is too short, i.e. there was no dbsnp match
                samtools_match = False
                cluster_start_split = cluster_start.split("\t")
                
                ## check against the genomic reference(s)
                samtools_match, best_ref, best_alt, best_norm, genome_flags = genomic_reference_samtools_check(cluster_start_split, sam_ref1, sam_ref2, A_allele_col, B_allele_col, tested_allele_col, is_snp)
                if samtools_match:
                    best_source = "genomic reference"
                    best_full_ref, best_full_alt = best_ref, best_alt
                
                try:
                    cs_no_rsid="|".join([cluster_start_split[i] for i in (0,target_col,A_allele_col,B_allele_col)])
                except TypeError:
                    cs_no_rsid=cluster_start_split[0]+"|"+cluster_start_split[A_allele_col]+"|"+cluster_start_split[B_allele_col]
                
                line = f1.readline()
                line_counter["lines viewed"] += 1
                line_counter["variants not found in dbSNP"] += 1
                line_counter["lines not normalized"] += 1 if samtools_match==False else 0
                line_counter["lines normalized via reference genome (missing in dbSNP)"] += 1 if samtools_match else 0
                
                cluster_next=line
                cluster_next_split=line.split("\t")

                sam_ref1_next = get_next_ref(f3)
                sam_ref2_next = get_next_ref(f4) if (f4 != None) else ""

                try:
                    cn_no_rsid="|".join([cluster_next_split[i] for i in (0,target_col,A_allele_col,B_allele_col)])
                except IndexError:    
                    cn_no_rsid=""
                except TypeError:#skip gene col when fixed (provided as a str)
                    try:
                        cn_no_rsid=cluster_next_split[0]+"|"+cluster_next_split[A_allele_col]+"|"+cluster_next_split[B_allele_col]
                    except IndexError:    
                        cn_no_rsid=""
                
                while (cs_no_rsid == cn_no_rsid): # keep skipping lines if short cluster continues; e.g. same ref,alt,gene but didn't join.
                    try:
                        sam_ref1_next = get_next_ref(f3)
                        sam_ref2_next = get_next_ref(f4) if (f4 != None) else ""
                    
                        line = f1.readline()
                        line_counter["variants not found in dbSNP"] += 1
                        line_counter["lines viewed"] += 1
                        cluster_next_split=line.split("\t")
                        cn_no_rsid="|".join([cluster_next_split[i] for i in (0,target_col,A_allele_col,B_allele_col)])
                    except IndexError:
                        line = f1.readline()
                        cn_no_rsid=""
                    except TypeError:#skip gene col when fixed (provided as a str)
                        try:
                            cn_no_rsid=cluster_next_split[0]+"|"+cluster_next_split[A_allele_col]+"|"+cluster_next_split[B_allele_col]
                        except IndexError:
                            line = f1.readline()
                            cn_no_rsid=""
            
            ## if the alt/ref provided in the rsid don't match exactly, don't use the dbSNP rsid
            if best_source != "dbSNP" or best_novel_alt:
                var_chr, var_pos = cluster_start_split[0].split(":")
                var_pos = str(int(var_pos)-1)
                best_rsid="chr"+var_chr+":"+var_pos+":"+cluster_start_split[A_allele_col]+">"+cluster_start_split[B_allele_col]
                rsid_defined_as_variant=True
            
            ## here is last part of loop, write the saved line
            no_first_to_end = "\t".join(best_cluster_list[1:last_original_col] + [best_cluster_list[0]]) # put the chr:pos column at the back
            new_line = no_first_to_end.replace("\n", "") + "\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\t{}\n".format(best_ref, best_alt,
            best_full_ref, best_full_alt, 
            best_rsid, best_strand, 
            best_norm, cluster_matches if cluster_matches else samtools_match, 
            best_source)

            if (cluster_matches if cluster_matches else samtools_match) == False:
                nonnorm_short = cluster_start_split[0]+":"+cluster_start_split[A_allele_col]+"-"+cluster_start_split[B_allele_col]
                all_nonnorm.add(nonnorm_short)
                try:
                    non_norm_out.write(new_line)
                except NameError:
                    non_norm_out = open(non_norm_path, "w")
                    non_norm_out.write(new_line)
            
            else:
                rsid_fixed = (best_rsid != ("NR" if old_rsid_col=="NR" else best_cluster_list[old_rsid_col])) and \
                    (best_rsid != "NR") and \
                    (best_novel_alt==False) and \
                    best_source=="dbSNP" and \
                    rsid_defined_as_variant==False
                # new rsid is different
                # # new rsid exists
                # both ref and alt matched exactly
                # rsid is only fixed if dbSNP used, not genomic reference

                new_thing_to_write=no_first_to_end.replace("\n", "")+"\tnormalizing_source="+best_source
                
                ### flag section
                flags = []
                flags.append("EFFECT_STATS_SIGN_CHANGED") if best_norm==-1 else None
                flags.append("BOTH_ALLELES_MATCH_REFERENCE_GENOME") if "BOTH_ALLELES_MATCH_REFERENCE_GENOME" in genome_flags else None
                flags.append("ON_NEGATIVE_STRAND") if best_strand else None
                flags.append("NEW_RSID_MAPPED") if rsid_fixed else None
                flags.append("RSID_NOT_VERIFIED_THROUGH_EXACT_MATCH") if (rsid_fixed==False and best_rsid != (best_cluster_list[old_rsid_col] if old_rsid_col!="NR" else "NR") and best_novel_alt==False) else None # rsid provided but couldn't be matched
                flags.append("INEXACT_ALLELE_MATCH") if (rsid_fixed==False and best_rsid != (best_cluster_list[old_rsid_col] if old_rsid_col!="NR" else "NR") and best_novel_alt) else None # rsid provided but couldn't be matched
                flags.append("NOVEL_ALT") if best_novel_alt else None
                new_thing_to_write += ";" if len(flags)>0 else ""
                new_thing_to_write += ",".join(flags)
                
                ### new information to be extracted
                new_thing_to_write += ";new_rsid="+best_rsid if rsid_fixed else ""
                new_thing_to_write += ";new_variant_id="+best_rsid if rsid_defined_as_variant else ""
                new_thing_to_write += ";provided_rsid="+("NR" if old_rsid_col=="NR" else best_cluster_list[old_rsid_col]) if (rsid_fixed or rsid_defined_as_variant) else ""
                
                if (best_ref != best_cluster_list[A_allele_col]): # if assigned differs from input ref/alt, update.
                    new_thing_to_write += ";new_ref="+best_ref
                if (best_alt != best_cluster_list[B_allele_col]):
                    new_thing_to_write += ";new_alt="+best_alt

                new_thing_to_write += ";source_ref_full="+best_full_ref if (best_ref != best_full_ref) or ("INEXACT_ALLELE_MATCH" in flags) else ""
                new_thing_to_write += ";dbSNP_alt_full="+best_full_alt if (best_alt != best_full_alt) or ("INEXACT_ALLELE_MATCH" in flags) else ""
                new_thing_to_write += ";dbSNP_rsid="+best_rsid if ("INEXACT_ALLELE_MATCH" in flags and rsid_defined_as_variant == False) else ""
                new_line=new_thing_to_write+"\n"
                
                f2.write(new_line)
    
    ### print a summary at the end
    single_counters = ["lines normalized via dbSNP", "lines normalized via reference genome (missing in dbSNP)", "lines not normalized"]
    double_counters = [ i for i in line_counter.keys() if not i in single_counters ]
    new_sum = sum([line_counter[k] for k in single_counters])
    for k in single_counters:
        print("{} {}".format(line_counter[k], k))
    print("---")
    for k in double_counters:
        print("{} {}".format(line_counter[k], k))    
        
    print("---")
    print("Lines in outputs: {}".format(new_sum))
    print("Unique non-normalized variants: {}".format(len(all_nonnorm)))
    print("---\n")
    total_normalized = line_counter["lines normalized via dbSNP"] + line_counter["lines normalized via reference genome (missing in dbSNP)"]
    print("{0}/{1} lines normalized. ({2:.4f}%)".format(total_normalized, new_sum, 100*total_normalized/new_sum))
    print("{0}/{1} lines normalized via dbSNP. ({2:.4f}%)".format(line_counter["lines normalized via dbSNP"], new_sum, 100*line_counter["lines normalized via dbSNP"]/new_sum))
    print("{0}/{1} lines normalized via reference genome. ({2:.4f}%)".format(line_counter["lines normalized via reference genome (missing in dbSNP)"], new_sum, 100*line_counter["lines normalized via reference genome (missing in dbSNP)"]/new_sum))
    
    try:
        non_norm_out.close()
    except NameError:
        pass
    