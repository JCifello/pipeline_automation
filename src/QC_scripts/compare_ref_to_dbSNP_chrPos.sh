#!/bin/bash
set -e

SCRIPT_DESCRIPTION="compare_ref_to_dbSNP_chrPos.sh: Join the input data with the dbSNP dataset specified in the input table. Remove degenerate variant_id's. NOTE: Input data must be a single variant type (SNP/INDEL). Variant_id column of final output will originate from this dbSNP lookup. Dependent scripts: evaluate_lines.py."

in_dat=$1
scripts_dir=$2
tag=$3
output_dir=$4
dbSNP_ref=$5
in_pos_col=$6
target_col=$7
allele1_col=$8
allele2_col=$9
old_rsid_col=${10}
tested_allele_col=${11}
other_allele_col=${12}
ref_allele_col=${13}
alt_allele_col=${14}
force_run=${15}
samtools_path=${16}
reference_genome_path=${17}
multiple_var_types=${18}

testing_evaluate="false"
if [ $testing_evaluate == "false" ]; then
	echo -e "\nJoining input data with ${dbSNP_ref}, the variant reference set.\n"
	LC_ALL=C join  -t $'\t' -1 $in_pos_col -2 8 -a 1 -e NOT_FOUND $in_dat $dbSNP_ref > $output_dir/.tmp_snps_ref_joined_${tag}.bed
fi

## if chr_colon_col occurs after ref and alt, 1 needs to be added to the ref/alt col numbers. Otherwise keep the same. 
# do this because chr_colon_col moves to the beginning after join.
for var in allele1_col allele2_col old_rsid_col target_col tested_allele_col other_allele_col ref_allele_col alt_allele_col; do
  # check if variable is not equal to "NR" and whether it got pushed behind chr:position column in join above
  if [[ ${!var} =~ ^[0-9]+$ && "$in_pos_col" -gt "${!var}" ]]; then
    ((++$var)) 
  fi
done

## get the regions file
if [ $testing_evaluate == "false" ]; then
	echo "Getting regions from the joined input file."
	LC_ALL=C awk 'BEGIN{FS="\t"}{ pos_start=$1; split(pos_start, split_pos, ":"); end_pos=split_pos[2]+length($'$allele1_col')-1
		print "chr"pos_start"-"end_pos}' $output_dir/.tmp_snps_ref_joined_${tag}.bed > $output_dir/.tmp_regions_for_samtools_${tag}_1.bed

	echo "Carrying out region query against $reference_genome_path using region file(s)."
	$samtools_path faidx $reference_genome_path -r $output_dir/.tmp_regions_for_samtools_${tag}_1.bed > $output_dir/.tmp_samtools_ref_seqs_${tag}_1.bed
	
	if [ "$ref_allele_col" == "NR" ] && [ "$multiple_var_types" == "true" ] && [ "$(echo $in_dat | rev | cut -d '_' -f 1 | rev)" == "OTHER.bed" ]; then
		echo "Getting regions from the joined input file (ALLELE 2)."
		LC_ALL=C awk 'BEGIN{FS="\t"}{ pos_start=$1; split(pos_start, split_pos, ":"); end_pos=split_pos[2]+length($'$allele2_col')-1
			print "chr"pos_start"-"end_pos}' $output_dir/.tmp_snps_ref_joined_${tag}.bed > $output_dir/.tmp_regions_for_samtools_${tag}_2.bed
		echo "Carrying out region query against $reference_genome_path using region file(s) (ALLELE 2)."
		$samtools_path faidx $reference_genome_path -r $output_dir/.tmp_regions_for_samtools_${tag}_2.bed > $output_dir/.tmp_samtools_ref_seqs_${tag}_2.bed
	fi 
fi

# get the total number of joined columns. This can inform final column positions. 
num_prejoin_cols=$(head -n 2 $in_dat | tail -n +2 | awk 'BEGIN{FS="\t"}{print NF}')
num_dbSNP_cols=$(head -n 1 $dbSNP_ref | awk 'BEGIN{FS="\t"}{print NF}')
joined_total_cols=$(echo "${num_prejoin_cols}+${num_dbSNP_cols}-1" | bc)

let new_ref_col=joined_total_cols-2

echo Remove duplicates following the join...

python_flags=""
if [ "$tested_allele_col" != "NR" ]; then
	python_flags=" --tested_allele_col $tested_allele_col"
fi
if [ "$other_allele_col" != "NR" ]; then
	python_flags="$python_flags --other_allele_col $other_allele_col"
fi
if [ "$ref_allele_col" != "NR" ]; then
	python_flags="$python_flags --ref_allele_col $ref_allele_col"
fi
if [ "$alt_allele_col" != "NR" ]; then
	python_flags="$python_flags --alt_allele_col $alt_allele_col"
fi
if [ "$force_run" == "true" ]; then
	python_flags="$python_flags --force_run"
fi

$python_use $scripts_dir/QC_scripts/evaluate_lines.py $output_dir/.tmp_snps_ref_joined_${tag}.bed \
$output_dir/.tmp_samtools_ref_seqs_${tag}_1.bed \
$output_dir/.tmp_samtools_ref_seqs_${tag}_2.bed \
$output_dir/.tmp_new_refs_alts_${tag}.bed \
$target_col \
$allele1_col \
$allele2_col \
$old_rsid_col \
$new_ref_col \
$joined_total_cols \
$python_flags

if [ $testing_evaluate == "false" ]; then
	if [ $? == 0 ]; then
		rm $output_dir/.tmp_snps_ref_joined_${tag}.bed
		rm $output_dir/.tmp_regions_for_samtools_${tag}_1.bed
		rm $output_dir/.tmp_samtools_ref_seqs_${tag}_1.bed
		if [ "$ref_allele_col" == "NR" ]; then
			rm $output_dir/.tmp_regions_for_samtools_${tag}_2.bed
			rm $output_dir/.tmp_samtools_ref_seqs_${tag}_2.bed
		fi
	fi
fi
