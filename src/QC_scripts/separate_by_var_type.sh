#!/bin/bash
set -e

SCRIPT_DESCRIPTION="1.0_separate_by_var_type.sh: Scan the number of characters in the input allele columns to separate SNP records from other types of records. If both alleles are only one character, this is saved as a SNP. This step ensures that mapping to dbSNP is fast and accurate. This will be skipped if MULTIPLE_VARIANT_TYPES in the file config is False."
input_dat=$1
tag=$2
allele_1_col=$3
allele_2_col=$4
output_dir=$5

echo ""

echo -n "" > "${output_dir}/.unzipped_input_w_chr_pos_${tag}_SNP.bed"
echo -n "" > "${output_dir}/.unzipped_input_w_chr_pos_${tag}_OTHER.bed"

LC_ALL=C awk 'BEGIN{FS="\t";OFS="\t";snp_count=0;
	indel_count=0;total_count=0
	}{A1=$'$allele_1_col';A2=$'$allele_2_col'; 
	total_count=total_count+1
	if ((length(A1)==1)&&(length(A2)==1)) {
		print $0 >> "'$output_dir/.unzipped_input_w_chr_pos_${tag}_SNP.bed'";
		snp_count = snp_count+1
	} else {
		print $0 >> "'$output_dir/.unzipped_input_w_chr_pos_${tag}_OTHER.bed'";
		indel_count = indel_count+1
	}
	} END { print "Total lines:",total_count
	print "SNP lines:",snp_count 
	print "INDEL lines:",indel_count 
	}' $input_dat
