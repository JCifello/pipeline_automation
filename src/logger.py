import logging, sys, os

def handle_same_file(output_path, curr_date):
    """Get the run log date from the previous file. Attempt to create a new output directory for it, and move it there."""
    
    f = open(output_path, "r")
    all_lines = f.readlines()
    date_line = [line for line in all_lines if "hipFG run-date" in line]
    try:
        new_date_key = date_line[0].split("=")[-1].rstrip()
    except IndexError:
        new_date_key = curr_date # if a date wasn't saved before, that means it errored out. Just replace errored-out logs. 

    if new_date_key != curr_date:
        # if dates differ, move old version to a new folder. 
        new_folder = os.path.join(os.path.dirname(output_path), "v"+new_date_key.replace(" ","_").replace(":","-"))

        if not os.path.exists(new_folder):
            os.mkdir(new_folder)
            
        if not os.path.exists(os.path.join(new_folder, os.path.basename(output_path))):
            os.rename(output_path, os.path.join(new_folder, os.path.basename(output_path)))

def setup_logger(output_path, scr_name, curr_date):
    ## Establish the logger to write to stdout + a log file.
    # Create a logger
    my_logger = logging.getLogger()
    
    my_logger.handlers = []

    my_logger.setLevel(logging.DEBUG)

    # Create a file handler to log to a file
    if (os.path.exists(output_path)) and (scr_name == "filer_xqtl.py"):
        handle_same_file(output_path, curr_date)

    file_handler = logging.FileHandler(output_path)
    file_handler.setLevel(logging.INFO)

    # Create a stream handler to log to stdout
    stream_handler = logging.StreamHandler(sys.stdout)
    stream_handler.setLevel(logging.INFO)

    # Create a formatter to specify the log format
    # log_format = "%(name)s - %(asctime)s - %(levelname)s - %(message)s"
    log_format = "{}\t %(asctime)s - %(levelname)s - %(message)s".format(scr_name)
    date_format = "%m/%d/%y - %H:%M:%S"

    formatter = logging.Formatter(log_format, datefmt=date_format)

    # Set the formatter for the handlers
    file_handler.setFormatter(formatter)
    stream_handler.setFormatter(formatter)

    # Add the handlers to the logger
    my_logger.addHandler(file_handler)
    my_logger.addHandler(stream_handler)

    return my_logger
