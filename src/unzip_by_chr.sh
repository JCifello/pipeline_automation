#!/bin/bash

SCRIPT_DESCRIPTION="unzip_by_chr.sh: Take the raw input file and unzip into the specified temp directory, separating chromosomes across different files. Chromosomes are separated for faster run in parallel of single samples. If it is specified to have a header, remove it during the unzipping."

input_file=$1
tag=$2
temp_dir=$3
has_header=$4
chr_col=$5

has_header=$(echo $has_header |  tr '[:upper:]' '[:lower:]')

test_zipped=$(file $input_file | cut -d " " -f 2- | grep "gzip compressed data" -c)

if [ $test_zipped -eq 1 ]; then
	if [ $has_header = "t" ] || [ $has_header = "true" ]; then
		gunzip -c $input_file | sed 's/\r//g' | LC_ALL=C awk 'BEGIN{FS="\t"} NR==2 { split($'$chr_col',col_split,"r"); if (col_split[1]!="ch") { chr_prefix="chr" } else { chr_prefix="" } }
		{chr=$'$chr_col'; if (NR>1) {print $0 > "'$temp_dir'/.tmp_unzipped_input_'$tag'_"chr_prefix chr".bed"}}'
	else
		gunzip -c $input_file | sed 's/\r//g' | LC_ALL=C awk 'BEGIN{FS="\t"} NR==1 { split($'$chr_col',col_split,"r"); if (col_split[1]!="ch") { chr_prefix="chr" } else { chr_prefix="" } }
		{chr=$'$chr_col'; print $0 > "'$temp_dir'/.tmp_unzipped_input_'$tag'_"chr_prefix chr".bed"}'
	fi
else
	if [ $has_header = "t" ] || [ $has_header = "true" ]; then
		sed 's/\r//g' $input_file | LC_ALL=C awk 'BEGIN{FS="\t"} NR==2 { split($'$chr_col',col_split,"r"); if (col_split[1]!="ch") { chr_prefix="chr" } else { chr_prefix="" } }
		{chr=$'$chr_col'; if (NR>1) {print $0 > "'$temp_dir'/.tmp_unzipped_input_'$tag'_"chr_prefix chr".bed"}}'
	else
		sed 's/\r//g' $input_file | LC_ALL=C awk 'BEGIN{FS="\t"} NR==1 { split($'$chr_col',col_split,"r"); if (col_split[1]!="ch") { chr_prefix="chr" } else { chr_prefix="" } }
		{chr=$'$chr_col'; print $0 > "'$temp_dir'/.tmp_unzipped_input_'$tag'_"chr_prefix chr".bed"}'
	fi
fi

