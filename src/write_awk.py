"""
Take in a manually curated column mapping file. Generate an appropriate awk 
    command to create a final file with chr;chrStart;chrEnd;name;non_ref_af;strand;ref;alt;gene;gtex_attributes. 

Check for any missing columns, and throw error if necessary. Return a list with flags indication which scripts should be run.
    Scripts may be required and determined via this script even if an awk script is never written or used. 

Also, throw errors if the types seem incorrect.
"""
import argparse, os, re, sys, gzip
import logger

def get_config_idx(header_list, cfg_dict):
    """
    Using the selected column names and return the column indices.
    Generates a dictionary associating each declared column with the number of its column.
    """
    ## in making sure all necessary columns are assigned to legitimate fields, pass over ones that are expected and not necessarily
    ## associated with input fields. 
    ## Also: allow up to 100 split columns (arbitrary)
    expected_descriptors = ["bed required-descriptors", "bed optional-descriptors"] + \
        cfg_dict["bed required-descriptors"].split(";") + cfg_dict["bed optional-descriptors"].split(";") + \
        [ "SPLIT_CHARACTERS_"+str(i) for i in range(1,100) ]


        ## if interact section
    header_dict={}

    for cfg_key in cfg_dict.keys():
        input_col_name = cfg_dict[cfg_key]

        if (input_col_name == "") and not (cfg_key in expected_descriptors):
            my_logger.info("Column {} left not assigned in new data.".format(cfg_key))
            continue
        if cfg_key in ("sep", "bed name", "bed schema", "bed required", "bed optional"):
            continue
        found_idx = [ i for i in range(len(header_list)) if (header_list[i]==input_col_name) ]
        if len(found_idx)!=0:
            header_dict[cfg_key]=found_idx[0]+1
        else:
            ## manually allow for times non-existent columns are acceptable.
            if (len(found_idx)==0) and not (cfg_key in expected_descriptors) and not ("-split" in input_col_name) \
                    and not (input_col_name=="ALL_COLUMNS") \
                    and not (re.search("\".*\"", input_col_name)!=None):
                my_logger.error("Column assigned to non-existent new field: {} was assigned to {}.".format(cfg_key, input_col_name))
                sys.exit(1)

            if re.search("\".*\"", input_col_name) != None:
                my_logger.warning("Static column inserted! {} assigned to {}.".format(cfg_key, input_col_name))

    if cfg_dict.get("BED_FORMAT") != None:
        my_schema = cfg_dict["bed schema"].split(";")
        header_dict = {val:(idx+1) for idx, val in enumerate(my_schema) }
    
    return header_dict

def get_chr(example_list, header_idx_dict, config_dict, use_key="chrom"):
    """Look at the example data and find out whether the chromosome includes 'chr.'"""
    if use_key=="chrom":
        chr_col=header_idx_dict.get("chr", header_idx_dict.get("chrom",header_idx_dict.get("sourceChrom", "NR")))
    else:
        chr_col=header_idx_dict.get(use_key, "NR")
    chr_example=example_list[chr_col-1]
    try:
        as_int=int(chr_example)
        return True
    except ValueError:
        pass
    
    return False

def detect_cases(awk_dict, config_dict, header_list, input_df, is_simple_mdt):
    """
    Based on the bed-format, determine which columns are missing. There are specific cases of columns that may be missing.
    After the first pass of the awk script has been determined (in fn write_awk) take that script and make changes to include:
    * changes to variables (such as adding text to a chromosome name)
    * a calculation of a variable (such as beta from beta.se and z-score)
    * adding a new variable (such as priming for including a new column)
    """
    ## get the maximum number used in the current awk script.
    max_col = len(header_list)
    scripts_req=[]
    new_awk_dict = dict(awk_dict)
    ## replace text for specific circumstances.
    for chr_end in ["chrEnd", "chromEnd"]:
        if chr_end in awk_dict:
            if awk_dict[chr_end]=="NR":
                my_logger.info("Adding 1 for {}, since it is undefined!".format(chr_end))
                new_awk_dict[chr_end]="chrStart+1" if chr_end=="chrEnd" else "chromStart+1"
    
    if config_dict["bed name"] == "bed3+17 qtl":
        ## once a combination is specified, A1 and A2 must be different. ref and alt must be different. 
        if new_awk_dict["tested_allele"] != "NR":
            if new_awk_dict["tested_allele"] == new_awk_dict["other_allele"]:
                my_logger.error("'tested_allele' and 'other_allele' cannot be the same column for QTLs!")
                sys.exit(1)
        
        if new_awk_dict.get("A1", "NR") != "NR":
            if new_awk_dict["A1"] == new_awk_dict["A2"]:
                my_logger.error("'A1' and 'A2' cannot be the same column for QTLs!")
                sys.exit(1)
        
        if new_awk_dict["ref"] != "NR":
            if new_awk_dict["ref"] == new_awk_dict["alt"]:
                my_logger.error("'ref' and 'alt' cannot be the same column for QTLs!")        
                sys.exit(1)
            if new_awk_dict["tested_allele"] == new_awk_dict["other_allele"]:
                my_logger.error("The 'tested' allele cannot be the same as the 'other' allele.")
                sys.exit(1)

        imputed, imputed_stat=False, None
        if new_awk_dict["z_score_non_ref"] == "NR": # beta_se=beta/z-score
            imputed, imputed_stat = True, "z-score"
            new_awk_dict["z_score_non_ref"] = "{}/${}".format(int(new_awk_dict["beta_non_ref"]), int(new_awk_dict["beta_se_non_ref"]))
        if new_awk_dict["beta_non_ref"] == "NR": 
            imputed, imputed_stat = True, "beta"
            new_awk_dict["beta_non_ref"] = "{}*${}".format(int(new_awk_dict["z_score_non_ref"]), int(new_awk_dict["beta_se_non_ref"]))
        if new_awk_dict["beta_se_non_ref"] == "NR":
            imputed, imputed_stat = True, "beta_se"
            new_awk_dict["beta_se_non_ref"] = "{}/${}".format(int(new_awk_dict["beta_non_ref"]), int(new_awk_dict["z_score_non_ref"]))
        
        if imputed:    
            all_stats=("z-score","beta","beta_se")
            my_logger.info("Autocalculating {} in QTLs from {} and {}.".format(imputed_stat, [i for i in all_stats if i!=imputed_stat][0], [i for i in all_stats if i!=imputed_stat][1]))
    
        ## account for any columns to be split:
        splitter_fields = [ i for i in config_dict.keys() if "SPLIT_CHARACTERS" in i ]
        
        # want to rank these by appearance in input data...
        ordered_outfields={}
        if len(splitter_fields)>0: # determine how many columns are to be derived from splitting columns, and account for those additional columns here. 
            scripts_req.append("position_splitter")
            
            for header_col in header_list:
                in_fields_using_col = [ k for k in config_dict.keys() if header_col+"-split" in str(config_dict[k]) ]
                out_fields_using_col = [ k for k in in_fields_using_col if str(config_dict[k]).index(header_col+"-split")==0 ]
                in_fields_using_col = [ config_dict[k] for k in in_fields_using_col if str(config_dict[k]).index(header_col+"-split")==0 ] # making sure this isn't matching a column that fits inside another
                
                if len(out_fields_using_col):
                    split_nums = [ i[i.index("-split")+6:] for i in in_fields_using_col ]
                    
                    out_to_num_map = { out_fields_using_col[i]:split_nums[i] for i in range(len(split_nums)) }
                    split_nums = set([ int(i) for i in split_nums ])
                    split_nums = sorted(list(split_nums))
                    ## then go through each variable assigned to the column and subcolumn combination, assign awk indexes in ascending order. 
                    for out_idx in split_nums:
                        out_fields_with_num = [ i for i in out_to_num_map.keys() if out_to_num_map[i]==str(out_idx) ]
                        for out_fields in out_fields_with_num:
                            new_awk_dict[out_fields] = str(max_col+1)
                        max_col += 1
                        ordered_outfields[max_col] = out_fields_with_num

        config_dict["splitting_fields"] = ordered_outfields
        
        if not "chr_colon_pos" in new_awk_dict.keys():
            scripts_req.append("chr_colon_pos")
            new_awk_dict["chr_colon_pos"] = str(max_col+1)
            max_col += 1

        tested_and_other = (config_dict["tested_allele"] != "NR") and (config_dict.get("other_allele", "NR") != "NR")
        ref_alt_other = (config_dict.get("ref","NR") != "NR") and (config_dict.get("alt","NR") != "NR") and (config_dict["tested_allele"] != "NR")
        a1_a2_tested = (config_dict.get("A1","NR") != "NR") and (config_dict.get("A2","NR") != "NR") and (config_dict["tested_allele"] != "NR")

        if not (tested_and_other or ref_alt_other or a1_a2_tested):
            my_logger.error("QTL data must have ref+alt+tested_allele, A1+A2+tested_allele, or tested_allele+other_allele assigned.")
            sys.exit(1)

        # # add columns for dbSNP and HGNC information. This assumes that the dbSNP joining happens before HGNC joining.
        new_awk_dict["QC_info"] = str(max_col+1)
        max_col += 1
        scripts_req.append("dbSNP")
    
        # additional xQTL adjustments: always use the looked up variant, ref, alt, and strand. Also update signs of statistics using these. 
        new_awk_dict["og_ref"] = new_awk_dict["ref"]
        new_awk_dict["og_alt"] = new_awk_dict["alt"]
        

        if (config_dict.get("LOOKUP_COL")!=None) and (config_dict.get("LOOKUP_TYPE")!=None):
            new_awk_dict["target_strand"] = str(max_col+1) # +1 to get first of HGNC section. 
            max_col += 1 # increment, so target_strand is not included in target_info section.
            
            if new_awk_dict.get("target_gene_symbol", "NR") != "NR":
                target_cols = ["target_gene_coord"]
                new_awk_dict["target_info"] = "(\"" + " \";".join([ "{}=\" ${}".format(target_cols[i-max_col], str(i+1)) for i in range(max_col, max_col+len(target_cols))]) + " target_flag)"
                new_awk_dict["qtl_dist_to_target"] = str(max_col+2)
                new_awk_dict["target_ensembl_id"] = str(max_col+3)
                new_awk_dict["target_flag"] = str(max_col+4)
                max_col += 4
                scripts_req.append("HGNC")
            else:
                target_cols = ["target_gene_coord", "target_gene_symbol"]
                new_awk_dict["target_info"] = "(\"" + " \";".join([ "{}=\" ${}".format(target_cols[i-max_col], str(i+1)) for i in range(max_col, max_col+len(target_cols))]) + " target_flag)"
                new_awk_dict["target_gene_symbol"] = str(max_col+2)
                new_awk_dict["qtl_dist_to_target"] = str(max_col+3)
                new_awk_dict["target_ensembl_id"] = str(max_col+4)
                new_awk_dict["target_flag"] = str(max_col+5)
                max_col += 5
                scripts_req.append("HGNC")
                scripts_req.append("target_gene_symbol")
                
                
            if config_dict.get("TARGET_HAS_SPAN","f").lower() in ("t", "true"):
                # prepare target chr and positions
                if config_dict["TARGET_CHROM_IS_NUM"]:
                    new_awk_dict["TARGET_CHROM"] = "\"chr\" $"+new_awk_dict["TARGET_CHROM"]
                if str(config_dict["IS_BASE_ONE"]).lower() in ("t", "true"):
                    new_awk_dict["TARGET_START"] = new_awk_dict["TARGET_START"] + "-1"
                if new_awk_dict["TARGET_END"]=="NR":
                    new_awk_dict["TARGET_END"] = "TARGET_START+1"
            
                if config_dict.get("TARGET_CHROM") == None or config_dict.get("TARGET_START") == None:
                    my_logger.error("To include the target's span please specify TARGET_CHROM and TARGET_START.")
                    sys.exit(1)
                
                # add the provided explicit span to the target info. Remove target_flag and add to the end.
                new_awk_dict["target_info"] = new_awk_dict["target_info"][:-13] + " \";target_coord=\" TARGET_CHROM \":\" TARGET_START \"-\" TARGET_END target_flag)" 
                
        if config_dict.get("LOOKUP_TARGET_SPAN","f").lower() in ("t", "true"):
            if config_dict.get("TARGET_CHROM")==None:
                my_logger.error("To look up the target's span please specify TARGET_CHROM.")
                sys.exit(1)
            if config_dict.get("TARGET_START")==None:
                my_logger.error("To look up the target's span please specify TARGET_START.")
                sys.exit(1)
 
            # edit the target if gene information is provided.
            if config_dict["TARGET_CHROM_IS_NUM"]:
                new_awk_dict["TARGET_CHROM"] = "\"chr\" $"+new_awk_dict["TARGET_CHROM"]
            
            if str(config_dict["IS_BASE_ONE"]).lower() in ("t", "true"):
                new_awk_dict["TARGET_START"] = new_awk_dict["TARGET_START"] + "-1"
            if new_awk_dict["TARGET_END"]=="NR":
                new_awk_dict["TARGET_END"] = "TARGET_START+1"
            if not (config_dict.get("TARGET_HAS_SPAN", "false").lower() in ("t", "true")):
                new_awk_dict["new_target"] = "(TARGET_CHROM \":\" TARGET_START \"-\" TARGET_END \":\" target)"
            
            new_awk_dict["target_strand"] = str(max_col+1) # +1 to get first of gene section output. 
            max_col += 1
            if config_dict.get("TARGET_END") in (None,""):
                my_logger.warning("No TARGET_END detected! Using the start position + 1.")
    
            target_cols = ["target_gene_coord", "target_gene_symbol"]
            new_awk_dict["target_info"] = "(\"" + " \";".join([ "{}=\" ${}".format(target_cols[i-max_col], str(i+1)) for i in range(max_col, max_col+len(target_cols))]) + "\";"
            new_awk_dict["target_info"] += "target_coord=\" TARGET_CHROM \":\" TARGET_START \"-\" TARGET_END target_flag)" # must be defined after target chrom, start, end!! 
            new_awk_dict["target_gene_coord"] = str(max_col+1)
            new_awk_dict["target_gene_symbol"] = str(max_col+2)
            new_awk_dict["target_ensembl_id"] = str(max_col+3)
            new_awk_dict["qtl_dist_to_target"] = str(max_col+4)
            new_awk_dict["target_flag"] = str(max_col+5)

            max_col += 5
            scripts_req.append("genome_partition")
    
        # add specific columns if needed
        if new_awk_dict.get("FDR", "NR") == "NR" and config_dict.get("CALC_FDR", "true").lower() in ("t", "true"):
            new_awk_dict["FDR"] = str(max_col+1)
            max_col += 1
            scripts_req.append("FDR")
        elif (new_awk_dict.get("FDR", "NR") == "NR") and config_dict.get("CALC_FDR", "true").lower() in ("f", "false"):
            new_awk_dict["FDR"] = "\".\""
    
    if config_dict["bed name"] == "bed4+19 interact":
        if not ("value_description" in input_df.keys()) and is_simple_mdt=="False":
            my_logger.error("\"value description\" must be provided for bed4+19 interact data! E.g., peakachu probability or interaction strength.")
            sys.exit(1)

        if awk_dict.get("score","NR")=="NR":
            if not (config_dict.get("SCORE_STRATEGY", "NR") in ("log", "multiply")):
                my_logger.error("SCORE_STRATEGY must be provided to obtain score from value. Options: log, multiply.")
                sys.exit(1)
            if config_dict["SCORE_STRATEGY"]=="log":
                my_logger.info("Using score strategy=log: score = -log(value). This is typically used when value is a pvalue or qval.")
            if config_dict["SCORE_STRATEGY"]=="multiply":
                my_logger.info("Using score strategy=log: score = value*1000. This is used when value is a confidence in binding, e.g., RF score.")

        # adding new values to new_awk_dict
        vals_to_get = ["input_dat", "name", "score", "value", "exp", "color", "sourceName", "sourceStrand", "targetName", "targetStrand", "gene_A", "gene_B"]
        missing_vals = [ i for i in vals_to_get if awk_dict.get(i, "NR")=="NR"]
        additions_to_awk_dict = { val:str((max_col+idx)) for idx, val in enumerate(missing_vals) }
        additions_to_awk_dict["cell_type"] = input_df.get("cell_type", "NA")
        additions_to_awk_dict["data_source"] = input_df.get("source_name", input_df.get("data_source", "NA"))
        additions_to_awk_dict["SCORE_STRATEGY"] = config_dict.get("SCORE_STRATEGY", "NR")
        max_col += len(missing_vals)
        new_awk_dict.update(additions_to_awk_dict)

        ## add custom fields to interactionAttr field. 
        new_labels=";".join(["\"{}=\"${}".format(k.replace("attr_label_", ""), new_awk_dict[k]) for k in awk_dict.keys() if "attr_label" in k])
        new_labels += "\";\"" if new_labels != "" else ""
        new_awk_dict["interactionAttr"]="("+new_labels+"\"sourceGene=\"$"+new_awk_dict["gene_A"]+"\";targetGene=\"$"+new_awk_dict["gene_B"]+")"
        new_awk_dict["interactionAttr"]=new_awk_dict["interactionAttr"].replace("\"\"","")

        # make sure A and B are specified in individual columns. If not, this cannot be helped. 
        non_na_interactions = [ new_awk_dict[i] for i in ["sourceChrom","sourceStart","sourceEnd","targetChrom","targetStart","targetEnd"] if new_awk_dict[i] != "NR" ]
        if len(non_na_interactions) < 6:
            my_logger.error("Interaction data must have interaction coords for A and B specified!")
            sys.exit(1)
        # check A and B are specified in a set of interchanging columns. If not, throw flag to run reformatting script.
        non_na_split_interactions =  [ new_awk_dict[i] for i in ["anchorChrom", "anchorStart", "anchorEnd"] if new_awk_dict[i] != "NR" ]
        if len(non_na_split_interactions) < 3:
            my_logger.info("Throwing flag for interaction data reformatting.")
            scripts_req.append("reformat_interactions")
        
    if config_dict["CHR_IS_NUM"]:
        #  add text to chr section in awk
        new_awk_dict["chrom"] = "\"chr\" $" + awk_dict["chrom"]
    
    if config_dict["IS_BASE_ONE"].lower() in ["t", "true"]:
        for chr_start in ("chrStart", "chromStart"):
            if chr_start in new_awk_dict: # determining whether to use chrStart or chromStart.
                if "-split" in config_dict.get(chr_start,""):
                    continue
                    
                new_awk_dict[chr_start] = new_awk_dict[chr_start]+"-1"
                chr_end = "chrEnd" if chr_start == "chrStart" else "chromEnd"
                # if new_awk_dict[chr_end] not in ("chrStart+1", "chromStart+1"):
                    # new_awk_dict[chr_end] = new_awk_dict[chr_end]+"-1"
        
    new_awk_dict = {i:new_awk_dict[i].replace("NR", "\"NR\"") for i in new_awk_dict.keys() }

    return new_awk_dict, scripts_req, config_dict
    

def write_awk_via_dict(dict_to_use, config_dict, input_df, full_header_dict):
    """
    Using the dictionary generated by the config file and detect_cases, write the awk script.
    """
    if config_dict["bed name"] == "bed3+17 qtl":
        ordered_keys = [ i for i in dict_to_use.keys() if ((i=="chromStart") or ("og" in i) or (i=="normalizing_factor")) and (i!="target_info") ] + [ i for i in dict_to_use.keys() if ((i!="chromStart") and ("og" not in i) and (i!="normalizing_factor")) and (i!="target_info")] + ["target_info"]
    if config_dict["bed name"] == "bed4+19 interact":
        ordered_keys = [ i for i in dict_to_use.keys() if not (i in ("cell_type", "data_source")) ]
    if (config_dict["bed name"] in ("bed3+17 qtl", "bed4+19 interact"))==False:
        ordered_keys = [ i for i in dict_to_use.keys() ]
    
    cols_section = "; ".join([ "{}=${}".format(i, dict_to_use[i]) for i in ordered_keys ])
    cols_section = cols_section.replace("$(", "(")
    cols_section = cols_section.replace(";;", ";")
    cols_section = cols_section.replace("$NA", "\"NA\"")

    if config_dict["bed name"] == "bed3+17 qtl":
        ## to calculate non_ref_af, if not explicitly provided, must have AC and AN.
        if config_dict.get("CALC_AF","f").lower() in ["t", "true"]:
            if not (("ac" in full_header_dict.keys()) and ("an" in full_header_dict.keys())):
                my_logger.error("For hipFG to calculate the minor allele frequency, the minor allele count (AC) and total allele number (AN) must be provided.")
                sys.exit(1)
            dict_to_use["non_ref_af"]="{}/${}".format(full_header_dict["ac"], full_header_dict["an"]) # not necessarily required columns
        
        if (config_dict.get("PVALUE_STRATEGY","NR")=="raise10"):
            dict_to_use["pval"] = "(10^(${}))".format(dict_to_use["pval"])

        ## get indexes for dbSNP values
        cols_section = "; ".join([ "{}=${}".format(i, dict_to_use[i]) for i in ordered_keys ])
        cols_section = cols_section.replace("$(", "(")
        cols_section = cols_section.replace(";;", ";")
        cols_section = cols_section.replace("$NA", "\"NA\"")

        awk_script="""#!/bin/script
SCRIPT_DESCRIPTION="This awk script is generated via hipFG_root/src/write_awk.py. It functions to rearrange columns to match a desired output. For QTLs, it additionally will ensure test statistics and allele frequencies always correspond to the test allele."

echo -n "" > $2
LC_ALL=C awk 'BEGIN{{FS="\\t";OFS="\\t"}}{{{}
{}{}
if (QC_info ~ /EFFECT_STATS_SIGN_CHANGED/) {{
    beta_non_ref = -1 * beta_non_ref
    z_score_non_ref = -1 * z_score_non_ref{}
}}

# get the new_ref
if (match(QC_info,/new_ref=[ACGT,]+/)){{
    ref=substr(QC_info,RSTART+8,RLENGTH-8)
}}
# get the new_alt
if (match(QC_info,/new_alt=[ACGT,]+/)){{
    alt=substr(QC_info,RSTART+8,RLENGTH-8)
}}

# get the new rsid
if (match(QC_info,/new_rsid=rs[0-9]+/)){{
    variant_id=substr(QC_info,RSTART+9,RLENGTH-9)
}}

## save a default variant identifier if no valid rsid is selected
if (QC_info ~ /new_variant_id/) {{
    variant_id=(chrom ":" chromStart "-" chromEnd ":" ref ">" alt)
}}

print {}}}' $1 >> $2
""".format(cols_section, # definitions at the top of the awk file
    "\nref=A1\nalt=A2" if dict_to_use["ref"]=="\"NR\"" else "",
    "\nnon_ref_af=EAF" if ("EAF" in full_header_dict.keys()) else "",
    "\n\tnon_ref_af=1-EAF" if ("EAF" in full_header_dict.keys()) else "",
    config_dict["bed schema"].replace(";",",")) # columns to be in output, after 'print' command
    
    if not (config_dict.get("TARGET_HAS_SPAN", "false").lower() in ("t", "true")):
        if "new_target" in dict_to_use.keys():
            awk_script = awk_script.replace("target_ensembl_id,target,z_score_non_ref", "target_ensembl_id,new_target,z_score_non_ref")
    
    if config_dict["bed name"] == "bed4+19 interact":
        cols_section=cols_section.replace("SCORE_STRATEGY=$log","") # these do not need to be in awk script. $ is auto-added above.
        cols_section=cols_section.replace("SCORE_STRATEGY=$multiply","")

        awk_script="""#!/bin/script
LC_ALL=C awk 'BEGIN{{FS="\\t";OFS="\\t"}}{{{}\nprint {}}}' $1 > $2
""".format(cols_section, config_dict["bed schema"].replace(";",","))
        awk_script=awk_script.replace("exp", "my_exp")
        
        ## add user-defined cols if necessary
        user_cols = [i for i in config_dict.keys() if "custom_col" in i]
        if len(user_cols)>0:
            awk_script=awk_script.replace("anchorName,interactionAttr,chrom", "anchorName,interactionAttr\";\"user_input,chrom")
    
    if not (config_dict["bed name"] in ["bed4+19 interact", "bed3+17 qtl"]):
        awk_script="""#!/bin/script
LC_ALL=C awk 'BEGIN{{FS="\\t";OFS="\\t"}}{{{}\nprint {}}}' $1 > $2
""".format(cols_section, config_dict["bed schema"].replace(";",","))

    to_remove_dollars=re.findall("(\$[a-zA-Z\"])", awk_script)
    for i in to_remove_dollars: # remove '$' whenever it appears before quotes or a letter
        keep_char=i[1:]
        awk_script = awk_script.replace(i, keep_char)

    with open(os.path.join(input_df["temp_directory"], "awk_script_"+input_df["fileid"]+".sh"), "w") as f:
        f.write(awk_script)

def get_user_info_cols(header_list, header_idx_dict, config_dict):
    """
    For columns not mapped to any pre-defined or expected columns, synthesize them into a new column called user_info.
    This new column is semicolon-delimited.
    """
    # make sure only the columns not used in the core bed columns are included in the user_info section.
    bed_cols = config_dict["bed schema"].split(";")    
    extra_cols = config_dict["bed optional"].split(";")
    header_idx_dict = { k:header_idx_dict[k] for k in header_idx_dict.keys() if (k in bed_cols+extra_cols) }
    used_columns = list(header_idx_dict.values())
    if config_dict.get("PVALUE_STRATEGY","NR")=="raise10":
        used_columns.remove(header_idx_dict["pval"])
    
    ## create an awk line defining variables not used in determined bed-format columns. Replace some possible variable name characters.
    # run a special version for chromatin interactions format, allowing users to specify new cols.
    allowed_cols = [ config_dict[i] for i in config_dict.keys() if "custom_col" in i ]
    if (config_dict["bed name"] == "bed4+19 interact") and not ("ALL_COLUMNS" in allowed_cols):
        user_info_str = "(\"" + " \";".join([ "{}=\" ${}".format(val,idx+1) for idx, val in enumerate(header_list) if (val in allowed_cols) ]) + ");"
    else:
        user_info_str = "(\"" + " \";".join([ "{}=\" ${}".format(val,idx+1) for idx, val in enumerate(header_list) if ((idx+1) not in used_columns) ]) + ");"
    
    not_used = [ idx for idx,val in enumerate(header_list) if ((idx+1) not in used_columns) ]
    if (len(not_used)==0) or (user_info_str == "(\");"):
        user_info_str="\"\";"
    
    return user_info_str

def get_header_and_first(input_file, config_dict):
    """Get the header line and the one that follows. Try with gzip, then without if that breaks."""
    
    sep = config_dict["sep"]
    if config_dict["HAS_HEADER"].lower() in ["t", "true"]:
        try:
            with gzip.open(input_file, "rt") as f:
                first_line = f.readline()
                header_list = first_line.strip().split(sep)
                second_line = f.readline()
                example_list = second_line.strip().split(sep)
        except gzip.BadGzipFile:
            with open(input_file, "r") as f:
                first_line = f.readline()
                header_list = first_line.strip().split(sep)
                second_line = f.readline()
                example_list = second_line.strip().split(sep)
    else:
        my_logger.info("Header missing. Using columns V0, V1, V2...")
        try:
            with gzip.open(input_file, "rt") as f:
                first_line = f.readline()
                example_list = first_line.strip().split(sep)
                header_list = [ "V"+str(i) for i in range(len(example_list)) ]
        except gzip.BadGzipFile:
            with open(input_file, "r") as f:
                first_line = f.readline()
                example_list = first_line.strip().split(sep)
                header_list = [ "V"+str(i) for i in range(len(example_list)) ]  
    
    return header_list, example_list

def main(cfg_dict, input_df, sep, is_simple_mdt):
    """Call fns."""
    out_path = os.path.join(input_df["temp_directory"], "run_logs", "hipFG_script_log_{}.txt".format(input_df["fileid"]))
    global my_logger
    my_logger = logger.setup_logger(out_path, os.path.basename(__file__), input_df["date_key"])
    
    header_list, example_list = get_header_and_first(input_df["input_file"], cfg_dict)

    ## get header dict if columns aren't listed explicitly.
    header_idx_dict = get_config_idx(header_list, cfg_dict)
    # create dict for optional and required columns. Req columns not always in output. 
    keys_use = (cfg_dict["bed schema"]+";"+cfg_dict["bed required"]).split(";")
    if cfg_dict["bed optional"] != "":
        keys_use += cfg_dict["bed optional"].split(";")

    dict_to_use = { i: str(header_idx_dict.get(i,"NR")) for i in keys_use } 

    dict_to_use.update({ i:cfg_dict[i] for i in cfg_dict.keys() if ((i == i.lower()) or i=="LOOKUP_COL") and (re.search("\".*\"", cfg_dict[i])!=None) })

    if cfg_dict["bed name"]=="bed4+19 interact":
        interaction_attr_dict={k:str(header_idx_dict[k]) for k in header_idx_dict.keys() if "attr_label" in k}
        dict_to_use.update(interaction_attr_dict)

    ## perform check for data types
    if "-split" not in cfg_dict.get("chrom",""):
        cfg_dict["CHR_IS_NUM"] = get_chr(example_list, header_idx_dict, cfg_dict)
    else:
        cfg_dict["CHR_IS_NUM"] = False # don't add a "chr" to the awk script. The position_splitter script will handle whether or not it is missing.  

    if (cfg_dict["bed name"]=="bed3+17 qtl") and (cfg_dict.get("TARGET_CHROM","")!=""):
        if "-split" not in cfg_dict.get("TARGET_CHROM",""):
            cfg_dict["TARGET_CHROM_IS_NUM"] = get_chr(example_list, header_idx_dict, cfg_dict, "TARGET_CHROM")
        else:
            cfg_dict["TARGET_CHROM_IS_NUM"] = False

    # get all other columns specified by the input, and put into the attr column
    user_info_set_vars_str = get_user_info_cols(header_list, header_idx_dict, cfg_dict)
    
    if user_info_set_vars_str != "\"\";":
        dict_to_use["user_input"] = user_info_set_vars_str

    ## create awk
    dict_to_use, scripts_needed, cfg_dict = detect_cases(dict_to_use, cfg_dict, header_list, input_df, is_simple_mdt)

    my_logger.info("Scripts required: [{}]".format(", ".join(scripts_needed)))

    write_awk_via_dict(dict_to_use, cfg_dict, input_df, header_idx_dict)

    cfg_dict["awk script"] = os.path.join(input_df["temp_directory"], "awk_script_{}.sh".format(input_df["fileid"]))
    return cfg_dict, scripts_needed, dict_to_use, header_idx_dict
