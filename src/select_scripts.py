"""
Take in the master config, and write the program that calls the other scripts. This will write a bash script, which:
1. Calls the Python script which writes an awk command for data reformatting
2. Calls the bash scripts that acquire any missing information needed for a fully processed FILER entry

Write the script to execute the re-formatting. 
"""
import argparse, os, re, sys
import logger
import write_awk

# def create_script_new(config_path, output_path, config_dict, python_args_dict, awk_as_dict, mult_types, unzipping_start_time):
def create_script_QTL(input_df, config_dict, awk_dict, header_idx_dict, scripts_required):
    """Use the previous functions to stitch together a functional script.
    Num col: the total number of columns, from which dbSNP_ref and dbSNP_alt can be derived."""
    if config_dict["MULTIPLE_VARIANT_TYPES"].lower() in ["t", "true"]:
        type_tags=["_SNP","_OTHER"]
        mult_types=True
    else:
        type_tags=[""]
        mult_types=False
    
    new_script = ""
    ##
    ## extract chr, position, and other column if necessary
    chr_col_finished = False

    if "position_splitter" in scripts_required:
        out_file="$temp_dir/.unzipped_input_w_chr_pos_${tag}.bed"
        
        user_cols = re.findall('(\"[a-zA-Z0-9_\-;:]*=\" \$[0-9]+)', awk_dict["user_input"]) # get the list of quote-closed column names followed by the "$number" pattern.
        # to do: add option to use fields that are also used in other fields. e.g. pull section from gene even if target=gene.
        defined_fields_to_split = [ i for i in config_dict.keys() if "-split" in str(config_dict[i]) ]
        new_def = []
        spl_num_min, spl_num_max = None, None
        for i in defined_fields_to_split:
            field_input = config_dict[i]
            spl_num = int(field_input[(field_input.index("-split")+6):])
            if spl_num_min == None and spl_num_max == None:
                spl_num_min, spl_num_max = spl_num, spl_num
                new_def.append(i)
            if spl_num<=spl_num_min:
                new_def = [i] + new_def
                spl_num_min = spl_num
            else:
                spl_num_max = spl_num
                new_def.append(i)

        new_def = defined_fields_to_split
        
        all_split_args = []
        split_col_count = 0
        chr_comb, chrStart_comb = "", ""
        for field in defined_fields_to_split:
            field_input = config_dict[field]
            unsplit_field = field_input[:field_input.index("-split")]
            col_using_field = [ i for i in config_dict.keys() if unsplit_field == config_dict[i]]
            
            if len(col_using_field)>0:
                split_idx = field_input[(field_input.index("-split")+6):]
                pull_col_idx = header_idx_dict[col_using_field[0]]-1 # subtract 1 from input col and its split. better_splitter uses 0-indexing, awk uses 1-indexing
                split_idx = int(split_idx)-1
                new_split_arg = [str(pull_col_idx) + "-" + str(split_idx)]
                
                if len(all_split_args)>0:
                    first_element_num = all_split_args[0].split("-")[0]
                    if pull_col_idx<int(first_element_num):# todo: add a general function to make sure split args are in ther right order always
                        all_split_args = new_split_arg + all_split_args
                    else:
                        all_split_args = all_split_args + new_split_arg
                else:
                    all_split_args += new_split_arg
                    
                split_col_count += 1
        

        for user_col in user_cols: # pull column names from user_cols appended to the end.
            column = " ".join(user_col.split(" ")[:-1])
            column = column[1:-2]
            if column[0]==";":
                column = column[1:]

            field_idx = user_col.split(" ")[-1][1:]
            field_idx = str(int(field_idx)-1)

            within_col_splits = [ config_dict[k].split("-")[-1].split("t")[-1] for k in config_dict.keys() if re.match(column+"-split[0-9]+", str(config_dict[k]))]            
            new_split_args = [ field_idx+"-"+str(int(idx)-1) for idx in within_col_splits ]
            if len(new_split_args)>0:
            
                if len(all_split_args)>0:
                    first_element_num = all_split_args[0].split("-")[0]
                    new_arg_num = new_split_args[0].split("-")[0]
                    if int(new_arg_num)<int(first_element_num):
                        all_split_args = new_split_args + all_split_args
                    else:
                        all_split_args = all_split_args + new_split_args
                else:
                    all_split_args += new_split_args
            
                split_col_count += 1

        better_splitter_args = ""
        if ("chr_colon_pos" in scripts_required) or config_dict["IS_BASE_ONE"]:
            split_fields_sorted = list(config_dict["splitting_fields"].keys())
            split_fields_sorted.sort

            chr_start_field = [ all_split_args[i] for i in range(len(split_fields_sorted)) if "chromStart" in config_dict["splitting_fields"][split_fields_sorted[i]] ]
            chr_field = [ all_split_args[i] for i in range(len(split_fields_sorted)) if "chrom" in config_dict["splitting_fields"][split_fields_sorted[i]] ]

            if "chr_colon_pos" in scripts_required:
                if len(chr_field)>0:
                    chr_use = chr_field[0]
                else:
                    chr_use = awk_dict["chrom"].replace("\"chr\" $","").replace("$","")
                
                if len(chr_start_field)>0:
                    chr_start_use = chr_start_field[0]
                else:
                    chr_start_use = awk_dict["chromStart"].replace("-1","")

                better_splitter_args += " --chromosome_idx \"{}\"".format(chr_use)
                better_splitter_args += " --chr_start \"{}\"".format(chr_start_use)
                chr_col_finished = True

        split_out = "$temp_dir/.unzipped_input_w_chr_pos_${tag}.bed" if ("chr_colon_pos" in scripts_required and "chr" in config_dict["splitting_fields"]) else "$temp_dir/.tmp_unzipped_input_${tag}.bed_2"
        new_script += "\ncall_script $scripts_dir/column_imputing_scripts/better_splitter.sh -i $temp_dir/.tmp_unzipped_input_${tag}.bed -o "+split_out
        new_script += " -ts " + " ".join([ "\""+i+"\"" for i in all_split_args ])
        unique_split_idx = set([i.split("-")[0] for i in all_split_args])
        new_script += " -dg " + " ".join([ "'"+config_dict["SPLIT_CHARACTERS_{}".format(i+1)]+"'" for i in range(len(unique_split_idx)) ])
        new_script += " --is_base_one " if config_dict["IS_BASE_ONE"] else ""
        new_script += better_splitter_args
        new_script += "\n"
        new_script += "mv $temp_dir/.tmp_unzipped_input_${{tag}}.bed_2 {}".format("$temp_dir/.unzipped_input_w_chr_pos_${tag}.bed\n" if ("chr_colon_pos" in scripts_required) else "$temp_dir/.tmp_unzipped_input_${tag}.bed\n")
        
    ##  get chr:position if not present yet
    if ("chr_colon_pos" in scripts_required) and (chr_col_finished == False):
        new_script+="\ncall_script $scripts_dir/column_imputing_scripts/get_chr_pos_col.sh $temp_dir/.tmp_unzipped_input_${{tag}}.bed $tag $temp_dir {} {} {} {}\n".format(re.search("([0-9]+)", awk_dict["chrom"]).group(1), awk_dict["chromStart"].replace("-1",""), config_dict["CHR_IS_NUM"], config_dict["IS_BASE_ONE"])
        new_script += "rm $temp_dir/.tmp_unzipped_input_${tag}.bed\n"
    
    if ("position_splitter" in scripts_required) and chr_col_finished==True and ("chr_colon_pos" in scripts_required):
        new_script += "rm $temp_dir/.tmp_unzipped_input_${tag}.bed\n"
    elif not ("chr_colon_pos" in scripts_required):
        new_script += "mv $temp_dir/.tmp_unzipped_input_${tag}.bed $temp_dir/.unzipped_input_w_chr_pos_${tag}.bed"
    
    ## sort after adding chr:pos column
    new_script += "\n"    
    new_script += "call_script $scripts_dir/column_imputing_scripts/sort_columns.sh {0} $temp_dir {1} {0}\n".format("$temp_dir/.unzipped_input_w_chr_pos_${tag}.bed", awk_dict["chr_colon_pos"])
        
    ## separate by var type
    if config_dict["MULTIPLE_VARIANT_TYPES"].lower() in ("true", "t"):
        new_script += "\n"
        new_script += "call_script $scripts_dir/QC_scripts/separate_by_var_type.sh $temp_dir/.unzipped_input_w_chr_pos_${{tag}}.bed $tag {} {} $temp_dir\n".format(awk_dict["og_ref"] if awk_dict["og_ref"] != "\"NR\"" else header_idx_dict.get("A1", awk_dict["tested_allele"]), 
        awk_dict["og_alt"] if awk_dict["og_alt"] != "\"NR\"" else header_idx_dict.get("A2", awk_dict["other_allele"]))
        if "chr_colon_pos" in scripts_required:
            new_script += "\nrm $temp_dir/.unzipped_input_w_chr_pos_${tag}.bed\n"
        my_logger.info("Splitting by var-type; SNPs and INDELs will be in separate output files.")

    new_script += "\n"
    config_dict["MULTIPLE_VARIANT_TYPES"] = "true" if config_dict["MULTIPLE_VARIANT_TYPES"].lower() in ("t", "true") else "false" # bash script isn't flexible to both options

    for type_tag in type_tags:
        if config_dict.get("SPLIT_CHR","false").lower() in ["t", "true"]:
            new_script+="dbSNP_ref_file=$(ls $dbSNP_dir_{}/*.${{split_chr}}.*)\n".format(input_df["genome_build"])
        
        new_script += "call_script $scripts_dir/QC_scripts/compare_ref_to_dbSNP_chrPos.sh $temp_dir/.unzipped_input_w_chr_pos_${{tag}}{}.bed $scripts_dir ${{tag}}{} $temp_dir $dbSNP_ref_file {} {} {} {} \"{}\" {} {} \"{}\" \"{}\" \"{}\" \"$samtools_use\" \"$reference_genome\" \"{}\"\n".format(type_tag, type_tag, awk_dict["chr_colon_pos"], awk_dict["target"], 
        awk_dict["og_ref"] if awk_dict["og_ref"]!="\"NR\"" else header_idx_dict.get("A1", awk_dict["tested_allele"]), 
        awk_dict["og_alt"] if awk_dict["og_alt"]!="\"NR\"" else header_idx_dict.get("A2", awk_dict["other_allele"]), 
        awk_dict.get("variant_id", "NR"), 
        awk_dict["tested_allele"], awk_dict.get("other_allele","NR"), # other allele may be missing if using A1/A2/tested
        awk_dict.get("og_ref", "NR"), awk_dict.get("og_alt", "NR"), config_dict.get("IGNORE_REF_MISMATCH", "false").lower(),
        config_dict["MULTIPLE_VARIANT_TYPES"])
        new_script += "rm $temp_dir/.unzipped_input_w_chr_pos_${{tag}}{}.bed\n".format(type_tag)
    
    ## 3. join with the HGNC set. Pull the gene symbol from the hgnc if necessary. No longer use a separate script for this.
    if "target_gene_symbol" in scripts_required:
        get_hgnc_symbol="1"
    else:
        get_hgnc_symbol="0"
    tmp_files_to_join=[]
    
    new_script += "\n"
    if "HGNC" in scripts_required:
        ff=os.path.join(os.path.dirname(__file__), "../resources/target_lookup_types.txt")
        expected_types=[ i.rstrip() for i in open(ff, "r").readlines() ]
        
        if not (config_dict["LOOKUP_TYPE"] in expected_types):
            my_logger.error("LOOKUP_TYPE must be a valid look-up type found in ./resources/target_lookup_types.txt")
            sys.exit(1)

        if header_idx_dict.get("LOOKUP_COL", awk_dict.get("LOOKUP_COL", "NR")) == "NR":
            my_logger.error("LOOKUP_COL must be a column present in the input data.")
            sys.exit(1)

        for type_tag in type_tags:
            use_chrom = awk_dict["chrom"][7:] if "chr" in awk_dict["chrom"] else awk_dict["chrom"].replace("$","")
            chrom_prefix = "chr" if "chr" in awk_dict["chrom"] else "none"
            new_script += "call_script $scripts_dir/column_imputing_scripts/join_with_hgnc_info.sh $temp_dir/.tmp_new_refs_alts_${{tag}}{}.bed ${{tag}}{} $temp_dir $hgnc_ref_file $hipFG_root/resources/hgnc_joined_cols.txt {} \"{}\" {} \"{}\" \"{}\" {}\n".format(type_tag, type_tag, awk_dict.get("LOOKUP_COL", header_idx_dict.get("LOOKUP_COL","NR")), config_dict["LOOKUP_TYPE"], use_chrom, awk_dict["chromStart"], chrom_prefix, get_hgnc_symbol)
            tmp_files_to_join.append("$temp_dir/.new_hgnc_cols_${{tag}}{}.bed".format(type_tag))
    
    if "genome_partition" in scripts_required:
        new_script += "\n\ngenome_partition_ref=$genome_partition/bed6/{}/giggle_index/".format(input_df["genome_build"].lower())
        for type_tag in type_tags:
            use_target_end = header_idx_dict.get("TARGET_END", awk_dict["TARGET_END"])
            use_target_end = "" if "TARGET_START" in use_target_end else use_target_end
        
            use_chrom = awk_dict["chrom"][7:] if "chr" in awk_dict["chrom"] else awk_dict["chrom"][1:]
            use_chrom_start = awk_dict["chromStart"].split("-")[0]
            new_script += "\ncall_script $scripts_dir/column_imputing_scripts/lookup_target_intervals.sh $temp_dir/.tmp_new_refs_alts_${{tag}}{}.bed $temp_dir/.target_pos_annot_${{tag}}{}.bed \"$genome_partition_ref\" $hgnc_ref_file ${{tag}}{} {} {} {} {} {} {}\n".format(type_tag, type_tag, type_tag, "true" if config_dict["IS_BASE_ONE"].lower() in ("t", "true") else "false", 
            use_chrom, use_chrom_start,
            header_idx_dict.get("TARGET_CHROM", awk_dict["TARGET_CHROM"]), header_idx_dict.get("TARGET_START", awk_dict["TARGET_START"]), use_target_end)
            tmp_files_to_join.append("$temp_dir/.tmp_add_anchor_genes_${{tag}}{}.bed".format(type_tag))
    
    if not ("genome_partition" in scripts_required or "HGNC" in scripts_required):
        my_logger.warning("No gene verification is being called! Is there a genomic target to be assigned in target? Or a genomic position to be provided in TARGET_CHROM+TARGET_START?")
    
    ## 4. add the other necessary columns to these trimmed data
    new_script += "\n"
    
    if "FDR" in scripts_required:
        for type_tag in type_tags:
            pval_col = awk_dict["pval"] if config_dict.get("PVALUE_STRATEGY", "NR")!="raise10" else awk_dict["pval"][6:-2]#(10^($6))
            new_script+="call_script $scripts_dir/column_imputing_scripts/get_FDRs.sh $temp_dir/.tmp_new_refs_alts_${{tag}}{}.bed ${{tag}}{} $scripts_dir {} {} \"{}\" $temp_dir\n".format(type_tag, type_tag, pval_col, awk_dict["target"], config_dict.get("PVALUE_STRATEGY", "NR"))
            tmp_files_to_join.append("$temp_dir/.tmp_all_fdrs_${{tag}}{}.txt".format(type_tag))
    
    new_cols_strings={}
    for type_tag in type_tags:
        new_cols_strings[type_tag] = " ".join([i for i in tmp_files_to_join if ('${{tag}}{}'.format(type_tag) in i)])

    new_script += "\n"
    if len(tmp_files_to_join) > 0:
        for type_tag in type_tags:
            new_script += "\n"
            new_script += "call_script $scripts_dir/column_imputing_scripts/join_multi_cols.sh $temp_dir/.tmp_new_refs_alts_${{tag}}{0}.bed {1} $temp_dir/.unzipped_input_preformat_${{tag}}{0}.bed\n".format(type_tag, new_cols_strings[type_tag])
            new_script += "rm $temp_dir/.tmp_new_refs_alts_${{tag}}{}.bed\n".format(type_tag)
            
            if "FDR" in scripts_required:
                new_script += "rm $temp_dir/.tmp_all_fdrs_${{tag}}{}.txt\n".format(type_tag)
            
            if "HGNC" in scripts_required:
                new_script += "rm $temp_dir/.new_hgnc_cols_${{tag}}{0}.bed\n".format(type_tag)
    
    new_script = new_script.replace("\"\"NR\"\"","\"NR\"") # remove when NR is flanked by TWO double quotes on each side
    
    return new_script, "$temp_dir/.unzipped_input_preformat_${{tag}}{}.bed" if len(tmp_files_to_join)>0 else "$temp_dir/.tmp_new_refs_alts_${{tag}}{0}.bed"

def create_script_interaction(input_df, config_dict, awk_dict, header_idx_dict, scripts_required):
    """Call the necessary scripts for interaction data."""
    skip_awk = False
    new_script = ""
    # if "reformat_interactions" in scripts_required:
    ## get column information to store the extra columns
    header_list, _ = write_awk.get_header_and_first(input_df["input_file"], config_dict)
    if config_dict["HAS_HEADER"].lower() in ["t", "true"]:
        header_list_not_used = [ header_list[i] for i in range(len(header_list)) if (i+1 not in header_idx_dict.values()) ]
        new_col_names = ";".join(header_list_not_used)
    else:
        # If columns are not named, add the new columns as V1,V2... based on their original column
        column_numbers_not_used = [ "V"+str(i) for i in range(len(header_list)) if (i+1 not in header_idx_dict.values()) ]
        new_col_names = ";".join(column_numbers_not_used)

    # save which optional arguments were specified in the config (non-requried ones). Skip the beginning ones which are usually imputed. 
    optional_col_names = ";".join([ str(header_idx_dict.get(i,"")) for i in config_dict["bed schema"].split(";")[7:] if (i not in config_dict["bed required"].split(";")) ])

    new_script += "\ncall_script $scripts_dir/column_imputing_scripts/complete_interact.sh $temp_dir/.tmp_unzipped_input_${tag}.bed $temp_dir/.tmp_remaining_fields_${tag}.bed " + \
        " ".join([ "\""+str(header_idx_dict.get(i,awk_dict.get(i, "NR")))+"\"" for i in ["name", "score", "value", "exp", "color", "sourceName", "sourceStrand", "targetName", "targetStrand", "data_source", "cell_type", "SCORE_STRATEGY"]])
    # input_dat" "output" "name" "score" "value" "exp" "color" "sourceName" "sourceStrand" "targetName" "targetStrand" "data_source" "celltype
    
    new_script += "\n\ngenome_partition_ref=$genome_partition/bed6/{}/giggle_index/".format(input_df["genome_build"].lower())
    new_script += "\ncall_script $scripts_dir/column_imputing_scripts/complete_interact_2.sh $temp_dir/.tmp_remaining_fields_${tag}.bed $temp_dir/.tmp_add_anchor_genes_${tag}.bed $genome_partition_ref $tag " + \
        " ".join([ str(header_idx_dict[i]) for i in ["sourceChrom", "sourceStart", "sourceEnd", "targetChrom", "targetStart", "targetEnd"]])
    last_file = "$temp_dir/.tmp_add_anchor_genes_${{tag}}.bed"
    new_script += "\nrm $temp_dir/.tmp_unzipped_input_${tag}.bed\n"
    new_script += "rm $temp_dir/.tmp_remaining_fields_${tag}.bed\n"

    return new_script, last_file, skip_awk, config_dict

def get_script_awk(config_dict, prev_file):
    """Call the awk-reformatting script. This script was written elsewhere."""
    if "MULTIPLE_VARIANT_TYPES" in config_dict.keys():
        if config_dict["MULTIPLE_VARIANT_TYPES"].lower() in ["t", "true"]:
            type_tags=["_SNP","_OTHER"]
            mult_types=True
        else:
            type_tags=[""]
            mult_types=False
    else:
        type_tags=[""]
        mult_types=False        
        
    new_script = "\n"
    for type_tag in type_tags:
        new_script += "call_script $temp_dir/awk_script_${tag}.sh " + prev_file.format(type_tag) + " $temp_dir/formatted_output_${{tag}}{}.bed\n".format(type_tag, type_tag)
        new_script += "rm " + prev_file.format(type_tag) + "\n"
    
    return new_script, "$temp_dir/formatted_output_${{tag}}{}.bed"

def get_script_start(input_df, config_dict, header_idx_dict):
    """Use the previous functions to stitch together a functional script.
    Num col: the total number of columns, from which dbSNP_ref and dbSNP_alt can be derived."""
    if config_dict.get("MULTIPLE_VARIANT_TYPES", "FALSE").lower() in ["t", "true"]:
        type_tags=["_SNP","_OTHER"]
        mult_types=True
    else:
        type_tags=[""]
        mult_types=False    

    awk_script="""#!/bin/bash
init_config={}
hipFG_root={}    
declare -x hipFG_root
source $init_config    
genome_build={}
output_type=\"{}\"

if [ \"$output_type\" = "QTLs" ]; then
    if [ $genome_build = "hg19" ]; then
        reference_genome="$reference_genome_hg19"
        dbSNP_ref_file="$dbSNP_hg19"
        hgnc_ref_file="$hgnc_gencode_hg19"
    elif [ $genome_build = "hg38" ]; then
        reference_genome="$reference_genome_hg38"
        dbSNP_ref_file="$dbSNP_hg38"
        hgnc_ref_file="$hgnc_gencode_hg38"
    fi
fi 
 
declare -x python_use
declare -x tabix_use
declare -x Giggle

tag={}
output_dir={}
temp_dir={}
scripts_dir=$hipFG_root/src
MAX_RAM=${{MAX_RAM:-5000}}

declare -x scripts_dir
declare -x MAX_RAM
echo -e "Running sample $tag with hipFG version $(cat $hipFG_root/VERSION).\\n"
start_time=$(date +%s)
call_script () {{
    # before running the script, record the time and 
    in_script=$1
    date
    echo All arguments: \"$@\"
	
    prestep_time=$(date +%s)
	day_dur=$(echo "($prestep_time-$start_time)/(3600*24)" | bc) # automatically rounds down
    hr_dur=$(echo "(($prestep_time-$start_time)/3600)%24" | bc | xargs printf "%02d")
    min_dur=$(echo "(($prestep_time-$start_time)/60)%60" | bc | xargs printf "%02d")
    sec_dur=$(echo "(($prestep_time-$start_time))%60" | bc | xargs printf "%02d")
    echo PRE-STEP TOTAL ELAPSED TIME: $day_dur-$hr_dur:$min_dur:$sec_dur

	## print description for this script
	grep SCRIPT_DESCRIPTION $in_script
	
    ## run the script with args as intended.
    bash \"$@\"
    
    post_step_time=$(date +%s)
	day_dur=$(echo "($post_step_time-$prestep_time)/(3600*24)" | bc) # automatically rounds down
    hr_dur=$(echo "(($post_step_time-$prestep_time)/3600)%24" | bc | xargs printf "%02d")
    min_dur=$(echo "(($post_step_time-$prestep_time)/60)%60" | bc | xargs printf "%02d")
    sec_dur=$(echo "(($post_step_time-$prestep_time))%60" | bc | xargs printf "%02d")
    echo THIS STEP ELAPSED TIME: $day_dur-$hr_dur:$min_dur:$sec_dur
    
	day_dur=$(echo "($post_step_time-$start_time)/(3600*24)" | bc) # automatically rounds down
    hr_dur=$(echo "(($post_step_time-$start_time)/3600)%24" | bc | xargs printf "%02d")
    min_dur=$(echo "(($post_step_time-$start_time)/60)%60" | bc | xargs printf "%02d")
    sec_dur=$(echo "(($post_step_time-$start_time))%60" | bc | xargs printf "%02d")
    echo POST-STEP TOTAL ELAPSED TIME: $day_dur-$hr_dur:$min_dur:$sec_dur
    
    echo
    echo
}}
""".format(input_df["init_config"], os.path.dirname(os.path.dirname(__file__)), input_df["genome_build"], 
    "QTLs" if config_dict["bed name"]=="bed3+17 qtl" else input_df.get("output_type", ""), 
    input_df["fileid"], input_df["output_directory"], input_df["temp_directory"])
    
    
    ### special circumstances for if splitting by chromosome. TODO: move this to its own function or section(?)
    if config_dict.get("SPLIT_CHR", "false").lower() in ["t", "true"]:
        awk_script += "call_script $scripts_dir/unzip_by_chr.sh {} $tag $temp_dir {} {}\n".format(input_df["input_file"], config_dict["HAS_HEADER"], header_idx_dict["chr"])
        awk_script += """mkdir -p $temp_dir/chr_logs
echo "Running pipeline for each chromosome. Below are the chr files:"

echo "----"
ls $temp_dir/.tmp_unzipped_input_${{tag}}_chr*.bed
echo "----"
        
echo -n "" > $temp_dir/chr_scripts_${{tag}}.txt
for chr_file in $(ls $temp_dir/.tmp_unzipped_input_${{tag}}_chr*.bed); do
    new_chr=$(echo $chr_file | rev | cut -d '.' -f 2 | cut -d '_' -f 1 | rev)
    sed "s/<my replacement tag>/${{tag}}_${{new_chr}}\\nsplit_chr=${{new_chr}}/" $temp_dir/auto_pipeline_${{tag}}_byChr.sh > $temp_dir/auto_pipeline_${{tag}}_${{new_chr}}.sh
    sed -i.bak "s/awk_script_.*\.sh \$temp/awk_script_${{tag}}.sh \$temp/g" $temp_dir/auto_pipeline_${{tag}}_${{new_chr}}.sh && rm $temp_dir/auto_pipeline_${{tag}}_${{new_chr}}.sh.bak
    echo "bash $temp_dir/auto_pipeline_${{tag}}_${{new_chr}}.sh > $temp_dir/chr_logs/chr_log_${{tag}}_${{new_chr}}.txt" >> $temp_dir/chr_scripts_${{tag}}.txt
done
parallel -a $temp_dir/chr_scripts_${{tag}}.txt --jobs {} --joblog $temp_dir/pipeline_log_by_chr_${{tag}}.txt "{{}}"
""".format(config_dict["JOBS_PER_SAMPLE"])

        if config_dict["JOBS_PER_SAMPLE"] == 1:
            awk_script = awk_script.replace(re.search(".*(parallel.*)", awk_script).group(1), "bash $temp_dir/chr_scripts_${tag}.txt > $temp_dir/chr_logs/chr_log_${tag}_${new_chr}.txt")
    else:
        awk_script += "call_script $scripts_dir/unzip_input.sh {} $tag $temp_dir {}\n".format(input_df["input_file"], config_dict["HAS_HEADER"])
    
    return awk_script, "$temp_dir/.tmp_unzipped_input_${{tag}}.bed"

def get_script_end(input_df, final_script, header_idx_dict, awk_dict, config_dict, scripts_req, run_for_filer, is_simple_mdt):
    """
    Get the end steps of the pipeline, such as sorting, zipping and getting the metadata.
    """
    if config_dict["bed name"] == "filerxQTL":
        final_script = final_script[:-4] + "{}.bed"
    
    if config_dict.get("MULTIPLE_VARIANT_TYPES", "FALSE").lower() in ["t", "true"]:
        type_tags=["_SNP","_OTHER"]
        mult_types=True
    else:
        type_tags=[""]
        mult_types=False
    
    new_script = "\n"
    
    for type_tag in type_tags:
        if final_script.format(type_tag) != "$temp_dir/formatted_output_${{tag}}{}.bed".format(type_tag):
            new_script += "mv {} $temp_dir/formatted_output_${{tag}}{}.bed\n".format(final_script.format(type_tag), type_tag)
    
    signifs=[""]
    if config_dict.get("SEP_BY_SIGNIF", "false").lower() in ["t", "true"]:
        signif_val = float(config_dict.get("SIGNIF_CUTOFF", "0.05"))
        signifs=["_all", "_significant"]
        for type_tag in type_tags:
            schema_split = config_dict["bed schema"].split(";")
            if config_dict["FDR"]!="":
                signif_col = [ i+1 for i in range(len(schema_split)) if schema_split[i]=="FDR" ][0]
            else:
                signif_col = [ i+1 for i in range(len(schema_split)) if schema_split[i]=="pval" ][0]

            new_script += "call_script $scripts_dir/wrapup_scripts/filter_by_fdr.sh {} $temp_dir \"{}\" {}\n".format(final_script.format(type_tag), input_df["fileid"]+type_tag, str(signif_col))
            my_logger.info("Splitting by significance!")
    
    new_script += "\n"
    for signif in signifs:
        for type_tag in type_tags:
            strand_info = header_idx_dict.get("strand", header_idx_dict.get("target_strand", awk_dict.get("strand", awk_dict.get("target_strand", "NR"))))
            new_script += "call_script $scripts_dir/wrapup_scripts/final_sort.sh $temp_dir/formatted_output_${{tag}}{0}{1}.bed $temp_dir {2} $temp_dir/formatted_output_${{tag}}{0}{1}.bed \"{3}\"\n".format(type_tag, 
            signif, strand_info, config_dict["bed schema"].replace(";","+")) # do not add a header for interactions data

    ## zip the output
    new_script += "\n"
    for signif in signifs:
        for type_tag in type_tags:
            new_script += "call_script $scripts_dir/wrapup_scripts/gzip_final.sh $temp_dir/formatted_output_${{tag}}{0}{1}.bed $temp_dir/formatted_output_${{tag}}{0}{1}.bed.gz\n".format(type_tag, signif)
    
    ## index it with tabix. This assumes bed format and 0-indexed, which should be true. 
    new_script += "\n"
    for signif in signifs:
        for type_tag in type_tags:
            new_script += "call_script $scripts_dir/wrapup_scripts/tabix_on_zipped.sh $temp_dir/formatted_output_${{tag}}{0}{1}.bed.gz\n".format(type_tag, signif)
    
    new_script += "\n"
    for signif in signifs:
        for type_tag in type_tags:
            new_script += "rm $temp_dir/formatted_output_${{tag}}{0}{1}.bed\n".format(type_tag, signif)
    
    ## hierarchy
    dir_bedname=config_dict["bed name"]
    if dir_bedname[:4] == "bed ":
        dir_bedname=dir_bedname[4:]
    dir_bedname = dir_bedname.replace("+", "plus").replace(" ","_")

    new_script += "\n"

    hierarchy_list = [signifs, type_tags, input_df.get("assay", "define_assay"), dir_bedname, input_df["genome_build"]]

    old_dirs = ["$output_dir"]
    prev_dir = ""
    for hier_level in hierarchy_list:
        if hier_level == [""]:
            continue
        
        new_dirs = []
        for old_dir in old_dirs:
            new_dir = "mkdir -p {}\n".format(old_dir)
            if new_dir != prev_dir:
                new_script += new_dir
            prev_dir = new_dir
            if type(hier_level) == list:
                for hier_member in hier_level:
                    if hier_member[1:]!="":
                        new_dirs.append("{}/{}".format(old_dir, hier_member[1:]))
            else:
                if hier_level!="":
                    new_dirs.append("{}/{}".format(old_dir, hier_level))
        old_dirs = new_dirs
    terminal_dirs=[]
    last_dir=""
    for old_dir in old_dirs:
        if old_dir != last_dir:
            new_script += "mkdir -p {}\n".format(old_dir).replace("//","/")
            terminal_dirs.append(old_dir)
        last_dir=old_dir
    ## finally move the output
    # <data source>_<assay>_<format (remove "bed ")>_<genome build>_<file ID>.bed
    new_script += "\n"
    all_out_files=[]
    
    # if len(signifs)>1 and len(type_tags)>1:
    if len(signifs)>1 or len(type_tags)>1:
        for signif in signifs:
            for type_tag in type_tags:
                if signif == "":
                    my_out_dir = [ tdir for tdir in terminal_dirs if "/"+type_tag[1:]+"/" in tdir][0]
                    all_out_files.append(os.path.join(my_out_dir, "formatted_output_"+input_df["fileid"]+type_tag+signif+".bed.gz"))
                else:
                    if type_tag == "":
                        my_out_dir = [ tdir for tdir in terminal_dirs if "/"+signif[1:]+"/" in tdir][0]
                        all_out_files.append(os.path.join(my_out_dir, "formatted_output_"+input_df["fileid"]+type_tag+signif+".bed.gz"))
                    else:
                        my_out_dirs = [ tdir for tdir in terminal_dirs if "/"+type_tag[1:]+"/" in tdir]
                        my_out_dir = [ tdir for tdir in my_out_dirs if "/"+signif[1:]+"/" in tdir][0]
                        all_out_files.append(os.path.join(my_out_dir, "formatted_output_"+input_df["fileid"]+type_tag+signif+".bed.gz"))
        
                new_script += "mv $temp_dir/formatted_output_${{tag}}{}{}.bed.gz {}\n".format(type_tag, signif, my_out_dir)
                new_script += "mv $temp_dir/formatted_output_${{tag}}{}{}.bed.gz.tbi {}\n".format(type_tag, signif, my_out_dir)
    
    else:
        my_out_dir = terminal_dirs[0]
        new_script += "mv $temp_dir/formatted_output_${{tag}}.bed.gz.tbi {}\n".format(my_out_dir)
        new_script += "mv $temp_dir/formatted_output_${{tag}}.bed.gz {}\n".format(my_out_dir)
        all_out_files.append(os.path.join(my_out_dir, "formatted_output_"+input_df["fileid"]+type_tag+signif+".bed.gz"))
        
    
    ## get the final metadata
    # add any extra columns as key-value pairs in track_description field
    expected_df_fields = ["fileid", "cell_type", "biosample_type", "input_file", 
        "output_directory", "temp_directory", "file_config", "init_config", "genome_build", 
        "data_source", "id_prefix", "assay", "output_type", "biosample_term_id", "encode_experiment_id",
        "downloaded_date", "download_date", "release_date", "doi", "biological_replicates", "technical_replicate","antibody",
        "project_url", "original_cell_type_name", "data_category_dict", "metadata_dict", "raw_file_url", "id_num", "life_stage", 
        "track_description", "date_key"]

    # remove quotes from provided fields
    for i in expected_df_fields:
        if type(input_df.get(i, ""))==str and "\"" in input_df.get(i, ""):
            input_df[i] = input_df[i].replace("\"", "")

    current_keys = list(input_df.keys())
    
    if is_simple_mdt=="false":
        for k in current_keys:
            if k not in expected_df_fields:
                new_val = input_df.get("track_description","")
                new_val = new_val + ";" + k + "=" + input_df[k]
                my_logger.info("Adding {} as new key-value pair in Track Descriptions!".format(k))
                input_df["track_description"] = new_val
        
        input_df["track_description"] = input_df["track_description"].replace("\"","").replace("%","%%")
        input_df["track_description"] = "Original Cell Type Name="+input_df["original_cell_type_name"]+input_df["track_description"]

        input_df["id_num"] = str(input_df["id_num"]).zfill(7)
        my_logger.info("dataset unique ID={}{}".format(input_df["id_prefix"].upper(), input_df["id_num"]))

        out_files = ",".join([ "$(echo $output_dir | sed 's/<my_allele_type>/{0}/')/${{tag}}{1}.bed.gz".format(type_tag[1:], type_tag) for type_tag in type_tags ])
        new_script += "bash $scripts_dir/wrapup_scripts/get_metadata_row.sh \"{}\" {} $tag $temp_dir \"{}\" $celltypes_dict $data_categories $scripts_dir \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\" \"{}\"\n".format(",".join(all_out_files), input_df["input_file"], 
            ",".join(terminal_dirs),
            input_df["id_prefix"].upper(), input_df["id_num"], input_df["data_source"],
            input_df["output_type"], input_df["genome_build"], input_df["cell_type"].replace("\"",""), input_df["biosample_type"].replace("\"",""),
            input_df.get("biosample_term_id",""), input_df.get("encode_experiment_id",""), input_df.get("biological_replicates",""), input_df.get("technical_replicate",""),
            input_df.get("antibody",""), input_df["assay"].replace("\"",""), config_dict["bed name"], input_df.get("downloaded_date", input_df["download_date"]), 
            input_df["release_date"], input_df["project_url"], input_df.get("raw_file_url", ""), input_df["track_description"], input_df["life_stage"].replace("\"", ""),
            run_for_filer)
    else:
        new_script += "echo \"Simple MDT provided! Skipping metadata steps.\""

    ## cleanup
    new_script += "\n"

    new_script += """
current_time=$(date +%s)
elapsed=$(echo "($current_time-$start_time)" | bc)
day_dur=$(echo "($current_time-$start_time)/(3600*24)" | bc) # automatically rounds down
hr_dur=$(echo "(($current_time-$start_time)/3600)%24" | bc | xargs printf "%02d")
min_dur=$(echo "(($current_time-$start_time)/60)%60" | bc | xargs printf "%02d")
sec_dur=$(echo "(($current_time-$start_time))%60" | bc | xargs printf "%02d")

## print the formatted time calculated above
echo ***FINAL ELAPSED TIME: $day_dur-$hr_dur:$min_dur:$sec_dur
"""
    return new_script
    
def main(input_df, config_dict, awk_dict, header_idx_dict, scripts_req, run_for_filer, is_simple_mdt): 
    ## get awk as dict
    pipeline_script, last_file = get_script_start(input_df, config_dict, header_idx_dict)
    out_path = os.path.join(input_df["temp_directory"], "run_logs", "hipFG_script_log_{}.txt".format(input_df["fileid"]))
    global my_logger
    my_logger = logger.setup_logger(out_path, os.path.basename(__file__), input_df["date_key"])
    ## 
    if config_dict.get("SPLIT_CHR", "false").lower() in ["t", "true"]:
        presplit_pipeline_script = pipeline_script
        
        ## remove the chr-splitting section. do it better
        split_script = pipeline_script.split("\n")
        last_line = [ i for i in range(2,len(split_script)) if (split_script[i].strip() == "}") and (split_script[i-1].strip() == "echo") and (split_script[i-2].strip() == "echo") ]
        last_line = last_line[0]    

        pipeline_script = "\n".join(pipeline_script.split("\n")[:(last_line+1)])
        
        pipeline_script=pipeline_script.replace("tag={}".format(input_df["fileid"]), "tag=<my replacement tag>") + "\n"
    
    skip_awk = False
    
    ## write middle portion of script depending on special data types
    if config_dict["bed name"] == "bed3+17 qtl":
        if not ("MULTIPLE_VARIANT_TYPES" in config_dict.keys()):
            my_logger.warning("MULTIPLE_VARIANT_TYPES not defined for QTL data! Defaulting to 'FALSE' (SNPs only).")
            config_dict["MULTIPLE_VARIANT_TYPES"] ="false"

        new_section, last_file = create_script_QTL(input_df, config_dict, awk_dict, header_idx_dict, scripts_req)
        pipeline_script += new_section
        
    if config_dict["bed name"] == "bed4+19 interact":
        new_section, last_file, skip_awk, config_dict = create_script_interaction(input_df, config_dict, awk_dict, header_idx_dict, scripts_req)
        pipeline_script += new_section
        
    new_section, last_file = get_script_awk(config_dict, last_file)
    pipeline_script += new_section

    if config_dict["bed name"] == "bed4+19 interact":
        pipeline_script += "\ncall_script $scripts_dir/column_imputing_scripts/reformat_interactions.sh $temp_dir/formatted_output_${tag}.bed $temp_dir/formatted_output_${tag}.bed_2"
        pipeline_script += "\nmv $temp_dir/formatted_output_${tag}.bed_2 $temp_dir/formatted_output_${tag}.bed"

    if config_dict.get("SPLIT_CHR", "false").lower() in ["t", "true"]:
        if config_dict.get("MULTIPLE_VARIANT_TYPES", "FALSE").lower() in ["t", "true"]:
            type_tags=["_SNP","_OTHER"]
        else:
            type_tags=[""]
        
        for type_tag in type_tags:
            presplit_pipeline_script += """
echo -n "" > $temp_dir/formatted_output_${{tag}}{0}.bed
for formatted_out in $(ls $temp_dir/formatted_output_${{tag}}_chr*{0}.bed); do
    cat $formatted_out >> $temp_dir/formatted_output_${{tag}}{0}.bed
    rm $formatted_out
done
""".format(type_tag)
        
        presplit_pipeline_script += get_script_end(input_df, last_file, header_idx_dict, awk_dict, config_dict, scripts_req, run_for_filer, is_simple_mdt)
        with open(os.path.join(input_df["temp_directory"], "auto_pipeline_{}.sh".format(input_df["fileid"])), "w") as f:
            f.write(presplit_pipeline_script)
        
        with open(os.path.join(input_df["temp_directory"], "auto_pipeline_{}_byChr.sh".format(input_df["fileid"])), "w") as f:
            f.write(pipeline_script)

    else:
        pipeline_script += get_script_end(input_df, last_file, header_idx_dict, awk_dict, config_dict, scripts_req, run_for_filer, is_simple_mdt)
        with open(os.path.join(input_df["temp_directory"], "auto_pipeline_{}.sh".format(input_df["fileid"])), "w") as f:
            f.write(pipeline_script)
    