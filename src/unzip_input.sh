#!/bin/bash

SCRIPT_DESCRIPTION="unzip_input.sh: Take the raw input file and unzip it into the specified temporary directory. If it is specified to have a header, remove it during the unzipping."

input_file=$1
tag=$2
temp_dir=$3
has_header=$4

has_header=$(echo $has_header | tr '[:upper:]' '[:lower:]')

test_zipped=$(file $input_file -b | grep "gzip" -c)

if [ $test_zipped -eq 1 ]; then
	if [ $has_header = "t" ] || [ $has_header = "true" ]; then
		gunzip -c $input_file | sed 's/\r//g' | tail -n +2 > $temp_dir/.tmp_unzipped_input_${tag}.bed
	else
		gunzip -c $input_file | sed 's/\r//g' > $temp_dir/.tmp_unzipped_input_${tag}.bed
	fi
else
	if [ $has_header = "t" ] || [ $has_header = "true" ]; then
		tail -n +2 $input_file | sed 's/\r//g' > $temp_dir/.tmp_unzipped_input_${tag}.bed
	else
		cat $input_file | sed 's/\r//g' > $temp_dir/.tmp_unzipped_input_${tag}.bed
	fi
fi
