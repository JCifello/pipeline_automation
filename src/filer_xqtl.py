"""
Take in the header line and one row of the input data frame. This includes data about one sample. 

Run 2 scripts:
    1. write_awk.py: Based on provided config, write the awk script to be used for reformatting.
    2. select_scripts.py: Write the bash file that runs the pipeline itself. 
"""
import argparse, os, gzip, sys, re
import select_scripts, write_awk
from subprocess import call
import logger

def check_types(input_df, config_dict):
    """If the bed format is specified by name, make sure columns are in the right order."""
    try:
        with gzip.open(input_df["input_file"], "rt") as f:
            first_line = f.readline()
            header_list = first_line.strip().split(config_dict["sep"])
            second_line = f.readline()
            example_list = second_line.strip().split(config_dict["sep"])
    
    except gzip.BadGzipFile:
        with open(input_df["input_file"], "r") as f:
            first_line = f.readline()
            header_list = first_line.strip().split(config_dict["sep"])
            second_line = f.readline()
            example_list = second_line.strip().split(config_dict["sep"])

    if config_dict["HAS_HEADER"].lower() in ["t", "true"]:
        joined_header=";".join(header_list)
        if joined_header != config_dict["bed schema"]:
            my_logger.error("Header name differs from expected! Manually label columns in your config.")
            my_logger.error("Provided: {}, expected: {}".format(joined_header, config_dict["bed schema"]))
            sys.exit(1)
        content_list = example_list
    else:
        content_list = header_list
        
    type_regex={"chr":"[0-9]+",
    "chrStart":"[0-9]+",
    "chrEnd":"[0-9]+",
    "itemRgb":"[0-9]+,[0-9]+,[0-9]+",
    "chr(hg38)":"[0-9]+","chrStart(hg38)":"[0-9]+","chrEnd(hg38)":"[0-9]+",
    "chr(hg19)":"[0-9]+","chrStart(hg19)":"[0-9]+","chrEnd(hg19)":"[0-9]+",
    "strand":"[\.\-\=]"}
    
    my_logger.info("Checking for compatible data types.")
    bed_cols = config_dict["bed schema"].split(";")
    for idx, col_name in enumerate(bed_cols):
        if col_name not in type_regex.keys():
            continue
        try_match = re.search(type_regex[col_name], content_list[idx]).group()
    
def get_config_dicts(my_config):
    """
    Save into a dictionary EVERY line from the config where there is an equals (=) sign. 
    Also, save to the dictionary the default separator "\t".
    """
    config_args_dict = {}
    with open(my_config, "r") as f:
        line = f.readline()
        taking_args=False
        while line:
            line = line.replace("\#", "POUNDSYMBOL")
            line = line.split("#")[0]
            line = line.replace("POUNDSYMBOL","#")
            if "=" in line:
                try:
                    line_left, line_right=line.rstrip().split("=")
                    config_args_dict[line_left] = line_right
                except ValueError:
                    my_logger.error("Could not unpack. Halting section.")
                    break    
            line = f.readline()

    ## some manual replacements to account for Python eccentricities:
    if "sep" not in config_args_dict.keys():
        config_args_dict["sep"] = "\t"
    if config_args_dict["sep"] == "\\t":
        config_args_dict["sep"] = "\t"

    ## check required fields
    req_fields = ["IS_BASE_ONE", "HAS_HEADER"]
    config_args_dict["SPLIT_CHR"] = config_args_dict.get("SPLIT_CHR", "false").lower() # set default value
    for req in req_fields:
        if req not in config_args_dict.keys():
            my_logger.error("Must specify {} in the config.".format(req))
            sys.exit(1)
    
    if config_args_dict["SPLIT_CHR"] in ["t", "true"]:
        if "JOBS_PER_SAMPLE" not in config_args_dict.keys():
            my_logger.warning("JOBS_PER_SAMPLE missing and SPLIT_CHR is true! Defaulting to running chromosomes in parallel on 2 jobs.")
            config_args_dict["JOBS_PER_SAMPLE"] = 2
        else:
            config_args_dict["JOBS_PER_SAMPLE"] = int(config_args_dict["JOBS_PER_SAMPLE"])
    
    return config_args_dict

def check_init_config(init_file, file_config_dict, input_dict):
    """Make sure that required fields are specified."""
    required_fields=["resources_dir", "dbSNP_hg19", "dbSNP_hg38", "hgnc_gencode_hg38", 
    "hgnc_gencode_hg19", "celltypes_dict", "data_categories", "Giggle", 
    "tabix_use", "python_use", "samtools_use", "reference_genome_hg19", "reference_genome_hg38"]

    with open(init_file, "r") as f:
        keep_lines = [line.replace(" ","").strip() for line in f.readlines() if "=" in line]
        keep_lines = [ line.split("#")[0] for line in keep_lines ]
        keep_lines = [ line for line in keep_lines if len(line)>0 ] # this allows commenting out lines in the config.
        init_dict = { line.split("=")[0]:line.split("=")[1] for line in keep_lines }

    for req in required_fields:
        if req not in init_dict.keys():
            my_logger.error("Required field {} not present in INIT config. (*ini file).".format(req)) # todo use logger
            sys.exit(1)

    if file_config_dict.get("SPLIT_CHR","f").lower() in ["t", "true"]:
        if "dbSNP_dir_{}".format(input_dict["genome_build"]) not in init_dict.keys():
            my_logger.error("dbSNP_dir_{} must be specified to run this pipeline and variant normalization in parallel.".format(input_dict["genome_build"]))
            sys.exit(1)


def determine_bed_format(config_dict):
    """
    Using the input config and the bed schemas file, determine which of the predetermined
    bed formats best matches the input in the config.
    """
    declared_cols=set(config_dict.keys())
    
    bed_schemas_file=os.path.join("/".join(__file__.split("/")[:-2]), "resources", "BED_schemas.tsv")
    best_overlap, best_format, best_schema = 0, None, None
    with open(bed_schemas_file, "r") as f:
        line = f.readline()
        line = f.readline() # skip the header line
        while line:
            line_name, _, line_schema, line_required, line_optional, line_desc_required, line_desc_optional = line.replace("\n","").split("\t")
            line_name = " ".join(line_name.split(" ")[1:])
            
            expected_cols = set(line_required.split(";"))

            overlap = len(declared_cols & expected_cols)
            if (overlap == len(expected_cols)) and (overlap > best_overlap):
                best_overlap = overlap
                best_format = line_name
                best_schema = line_schema
                best_required = line_required
                best_optional = line_optional
                best_desc_required = line_desc_required
                best_desc_optional = line_desc_optional

            line = f.readline()

    my_logger.info("{} automatically determined as the best format.".format(best_format))
    try:
        return best_format, best_schema, best_required, best_optional, best_desc_required, best_desc_optional
    except UnboundLocalError:
        my_logger.error("The correct bed schema could not be found! Config does not contain cover enough columns to fulfill required columns of an expected type. Correct config txt file.")
        sys.exit(1)

def get_bed_cols(bed_name):
    """If the bed schema is declared directly, instead of determining it based on column names, return the expected columns.
    These will be used in case any changes are needed.
    """
    bed_schemas_file=os.path.join("/".join(__file__.split("/")[:-2]), "resources", "BED_schemas.tsv")
    if "bed " not in bed_name:
        bed_name="bed " + bed_name
        
    with open(bed_schemas_file, "r") as f:
        line = f.readline()
        while line:
            line_name, _, line_schema, line_required, line_optional, line_desc_required, line_desc_optional = line.replace("\n","").split("\t")
            if line_name == bed_name:
                best_schema = line_schema
                best_required = line_required
                best_desc_required = line_desc_required
                best_desc_optional = line_desc_optional
                
            line = f.readline()
    try:        
        return best_required, best_schema, best_desc_required, best_desc_optional
    except UnboundLocalError:
        my_logger.error("{} is not a valid pre-defined bed format name!".format(bed_name))
        sys.exit(1)

def get_input_dict(input_table, sample_line_num, is_simple_mdt):
    """Take the input lines and return a dictionary after checking that all necessary fields are present."""
    all_fileids, all_initconfigs = [], []
    with open(input_table, "r") as f:
        line = f.readline()
        line_counter = 1
        while line:
            if line_counter==1:
                input_header=line.rstrip().split("\t")
                try:
                    fileid_idx = [ i for i in range(len(input_header)) if input_header[i].lower()=="fileid" ][0]
                except IndexError:
                    my_logger.error("Must supply FileID, a unique identifer for each sample.")
                    sys.exit(1)
                    
                try:
                    initconfig_idx = [ i for i in range(len(input_header)) if input_header[i].lower()=="init config" ][0]
                except IndexError:
                    my_logger.error("Must supply FileID, a unique identifer for each sample.")
                    sys.exit(1)
            else:
                try:
                    all_fileids.append(line.rstrip().split("\t")[fileid_idx])
                    all_initconfigs.append(line.rstrip().split("\t")[initconfig_idx])
                except IndexError:
                    my_logger.error("Could not find column {} in row {}! Check your MDT for any extra white-space.".format(initconfig_idx, line_counter))
                    sys.exit(1)
                
            if line_counter == sample_line_num:
                input_line = line.rstrip().split("\t")    
            
            line = f.readline()
            line_counter += 1
    
    ## check for uniqueness of sample-ids
    if len(all_fileids) != len(set(all_fileids)):
        my_logger.error("FileID detected to belong to multiple samples! All fileIDs must be unique.")
        sys.exit(1)
        
    ## make sure there is only one config init
    if len(set(all_initconfigs)) != 1:
        my_logger.error("Only one Init Config allowed, please make sure that this column contains only one unique value.")
        sys.exit(1)

    if len(input_line) != len(input_header):
        my_logger.warning("Sample #{} has {} columns, but the header expects {}. You have empty columns!".format(sample_line_num-1, len(input_line), len(input_header)))
    
    input_dict = { val.lower().replace(" ","_"):input_line[i] for i, val in enumerate(input_header) if i<len(input_line) }
        
    if is_simple_mdt == False:    
        input_dict["data_source"] = input_dict["data_source"].replace(" ", "_")
        req_fields = ["fileid", "cell_type", "biosample_type", "input_file", 
            "output_directory", "temp_directory", "file_config", "init_config", "genome_build", 
            "data_source", "id_prefix", "assay", "output_type", 
            "download_date", "release_date", "doi", "pubmed_id",
            "project_url", "original_cell_type_name", "life_stage"]
    else:
        req_fields = ["fileid", "input_file", "output_directory", "temp_directory",
            "file_config", "init_config"]
        
    for req in req_fields:
        if req not in input_dict.keys():
            my_logger.error("Must specify {} in input tsv.".format(req))
            sys.exit(1)

    if not (input_dict["genome_build"] in ("hg19", "hg38")):
        my_logger.error("genome_build must be one of hg19 or hg38.")
        sys.exit(1)

    return input_dict

def get_dirs_from_input_dict(input_table, sample_line_num):
    """Before even establishing a whole input dictionary, determine where the work space will be"""
    all_fileids, all_initconfigs = [], []
    with open(input_table, "r") as f:
        line = f.readline()
        line_counter = 1
        while line:
            if line_counter==1:
                input_header=line.rstrip().split("\t")
                
            if line_counter == sample_line_num:
                input_line = line.replace("\n","").split("\t")  

            line = f.readline()
            line_counter += 1  

    input_dict = { val.lower().replace(" ","_"):input_line[i] for i, val in enumerate(input_header) }
    input_dict = { i:input_dict[i] for i in ("fileid", "temp_directory", "output_directory") }
    return input_dict


def check_in_table_contents(in_df):
    """After confirming that all fields are present, for fields that have controlled vocabularies or ontologies, check that valid values are present.
    todo: allow pointing to different resources directory? Currently assumes its the same as repo. 
    """
    life_stage_file=os.path.join(os.path.split(__file__)[0], "..", "resources", "life_stages.txt")

    with open(life_stage_file, "r") as f:
        life_stage_list = [ i.rstrip() for i in f.readlines() ]

    if not (in_df["life_stage"] in life_stage_list):
        my_logger.error("Error with 'Life Stage'= {}\nValues in 'Life Stage' must appear in ({}).".format(in_df["life_stage"], "\", \"".join(life_stage_list)))
        sys.exit(1)

def get_id_starting_num(mdt_path, sample_line, my_date):
    """
    Search all run logs in all temp directories for flags indicating the same ID prefix is used.
    Pull the one with the highest ID number.
    """

    ## first acquire all log files in all mentioned temp directories
    temp_dirs=set()
    with open(mdt_path, "r") as f:
        line = f.readline()
        line_counter=1
        req_and_idx = {"fileid":None,"temp_directory":None, "id_prefix":None}
        while line:
            if line_counter==1:
                for k in req_and_idx.keys():
                    found_idx = [ i for i,val in enumerate(line.split("\t")) if val.lower().replace(" ","_")==k ]
                    req_and_idx[k] = found_idx[0]
            else:
                spl_line = line.split("\t")
                temp_dirs.add(spl_line[req_and_idx["temp_directory"]])

            if line_counter == sample_line:
                spl_line = line.split("\t")
                my_id_prefix = spl_line[req_and_idx["id_prefix"]].upper()
                my_fileid = spl_line[req_and_idx["fileid"]]

            line_counter += 1
            line = f.readline()

    log_files=set()
    temp_dirs = list(temp_dirs)
    for tdir in temp_dirs:
        all_files = os.listdir(os.path.join(tdir, "run_logs"))
        keep_files = [os.path.join(tdir,"run_logs",i) for i in all_files if i[:16] == "hipFG_script_log"]
        log_files.update(set(keep_files))
    
    ## go through these run-log files.
    max_found=0
    signif_factor, var_type_factor = 1,1
    for log_file in list(log_files):
        with open(log_file, "r") as f:
            line = f.readline()
            while line:
                if "hipFG run-date" in line:
                    new_date=line.split("=")[-1].rstrip()
                    if new_date != my_date:
                        break
                if "Splitting by significance" in line:
                    signif_factor=2
                if "Splitting by var-type" in line:
                    var_type_factor=2
                if "FileID" in line:
                    old_fileid = line.split("=")[-1].rstrip()
                    if old_fileid == my_fileid:
                        break

                if "dataset unique ID" in line:
                    old_id = line.split("=")[-1].rstrip()
                    try:
                        old_id_int = int(old_id.replace(my_id_prefix, ""))
                    except ValueError:
                        pass
                    old_id_int = old_id_int+(1*var_type_factor*signif_factor)-1
                    if old_id_int > max_found:
                        max_found = old_id_int

                line = f.readline()

    return max_found+1

def check_config_descriptors(config_dict):
    """"""
    req_descriptors = config_dict["bed required-descriptors"]
    opt_descriptors = config_dict["bed optional-descriptors"]
    
    ## req
    req_list = req_descriptors.split(";")
    for req_desc in req_list:
        if not (req_desc in config_dict):
            my_logger.error("For this output format type, please specify {} in the File Config.".format(req_desc))
            sys.exit(1)

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--mdt")
    parser.add_argument("--sample_line", type=int)
    parser.add_argument("--date_key")
    parser.add_argument("--for_filer", type=str, choices=("True", "False", "true", "false"))
    parser.add_argument("--simple_mdt", type=str, choices=("True", "False", "true", "false"))
    args = parser.parse_args()

    temp_input_dict = get_dirs_from_input_dict(args.mdt, args.sample_line)
    
    for new_dir in (temp_input_dict["output_directory"], temp_input_dict["temp_directory"], os.path.join(temp_input_dict["temp_directory"], "run_logs")):
        try:
            os.mkdir(new_dir)
        except FileExistsError:
            pass

    out_path = os.path.join(temp_input_dict["temp_directory"], "run_logs", "hipFG_script_log_{}.txt".format(temp_input_dict["fileid"]))
    my_logger = logger.setup_logger(out_path, os.path.basename(__file__), args.date_key)
    input_dict = get_input_dict(args.mdt, args.sample_line, args.simple_mdt)

    script_dir = os.path.dirname(__file__)
    hipfg_root = os.path.join(script_dir, "..")
    version = open(os.path.join(hipfg_root,"VERSION"),"r").read().rstrip()
    my_logger.info("Running hipFG v{}".format(version))
    my_logger.info("hipFG run-date={}".format(args.date_key))
    my_logger.info("Running for FileID={}".format(input_dict["fileid"]))
    input_dict["date_key"] = args.date_key

    args.simple_mdt = args.simple_mdt.lower()
    if args.simple_mdt=="false":
        check_in_table_contents(input_dict)
    ## get all the fields specified in the config
    config_dict = get_config_dicts(input_dict["file_config"])

    ## check the init config to make sure that all required fields have been specified
    check_init_config(input_dict["init_config"], config_dict, input_dict)
    
    ## make all paths absolute
    for relative_path in ["file_config", "init_config", "temp_directory", "output_directory", "input_file"]:
        input_dict[relative_path] = os.path.abspath(input_dict[relative_path])
    
    ##
    if "BED_FORMAT" in config_dict.keys():
        config_dict["bed name"] = config_dict["BED_FORMAT"]
        config_dict["bed required"], config_dict["bed schema"], config_dict["bed required-descriptors"], config_dict["bed optional-descriptors"] = get_bed_cols(config_dict["bed name"])
        config_dict["bed optional"] = ""
        my_logger.info("Specific format provided! Will execute hipFG for: {}".format(config_dict["BED_FORMAT"]))
        check_types(input_dict, config_dict)
        
    else:
        config_dict["bed name"], config_dict["bed schema"], config_dict["bed required"], config_dict["bed optional"], config_dict["bed required-descriptors"], config_dict["bed optional-descriptors"] = determine_bed_format(config_dict)
    
    check_config_descriptors(config_dict)

    new_config_dict, scripts_required, awk_dict, header_idx_dict = write_awk.main(config_dict, input_dict, "\t", args.simple_mdt)
    
    if args.simple_mdt.lower()=="false":
        input_dict["id_num"] = get_id_starting_num(args.mdt, args.sample_line, args.date_key)

    select_scripts.main(input_dict, new_config_dict, awk_dict, header_idx_dict, scripts_required, args.for_filer.lower(), args.simple_mdt)
