#!/bin/bash

SCRIPT_DESCRIPTION="filter_by_fdr.sh: Take an input file, typically some sort of QTL data, and create a new file, with only the rows where FDR < cutoff."

input_file=$1
temp_dir=$2
fileid=$3
fdr_col=$4
cutoff=${5:-0.05}

LC_ALL=C awk 'BEGIN{FS="\t"}{fdr=$'$fdr_col'; cutoff='$cutoff'; if (fdr<cutoff) {print $0}}' $input_file > $temp_dir/formatted_output_${fileid}_significant.bed

mv $input_file $temp_dir/formatted_output_${fileid}_all.bed
