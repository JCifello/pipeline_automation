#!/bin/bash

SCRIPT_DESCRIPTION="get_metadata_row.sh: Take the input raw and processed files. Get the number of final intervals, bp covered, and their md5sums. Print all available info for the FILER metadata schema. Also lookup biological info based on provided cell type."

## inputs concerning files
out_files=${1} # list of output files comma-delimited
raw_file=${2}
FileID=${3}
temp_dir=${4}
out_dirs=${5}
metadata_dict=${6}
data_category_dict=${7}
scripts_dir=${8}

## inputs which are metadata specific. Several are optional and should be imputed as "" by the inputting select_scripts python script. 
id_prefix=${9}
id_num=${10}
data_source=${11}
output_type=${12}
genome_build=${13}
cell_type=${14}
biosample_type=${15}
biosample_term_id=${16:-"Not applicable"}
encode_experiment_id=${17:-"Not applicable"}
biological_replicates=${18:-"Not reported"} # option for "not applicable"? GWAS...
technical_replicate=${19:-"Not applicable"}
antibody=${20:-"Not applicable"}
assay=${21}
file_format=${22}
download_date=${23}
release_date=${24}
link_out_url=${25}
raw_file_url=${26:-"Not applicable"}
original_cell_type_name=${27}
life_stage=${28}
running_for_filer=${29}

date_added_to_gadb=$(date +%m/%d/%y)

out_files=($(echo $out_files | sed 's/,/ /g'))
out_dirs=($(echo $out_dirs | sed 's/,/ /g'))

function get_md5(){
	if command -v md5sum >/dev/null 2>&1; then
		my_md5=$(md5sum "$1" | cut -d ' ' -f 1)
	elif command -v md5 >/dev/null 2>&1; then
		my_md5=$(md5 -q "$1")
	fi
	echo $my_md5
}

# biosample_type=$(echo $biosample_type | sed 's/_/ /g')
# cell_type=$(echo $cell_type | sed 's/ /_/g')

echo -e "Acquiring metadata for ${out_files[@]}...\n"

num_files=$(echo "${#out_files[@]}-1" | bc)

for i in $(seq 0 $num_files); do
	# get number of intervals + genomic coverage. 
	awk_metric_outs=$(gunzip -c ${out_files[$i]} | tail -n +2 | LC_ALL=C awk 'BEGIN{FS="\t";OFS=",";total_span=0;chr_last="xxxx";last_end=0}{
	bedChr=$1;bedStart=$2+1;bedEnd=$3+0
		
	if (chr_last == bedChr) {
		## start counting at beginning of interval if new interval starts higher
		if (bedStart>last_end){
			span_start=bedStart
		}else{
			span_start=last_end
		}
		
		## 
		if (bedEnd>last_end){
			span=(bedEnd-span_start)+1
		}else{
			span=0
		}
	
		if (bedEnd>last_end) {
			last_end=bedEnd
		}
	} else {
		span=bedEnd-bedStart +1
		last_end=bedEnd
	}
	
	chr_last=bedChr
	total_span+=span
	# print span
}END{print NR,total_span}')
	
	num_intervals=$(echo $awk_metric_outs | cut -d ',' -f 1)
	bp_covered=$(echo $awk_metric_outs | cut -d ',' -f 2)

	processed_md5=$(get_md5 ${out_files[$i]})
	raw_md5=$(get_md5 $raw_file)
	
	file_name=$(echo ${out_files[$i]} | rev | cut -d '/' -f 1 | rev)
	file_path=$(echo ${out_files[$i]} | rev | cut -d '/' -f 2- | rev)
	
	# all columns:
	# Identifier,Data Source,File name,Number of intervals,bp covered,Output type,Genome build,cell type,Biosample type,Biosamples term id,Tissue category,
	# ENCODE Experiment id,Biological replicate(s),Technical replicate,Antibody,Assay,File format,File size,filepath,
	# Downloaded date,Release date,Date added to GADB,Processed File Download URL,Processed file md5,wget command,tabix_index Download,Link out URL,Raw 
	# File URL,Raw file download,Raw file md5,Data Category,classification,original cell type name,system category
	
	metadata_line=$(LC_ALL=C awk 'BEGIN {FS="\t";OFS=";"} {cell_type=$1; if (cell_type == "'"$cell_type"'") { print $1,$2,$3 } }' $metadata_dict)
	
	if [ "$metadata_line" == "" ]; then
		echo -e "WARNING: Cell type \"$cell_type\" does not map to a known cell type and therefore was not assigned a tissue or system category.\n"
	fi
	
	# look up the data category using Assay and file-format
		# print assay,file_format,"'$assay'","bed '$file_format'"
	data_category=$(LC_ALL=C awk 'BEGIN {FS="\t"} {assay=$1; file_format=$2; data_category=$3; 
		if ((assay == "'"$assay"'") && (file_format == "bed '"$file_format"'")) { 
			print $3
		} 
	}' $data_category_dict)

	tissue_category=$(echo $metadata_line | cut -d ';' -f 2)
	system_category=$(echo $metadata_line | cut -d ';' -f 3)
	file_size=$(ls -o $file_path/$file_name | awk '{print $4}')
	
	### put together link information
	after_annTracks=$(echo $file_path | LC_ALL=C awk 'BEGIN{FS="/"; OFS="/"; found=0} { for(i=1;i<=NF;i++) { if (found==1) { printf "/"$i } if ($i== "Annotationtracks") { found=1 } } }')
	
	# echo "after inputs for raw:" $after_annTracks_raw
	if [ $running_for_filer == "true" ]; then
		processed_url=$(echo "https://lisanwanglab.org/GADB/FILER2/Annotationtracks/"$after_annTracks"/"$file_name | sed 's/\/\//\//g' | sed 's/:\//:\/\//g')
		wget_processed=$(echo "wget https://lisanwanglab.org/GADB/FILER2/Annotationtracks/"$after_annTracks"/"$file_name" -P GADB/FILER2/Annotationtracks/"$after_annTracks | sed 's/\/\//\//g' | sed 's/:\//:\/\//g')
		wget_processed_tbx=$(echo "wget https://lisanwanglab.org/GADB/FILER2/Annotationtracks/"$after_annTracks"/"$file_name".tbi -P GADB/FILER2/Annotationtracks/"$after_annTracks | sed 's/\/\//\//g' | sed 's/:\//:\/\//g')
		raw_name=$(echo $raw_file | rev | cut -d '/' -f 1 | rev)
		wget_raw=$(echo "wget https://lisanwanglab.org/GADB/FILER2/Annotationtracks/Downloads/"$data_source"/"$raw_name" -P GADB/FILER2/Annotationtracks/Downloads/"$data_source"/"$raw_name | sed 's/\/\//\//g' | sed 's/:\//:\/\//g')	
	else 
		processed_url="Not applicable"
		wget_processed="Not applicable"
		wget_processed_tbx="Not applicable"
		wget_raw="Not applicable"
	fi
	
	file_path=$(echo $file_path | sed 's@/\+@/@g') # replace multiple slashes in output directories with one. 
	
	## only use date added to filer, processed file download url, wget command, and tabix_index download if this is being run for FILER
	if [ $running_for_filer == "true" ]; then
		out_line="${id_prefix}${id_num}\t$data_source\t$file_name\t$num_intervals\t$bp_covered\t$output_type\t$genome_build\t$cell_type\t$biosample_type\t$biosample_term_id\t$tissue_category"
		out_line="${out_line}\t$encode_experiment_id\t$biological_replicates\t$technical_replicate\t$antibody\t$assay\t$file_format\t$file_size\t$file_path\t$download_date\t$release_date"
		out_line="${out_line}\t$date_added_to_gadb\t$processed_url\t$processed_md5\t$wget_processed\t$wget_processed_tbx\t$link_out_url\t$raw_file_url\t$wget_raw\t$raw_md5\t$data_category\t""\t$original_cell_type_name\t$system_category"
	else
		out_line="${id_prefix}${id_num}\t$data_source\t$file_name\t$num_intervals\t$bp_covered\t$output_type\t$genome_build\t$cell_type\t$biosample_type\t$biosample_term_id\t$tissue_category"
		out_line="${out_line}\t$encode_experiment_id\t$biological_replicates\t$technical_replicate\t$antibody\t$assay\t$file_format\t$file_size\t$file_path\t$download_date\t$release_date"
		out_line="${out_line}\t$processed_md5\t$link_out_url\t$raw_file_url\t$wget_raw\t$raw_md5\t$data_category\t""\t$original_cell_type_name\t$system_category"
	fi
	
	
	# leaving a blank space for where the track classification will go
	echo -e $out_line > $temp_dir/.metadata_rows_${FileID}.tsv
	
	## print for logging file:
	echo -e $file_path/$file_name"\t"$num_intervals
	
	## after running the MD, run the track_classification script, move extra columns around. 
	bash $scripts_dir/wrapup_scripts/track_classification.sh $temp_dir/.metadata_rows_${FileID}.tsv | LC_ALL=C awk 'BEGIN{FS="\t";OFS="\t"}{ 
	for (i=1;i<NF;i++) {data_cat_col=32;
		if ((i != data_cat_col)&&(i!=(NF-1))) { printf "%s\t",$i } # <-- problem line when original cell type name has a % in it.
		if (i == data_cat_col) { printf $NF"\t" }
		if (i == (NF-1)) { print $i"\t'$life_stage'" } # allow a new line char for the last argument. Also add life stage as the last argument.
	}
	}' > $file_path/metadata_row_${FileID}.tsv
	rm $temp_dir/.metadata_rows_${FileID}.tsv
done
