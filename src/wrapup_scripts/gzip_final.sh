#!/bin/bash

SCRIPT_DESCRIPTION="6.0_gzip_final.sh: Perform final zip into the output directory using bgzip. Force the output."

in_file=$1
out_file=$2

bgzip $in_file --force --stdout > ${in_file}_2
mv ${in_file}_2 $out_file
