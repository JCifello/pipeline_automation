#!/bin/bash

SCRIPT_DESCRIPTION="final_sort.sh: Take the final formatted output and sort it based on chr, start, stop. ALSO: If the input data never had a header, add one here."

in_file=$1
temp_dir=$2
strand_col=$3
out_file=$4
new_header=$5
add_header=${6:-"true"}

add_header=$(echo $add_header | tr '[:upper:]' '[:lower:]')

if [ $add_header == "true" ]; then
	echo "#${new_header}" | sed 's/+/\t/g' > ${in_file}_2
else
	echo -n "" > ${in_file}_2
fi

if [ $strand_col == "NR" ]; then
	LC_ALL=C sort $in_file -t $'\t' -k1,1 -k2,2n -k3,3n -T ${temp_dir} -S ${MAX_RAM}M --parallel=1 >> ${in_file}_2
else
	LC_ALL=C sort $in_file -t $'\t' -k1,1 -k2,2n -k3,3n -k${strand_col},${strand_col} -T ${temp_dir} -S ${MAX_RAM}M --parallel=1 >> ${in_file}_2
fi

mv ${in_file}_2 $out_file
