#!/bin/bash

SCRIPT_DESCRIPTION="Run tabix on the output after it is zipped. The zip and indexing will take place in the temporary directory, then get moved to the final directory."

in_zipped=$1

# using version 1.13
$tabix_use -p bed -0 $in_zipped
