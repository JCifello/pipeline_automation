#!/bin/bash

# this script assumes that the giggle variable has already been set and exported via declare -x <your giggle>

input_dir=$1

use_dir=$(echo $input_dir | sed 's@//@/@g' | sed s'@/$@@') # remove double slashes and any slash at the end
in_files=${use_dir}/*.bed.gz
$Giggle index -i "$in_files" -o ${use_dir}/giggle_index -s -f

chmod a+rx ${use_dir}/giggle_index
chmod a+r ${use_dir}/giggle_index/*
chmod a+rw ${use_dir}/giggle_index/cache.*
