#cut -f6,15,16 GADB_metadata_V1_final_9252020.tsv | awk 'BEGIN{FS="\t"}{print $1, $2, $3, "classification", ("[" $3 " " $2 " " $1 "]");}' | freq
INMETA=${1:-GADB_metadata_V1_final_9252020.tsv}
LC_ALL=C awk 'BEGIN{FS="\t";OFS="\t"}
  {
		# if (NR==1) {print $0, "classification"; next}; # print header
	  outType=$6; antibody=$15; assay=$16;
		dataSource=$2; originalAntibody=$15;
		if (antibody~/^H[0-9]+[A-Z]+[0-9]+/)
			antibody=(antibody "-histone-mark");
		else if (antibody!~/^Not appl/ && antibody!~/consolidated/ && antibody!~/CTCF/ && antibody!~/ChromatinAcc/)
			antibody=("protein");
		#if (!antibodies[originalAntibody])
		#{
		#	antibodies[originalAntibody]=1;
		#	antibodyList[antibody]=(antibodyList[antibody] "," originalAntibody);
		#}	
		gsub(/-human/,"",antibody); # remove antibody '-human' suffix (ENCODE)
		if (antibody!~/Not applicable/)
		{
		  classification=(assay " " antibody " " outType);
			#print dataSource, originalAntibody,  classification;
			#print outType, antibody, assay, "classification", ("[" assay " " antibody " " outType "]");
		}
		else
		{
			classification=(assay " " outType);
			#print dataSource, originalAntibody, classification;
			#print outType, antibody, assay, "classification", ("[" assay " " outType "]");
    }
		print $0, classification;
  }' "${INMETA}"
