#!/bin/bash
## LiftOver for bed.gz
set -e 
INBEDGZ=${1:-"input.bed.gz"}
if [ $# -lt 1 ]; then
  echo "USAGE: $0 <input.bed.gz> <liftover.chain> <lifted.bed> <unlifted.bed>"
  exit 1
fi
CHAIN=${2:-"/project/wang/pkuksa/datasets/hg19/hg19ToHg38.over.chain.gz"}
LIFTEDBED=${3:-"${INBEDGZ%.bed.gz}.lifted.bed"}
UNLIFTEDBED=${4:-"${INBEDGZ%.bed.gz}.unlifted.bed"}
liftOverInputBED=${5:-"${INBEDGZ%.bed.gz}.liftOver_input.bed"} # intermediate BED file to be used as input to liftOver

# check liftOver, bgzip are available
LIFTOVER=/project/wang/pkuksa/bin/liftOver
if [ ! -x "${LIFTOVER}" ]; then
  echo "liftOver ($LIFTOVER) not found. Please make sure correct path is specified"
  exit 1
fi
BGZIP=/project/wang/pkuksa/bin/htslib-1.7/bgzip
if [ ! -x "${BGZIP}" ]; then
  echo "bgzip (${BGZIP}) not found. Please make sure correct path is specified"
  exit 1
fi

# 1. prepare liftOver input
# duplicate coordinate fields; these first 3 fields (hg19) will be replaced by liftOver with the new (hg38) coordinates
gunzip -c "${INBEDGZ}" | \
  awk 'BEGIN{FS="\t";OFS="\t"}{print $1, $2, $3, $0}' > "${liftOverInputBED}" 

# 2. run liftOver
# NOTE: -tab separated input; this will prevent 'spaces' to be converted to tabs by 'liftOver' script 
# NOTE: -bedPlus=3, this will keep/output the rest of the fields (all original BED columns)
cmd=("${LIFTOVER}" -bedPlus=3 -tab "${liftOverInputBED}" "${CHAIN}" "${LIFTEDBED}" "${UNLIFTEDBED}")
echo -e "Liftover command:\n${cmd[@]}"
${cmd[@]}

# 3. sort, compress
LC_ALL=C sort -k1,1 -k2,2n -k3,3n ${LIFTEDBED} | ${BGZIP} -c > "${LIFTEDBED}.gz"


# 4. clean up
rm "${liftOverInputBED}"

echo "Lifted bed.gz: ${LIFTEDBED}.gz"
