#!/bin/bash
set -e

SCRIPT_DESCRIPTION="column_imputing_scripts/get_chr_pos_col.sh: This script is called if the input data does not have a column formatted chr:startPos. Generate this column, omitting "chr" in the chromosome column. This step is needed for the 3.0_compare_ref_to_dbSNP_chrPos.sh script, which normalizes the input against the dbSNP dataset. If input is 1-indexed, subtract 1 from position."

input_dat=$1
tag=$2
output_dir=$3
chr_col=$4
pos_col=$5
chr_is_num=$6
is_base_one=$7 # if data is 0-based, need to add 1 before lookup

chr_is_num=$(echo $chr_is_num | tr '[:upper:]' '[:lower:]')
is_base_one=$(echo $is_base_one | tr '[:upper:]' '[:lower:]')

## if parse_num is true, assume column is chr# and pull out after the "r"
if [ $is_base_one = "t" ] || [ $is_base_one = "true" ]; then
	if [ $chr_is_num = "t" ] || [ $chr_is_num = "true" ]; then
        cat $input_dat | LC_ALL=C awk 'BEGIN{FS="\t";OFS="\t"}{chr=$'$chr_col'; pos=$'$pos_col'; chr_pos=(chr ":" pos); print $0,chr_pos }' > $output_dir/.unzipped_input_w_chr_pos_${tag}.bed
    else
        cat $input_dat | LC_ALL=C awk 'BEGIN{FS="\t";OFS="\t"}{split($'$chr_col', chr_split, "r"); chr=chr_split[2]; pos=$'$pos_col'; chr_pos=(chr ":" pos); print $0,chr_pos }' > $output_dir/.unzipped_input_w_chr_pos_${tag}.bed
    fi
else # if base-zero, add 1 before joining with dbSNP
    if [ $chr_is_num = "t" ] || [ $chr_is_num = "true" ]; then
        cat $input_dat | LC_ALL=C awk 'BEGIN{FS="\t";OFS="\t"}{chr=$'$chr_col'; pos=$'$pos_col'+1; chr_pos=(chr ":" pos); print $0,chr_pos }' > $output_dir/.unzipped_input_w_chr_pos_${tag}.bed
    else
        cat $input_dat | LC_ALL=C awk 'BEGIN{FS="\t";OFS="\t"}{split($'$chr_col', chr_split, "r"); chr=chr_split[2]; pos=$'$pos_col'+1; chr_pos=(chr ":" pos); print $0,chr_pos }' > $output_dir/.unzipped_input_w_chr_pos_${tag}.bed
    fi
fi
