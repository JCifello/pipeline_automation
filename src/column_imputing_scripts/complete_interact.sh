#!/bin/bash

# input_dat=$1


args=("input_dat" "output" "name" "score" "value" "exp" "color" "sourceName" "sourceStrand" "targetName" "targetStrand" "data_source" "cell_type" "score_strategy")

for i in "${!args[@]}"; do
    let ii=i+1
    arg_name=${args[$i]}
    arg_value="${!ii}"
    printf -v "$arg_name" "$arg_value"
done

cell_type=$(echo $cell_type | sed 's/ /_/g')
data_source=$(echo $data_source | sed 's/ /_/g')
exp=$(echo $exp | sed 's/ /_/g')

awk -v name="$name" \
    -v score="$score" \
    -v value="$value" \
    -v my_exp="$exp" \
    -v color="$color" \
    -v sourceName="$sourceName" \
    -v sourceStrand="$sourceStrand" \
    -v targetName="$targetName" \
    -v targetStrand="$targetStrand" \
    -v data_source="$data_source" \
    -v score_strategy="$score_strategy" \
    -v cell_type="$cell_type" 'BEGIN{FS="\t";OFS="\t"} {
        original_nf=NF
        ## handle the interaction name. Add it to the end.
        if (name>original_nf) {
            if (data_source!="NR") {
                $0=($0"\t"data_source"___"cell_type"___"NR)
            } else {
                exit 1
            }
        }

        ## handle missing score, value, or both. 
        special_vals["sourceName"] = interaction_number"A"
	    special_vals["targetName"] = interaction_number"B"
	
        if ((value>original_nf)&&(score>original_nf)) {
            new_score = 0
            new_value = 1
        } 
        
        # value is provided but score isnt:
        if ((value<=original_nf)&&(score>original_nf)) { 
                if (score_strategy=="multiply") {
                    new_score = $value * 1000
                } else {
                    if (value==0) { 
                        new_score = 1000
                    } else {
                        new_score = -1 * (log($value) / log(10))
                    }

                }
                if ((new_score%1)>=0.5) { new_score = new_score + (1-new_score%1) } # round up if should
                new_score = int(new_score)
        }

        # score is provided but value isnt
        if ((score<=original_nf)&&(value>original_nf)) {
            new_value = $score / 1000
        }

        if (score>original_nf) $0=($0"\t"new_score)
        if (value>original_nf) $0=($0"\t"new_value)

        ## handle missing experiment field
        if (my_exp>original_nf) {
            $0=($0"\t"cell_type)
        }

        ## if color is not provided, use 0 instead
        if (color>original_nf){
            $0=($0"\t0")
        }

        # set source and target name + strand
        if (sourceName>original_nf) {
            $0=($0"\t"NR"A")
        }
        if (sourceStrand>original_nf) {
            $0=($0"\t.")
        }
        if (targetName>original_nf) {
            $0=($0"\t"NR"B")
        }
        if (targetStrand>original_nf) {
            $0=($0"\t.")
        }
        print $0

    }' "$input_dat" > "$output"


