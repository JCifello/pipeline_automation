#!/bin/bash

SCRIPT_DESCRIPTION="better_splitter.sh: Calls better_splitter.py. Takes pairs of columns to be split and what columns are to be pulled out. Also takes in the delimiters for each unique column."


python $scripts_dir/column_imputing_scripts/better_splitter.py $@
