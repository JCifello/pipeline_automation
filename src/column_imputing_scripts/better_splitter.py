"""
Take in the original data, any columns to be split, and which columns to keep. 
Output the original data with any desired columns appended to the end. Split columns will be put onto the end in order of:
    * original (to-split) column appearance, then
    * appearance of (split) columns in to-split columns
"""
import argparse

def get_field_as_list(field_str, delimiters):
    """Take a value and delimit by all necessary values. Take in str and return list."""
    split_field = [field_str]
    for delim in delimiters:
        new_list=[]
        for old_field in split_field:
            new_list += old_field.split(delim)
        split_field = new_list
    return split_field

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--in_file", "-i", help="The input file.")
    parser.add_argument("--out_file", "-o", help="The output file.")
    parser.add_argument("--to_extract","-ts", nargs="+", help="Which columns to split, hyphenated with which element to keep. These should be provided in order.")
    parser.add_argument("--delimiter_groups", "-dg", nargs="+",help="For each unique column to be split, in order, what delimiters should be used?")
    parser.add_argument("--chromosome_idx", help="The split position of the chromosome. Correct for numeric chromosomes. May be used for adding chr:pos to the end.", required=False, default="")
    parser.add_argument("--chr_start", help="The split position of the starting position. Used for adding chr:pos to the end.", required=False, default="")
    parser.add_argument("--is_base_one", help="Whether start position is 1-indexed and needs to be subtracted from. Used for adding chr:pos to the end.", action="store_true")
    ### need to add a section here where it can handle chromosome appending. 
    ### also add chr and chrStart if chr:colon needs to be generated. 
    args = parser.parse_args()
    
    with open(args.in_file, "r") as f:
        line = f.readline()
        max_cols = line.split("\n")
    
    split_col_ints = [ int(i.split("-")[0]) for i in args.to_extract ]
    ordered_ints = list(set(split_col_ints))
    ordered_ints.sort()

    col_to_delim = { ordered_ints[i]:[j for j in args.delimiter_groups[i]] for i in range(len(ordered_ints)) }
    chr_is_split, start_is_split = ("-" in args.chromosome_idx), ("-" in args.chr_start)

    if not chr_is_split: args.chromosome_idx = int(args.chromosome_idx)-1
    if not start_is_split: args.chr_start = int(args.chr_start)-1

    with open(args.in_file, "r") as f, open(args.out_file, "w") as f_out:
        line = f.readline()
    
        counter = 0
        # while line and counter<1000:
        while line:
            keep_vals = []
            split_line = line.replace("\n", "").split("\t")
            
            for i in range(len(split_line)):
                field = split_line[i]
                if i in ordered_ints: # if this column gets split, split it and add to list of vals that go on the end. 
                    delims = col_to_delim[i]
            
                    field_list = get_field_as_list(field, delims)
                    
                    new_vals = []
                    for j in range(len(field_list)):
                        as_pair = "{}-{}".format(i,j)
                        
                        if as_pair in args.to_extract:
                            if as_pair == args.chr_start:
                                if args.is_base_one: # if data is base-one, keep as base-one for chr:pos column BUT remove 1 for chromStart output field. FILER is 0-indexed. 
                                    chr_start = field_list[j]
                                    new_vals.append(str(int(field_list[j])-1))
                                else:   
                                    chr_start = str(int(field_list[j])+1) #chr:pos column is always base-one. 
                                    new_vals.append(field_list[j])
                                continue
                                    
                            if (as_pair == args.chromosome_idx):
                                my_chr = "chr"+field_list[j]
                                my_chr = my_chr.replace("chrchr", "chr")
                                    
                            new_vals.append(field_list[j])
                    
                    keep_vals += new_vals
                    
            for i in split_line + keep_vals[:-1]:
                f_out.write(i+"\t")
            
            if (args.chromosome_idx != ""): # chromosme idx is only specified when chr:position column needs to be added. 
                if chr_is_split:
                    my_chr = my_chr[3:]
                else:
                    my_chr = split_line[args.chromosome_idx].replace("chr","")
                
                if start_is_split==False:
                    chr_start= str(split_line[args.chr_start]) if args.is_base_one else str(int(split_line[args.chr_start])+1)
                
                chr_pos =  my_chr + ":" + chr_start
                f_out.write(keep_vals[-1]+"\t"+chr_pos+"\n")
            else:
                f_out.write(keep_vals[-1]+"\n")    
            
            line = f.readline()
            counter += 1
