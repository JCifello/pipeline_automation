#!/bin/bash

args=("input_dat" "output" "ref_index" "tag" "sourceChrom" "sourceStart" "sourceEnd" "targetChrom" "targetStart" "targetEnd")

for i in "${!args[@]}"; do
    let ii=i+1
    arg_name=${args[$i]}
    arg_value=${!ii}
    printf -v "$arg_name" "$arg_value"
done

temp_dir=$(dirname $output)
source_out="${temp_dir}/.tmp_source_anchor_regions_${tag}.bed"
target_out="${temp_dir}/.tmp_target_anchor_regions_${tag}.bed"

## print out the source and target anchors into a region bed file.
echo "Starting awk to get regions..."
awk -v sourceChrom="$sourceChrom" \
    -v sourceStart="$sourceStart" \
    -v sourceEnd="$sourceEnd" \
    -v targetChrom="$targetChrom" \
    -v targetStart="$targetStart" \
    -v targetEnd="$targetEnd" \
'BEGIN{FS="\t";OFS="\t"}{
    print $sourceChrom,$sourceStart,$sourceEnd >> "'"${source_out}"'"
    print $targetChrom,$targetStart,$targetEnd >> "'"${target_out}"'"
}' $input_dat


echo "Zipping..."
bgzip "${source_out}" --force --stdout > "${source_out}.gz"
bgzip "${target_out}" --force --stdout > "${target_out}.gz"

rm "${source_out}"
rm "${target_out}"

source_out="${source_out}.gz"
target_out="${target_out}.gz"

## look up the regions from above in the reference giggle directory
source_gene_out="${temp_dir}/.tmp_source_anchor_genes_${tag}.bed"
target_gene_out="${temp_dir}/.tmp_target_anchor_genes_${tag}.bed"

echo "Doing Giggle search..."
$Giggle search -i "${ref_index}" -q "${source_out}" -o -v -b > "$source_gene_out"
$Giggle search -i "${ref_index}" -q "${target_out}" -o -v -b > "$target_gene_out"

## process the output
echo "Creating unique anchor and gene keys..."
awk 'BEGIN{FS="\t";OFS="\t"}{gene_match=$7; gsub(".*ID=", "", gene_match); gsub(";.*", "", gene_match); print $1,$2,$3,gene_match}' "$source_gene_out" > "${temp_dir}/.tmp_anchor_gene_key_${tag}.bed"
awk 'BEGIN{FS="\t"; OFS="\t"}{gene_match=$7; gsub(".*ID=", "", gene_match); gsub(";.*", "", gene_match); print $1,$2,$3,gene_match}' "$target_gene_out" >> "${temp_dir}/.tmp_anchor_gene_key_${tag}.bed"

cat "${temp_dir}/.tmp_anchor_gene_key_${tag}.bed" | sort -u > "${temp_dir}/.tmp_anchor_gene_key_${tag}.bed_2"
mv "${temp_dir}/.tmp_anchor_gene_key_${tag}.bed_2" "${temp_dir}/.tmp_anchor_gene_key_${tag}.bed"

echo "Writing to a new file..."
awk  -v sourceChrom="$sourceChrom" \
    -v sourceStart="$sourceStart" \
    -v sourceEnd="$sourceEnd" \
    -v targetChrom="$targetChrom" \
    -v targetStart="$targetStart" \
    -v targetEnd="$targetEnd" \
'BEGIN{FS="\t"; OFS="\t"}
FILENAME==ARGV[1]{
    pos_id=($1"-"$2"-"$3)
    if ($4 != "n/a") {
        if (pos_and_gene[pos_id]=="") {
            pos_and_gene[pos_id]=$4
        } else {
            pos_and_gene[pos_id]=(pos_and_gene[pos_id]"|"$4)
        }
    }
}
FILENAME==ARGV[2]{
    source_pos_id=$sourceChrom"-"$sourceStart"-"$sourceEnd
    target_pos_id=$targetChrom"-"$targetStart"-"$targetEnd
    
    source_gene=pos_and_gene[source_pos_id]
    target_gene=pos_and_gene[target_pos_id]

    segment_search="-mRNA.-UTR3_exon.-UTR3_intron.-intron.-exon.-UTR5_exon.-UTR5_intron"
    segment_write="promoter.UTR3exon(s).UTR3intron(s).intron(s).exon(s).UTR5exon(s).UTR5intron(s)"
    split(segment_search, search_a, ".")
    split(segment_write, write_a, ".")
    
    ## do a series of checks to try and shorten the ID.
    delete u_genes
    for (anchor_count=1;anchor_count<=2;anchor_count++){

        if (anchor_count==1) {
            split(source_gene, a, "|")
            full_gene=source_gene
        }
        if (anchor_count==2) {
            split(target_gene, a, "|")
            full_gene=target_gene
        }
    
        gene_count=1
        for (i in a){
            gene_segment=a[i]
            gene_name=gene_segment
            gsub(/\-intron.*/,"",gene_name)
            gsub(/\-exon.*/,"",gene_name)
            gsub(/\-mRNA.*/,"",gene_name)
            gsub(/\-UTR5_.*.*/,"",gene_name)
            gsub(/\-UTR3.*/,"",gene_name)
            
            if (u_genes[gene_name]=="") {
                u_genes[gene_name]=gene_count
                gene_count++
            }
        }
        
        tot_found=0
        new_out_str=""
        for (i=1;i<gene_count;i++) {
            for (gene_name in u_genes) {
                if (u_genes[gene_name]==i) my_gene=gene_name
            }

            for (j in search_a) {
                new_str=(my_gene search_a[j])
                if (full_gene ~ new_str) {
                    new_out_str=(new_out_str"|"my_gene" "write_a[j])
                    tot_found++
                }
            }
        }

        if (length(new_out_str)>0) {
            new_out_str=substr(new_out_str, 2, length(new_out_str))
        } else {
            new_out_str="intergenic_region"
        }

        if (anchor_count==1) source_str=new_out_str
        if (anchor_count==2) target_str=new_out_str

    }
    
    ## replace blank or n/a strings. Save.
    if ((source_gene == "n/a") || (source_gene == "")) {
        source_str="intergenic_region"
    }
    if ((target_gene == "n/a") || (target_gene == "")) {
        target_str="intergenic_region"
    }
    print $0,source_str,target_str
}' "${temp_dir}/.tmp_anchor_gene_key_${tag}.bed" "$input_dat" > "$temp_dir/.tmp_add_anchor_genes_${tag}.bed"

rm "${temp_dir}/.tmp_anchor_gene_key_${tag}.bed"
rm "$source_out"
rm "$target_out"
rm "$source_gene_out"
rm "$target_gene_out"

## parse these outputs to get the original file but with this gene info.
# save gene info for each anchor, sep files (or skip this and just make the next step parse even more.)
# do original + source genes + target genes with 'paste'
