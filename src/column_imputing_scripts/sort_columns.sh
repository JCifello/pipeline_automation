#!/bin/bash
set -e

SCRIPT_DESCRIPTION="sort_columns.sh: Take an input file and sort it by the specified column. Save a temporary version to memory and use it to overwrite the original. Use _2 placeholder suffix in case input and output have the same name."

in_file=$1
temp_dir=$2
sort_col=$3
out_name=$4
echo "Pre-sorting and uniqueness length:"
wc -l $in_file

echo "Carry out full-line sort for uniqueness:" 
LC_ALL=C sort $in_file -u -T $temp_dir -S ${MAX_RAM}M --parallel=1 > ${in_file}_2
mv ${in_file}_2 ${in_file}

echo "Post-sorting and uniqueness length:"
wc -l ${in_file}

LC_ALL=C sort $in_file -t $'\t' -k${sort_col},${sort_col} -T $temp_dir -S ${MAX_RAM}M --parallel=1 > ${in_file}_2
mv ${in_file}_2 $out_name

