import os, sys
import pandas as pd
from statsmodels.stats.multitest import multipletests

output_dir = sys.argv[1]
tag = sys.argv[2]

my_pattern = ".tmp_pvals_"
all_gene_files = [os.path.join(output_dir, filename) for filename in os.listdir(output_dir) if filename.startswith(my_pattern)]

def bh_correction_and_save(in_file):
    gene_name = os.path.basename(in_file)[len(".tmp_pvals_" + tag) + 1: -4]  # extract gene name from filename
    input_data = pd.read_csv(in_file, sep="\t", header=None, names=["row.idx", "__", "pval"])

    adjusted_pvals = multipletests(input_data["pval"], method="fdr_bh")[1]
    input_data["adjusted"] = adjusted_pvals

    os.remove(in_file)

    output_path = os.path.join(os.path.dirname(in_file), f".tmp_bh_adjusted_{tag}_{gene_name}.txt")
    input_data.to_csv(output_path, sep="\t", index=False, header=False)

writing_results = [bh_correction_and_save(file) for file in all_gene_files]
