#!/bin/bash
set -e

SCRIPT_DESCRIPTION="2_add_columns/1.1v1_get_FDRs.sh: Save the p-values associated with each gene and save the row numbers. Sequentially, load in genes p-values and perform Benjamini-Hochberg p-value adjustment via R p.adjust function. Dependent scripts: 1.3v1_get_bh_one_gene.R."

raw_input=$1
tag=$2
scripts_dir=$3
pval_col=$4
gene_col=$5
pval_strat=$6
output_dir=$7

## delete previous files if necessary since '>>' is used. This should clean up when it's done, but might not.
mkdir -p ${output_dir}/${tag}_gene_files

if compgen -G ${output_dir}/${tag}_gene_files/.tmp_pvals_${tag}_*.txt; then
	find ${output_dir}/${tag}_gene_files -type f -name ".tmp_pvals_${tag}_*.txt" -ls | awk '{print $NF}' | xargs rm
fi
echo "All args:" $@

## check whether the provided gene is a string or a column number.
if [[ $gene_col =~ ^[0-9]+$ ]]; then
	gene_is_col=1
else
	gene_is_col=0
fi

if [ "pval_strat" != "raise10" ]; then
	LC_ALL=C awk -v pval_col="$pval_col" -v gene_col="$gene_col" -v gene_is_col="gene_is_col" -v output_dir="$output_dir" -v tag="$tag" 'BEGIN { FS="\t"; OFS="\t" } { 
		if (gene_is_col==1) {
			gene=$(gene_col)
		} else {
			gene=gene_col
		}
		pval=$(pval_col);
		new_out_dir=(output_dir "/" tag "_gene_files/.tmp_pvals_" tag "_" gene ".txt")
		print NR, gene, pval >> new_out_dir;
	}' "$raw_input"
else
	LC_ALL=C awk -v pval_col="$pval_col" -v gene_col="$gene_col" -v gene_is_col="gene_is_col" -v output_dir="$output_dir" -v tag="$tag" 'BEGIN { FS="\t"; OFS="\t" } { 
		if (gene_is_col==1) {
			gene=$(gene_col)
		} else {
			gene=gene_col
		}
		pval=10^($(pval_col));
		new_out_dir=(output_dir "/" tag "_gene_files/.tmp_pvals_" tag "_" gene ".txt")
		print NR, gene, pval >> new_out_dir;
	}' "$raw_input"
fi


## get BH adjustment for each unique gene+p-value file. Also, delete the temporary gene+p-value file. 
$python_use $scripts_dir/column_imputing_scripts/get_bh_all_genes.py ${output_dir}/${tag}_gene_files $tag

# cat these files together, then sort by row number which was saved previously. Sorting keeps intact the original row numbers. 
# Need to use find and xargs because the number of files is too large for normal cat and rm.
find ${output_dir}/${tag}_gene_files -type f -name ".tmp_bh_adjusted_${tag}*" -ls | awk '{print $NF}' | xargs cat > ${output_dir}/${tag}_gene_files/.tmp_all_fdrs_${tag}.txt
sort -n ${output_dir}/${tag}_gene_files/.tmp_all_fdrs_${tag}.txt -T ${output_dir}/${tag}_gene_files -S 2000M > ${output_dir}/${tag}_gene_files/.tmp_all_fdrs_${tag}.txt_2
cut -f4 ${output_dir}/${tag}_gene_files/.tmp_all_fdrs_${tag}.txt_2 > ${output_dir}/.tmp_all_fdrs_${tag}.txt

rm ${output_dir}/${tag}_gene_files/.tmp_all_fdrs_${tag}.txt_2

rm -r ${output_dir}/${tag}_gene_files
