#!/bin/bash
set -e

SCRIPT_DESCRIPTION="2.0v1_join_multi_cols.sh: Take in multiple files and join them column-wise using the "paste" function. The last argument is not an input file, but rather the output."

## Join columns to the raw input before re-ordering the columns
if [ $# -eq 0 ]; then
    echo ERROR: no files to join!
fi

all_but_out_file=$(echo "$@" | rev | cut -d ' ' -f 2- | rev)
out_file=$(echo "$@" | rev | cut -d ' ' -f 1 | rev)

paste -d "\t" $all_but_out_file > $out_file

# pr $all_but_out_file -m -t -s -c > $out_file
