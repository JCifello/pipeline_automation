"""
Given the list of characters that can be used to split the input data, use provided indexes to create new columns with this new data.

Test command: 
python /mnt/data/jeffrey/FILER/scripts/pipeline_automation/src/position_splitter.py ./in_data/test_gtex.txt ./out_data/test_out.txt "_" 2 True --chr_index 1 --start_index 2 --ref_index 3 --alt_index 4

"""
import argparse


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("in_data", help="Path to the input file.")
    parser.add_argument("out_file", help="Path to the output file, containing only the columns to be added.")
    parser.add_argument("splitter_chrs", type=str)
    parser.add_argument("col_to_split", type=int)
    parser.add_argument("add_chr_pos", choices=[True, False], type=bool)
    parser.add_argument("has_header", choices=[True, False], type=bool)
    parser.add_argument("is_base_one", choices=[True, False], type=bool)
    parser.add_argument("chr_index", type=str)
    parser.add_argument("start_index", type=str)
    parser.add_argument("stop_index", type=str)
    parser.add_argument("ref_index", type=str)
    parser.add_argument("alt_index", type=str)
    parser.add_argument("a1_index", type=str)
    parser.add_argument("a2_index", type=str)
    parser.add_argument("tested_index", type=str)
    parser.add_argument("target_index", type=str)
    parser.add_argument("variant_id_index", type=str)
    args = parser.parse_args()
    
    my_chars = [ i for i in args.splitter_chrs ]
    kwarg_dict = {i[0]:int(i[1])-1 for i in args._get_kwargs() if ("index" in i[0]) and (i[1]!="NR")}
    kwarg_back_index = { kwarg_dict[k]:k for k in kwarg_dict.keys()}
    sorted_idx = list(kwarg_back_index.keys())
    sorted_idx.sort
    
    ## need to add when one column is used for two. 
    
    with open(args.in_data, "r") as f_old:
        with open(args.out_file, "w") as f_new:
            line = f_old.readline()
            if args.has_header==True:
                line = f_old.readline()
            
            while line:
                val_to_split = line.split("\t")[args.col_to_split-1]
                
                split_list = [val_to_split]
                for split_char in my_chars:
                    split_list = [i.split(split_char) for i in split_list]
                    new_split_list = []
                    for split_val in split_list:
                        new_split_list=new_split_list+split_val
                        split_list=new_split_list  

                f_new.write(line.rstrip())
                
                if "chr_index" in kwarg_dict.keys():
                    if "chr" in split_list[kwarg_dict["chr_index"]]:
                        chr_w_chr = split_list[kwarg_dict["chr_index"]]
                        chr_num = chr_w_chr[3:]
                    else:
                        chr_num = split_list[kwarg_dict["chr_index"]]
                        chr_w_chr = "chr"+chr_num
                
                for col_name in kwarg_dict.keys():
                    if kwarg_dict[col_name] != "NR":
                        if col_name == "chr":
                            f_new.write("\t{}".format(chr_w_chr))
                            continue
                        kwarg_col_num = kwarg_dict[col_name]
                        f_new.write("\t{}".format(split_list[kwarg_col_num]))
                
                if args.add_chr_pos==True and args.chr_index!="NR":
                    if args.is_base_one == True:
                        f_new.write("\t{}:{}".format(chr_num, split_list[kwarg_dict["start_index"]]))
                    else:
                        my_pos=int(split_list[kwarg_dict["start_index"]])+1
                        f_new.write("\t{}:{}".format(chr_num, str(my_pos)))
                
                f_new.write("\n")
                line = f_old.readline()
  