#!/bin/bash

args=("input_dat" "output" "ref_index" "ref_genes" "tag" "is_base_one" "qtl_chr" "qtl_pos" "target_chrom" "target_start" "target_end")

for i in "${!args[@]}"; do
    let ii=i+1
    arg_name=${args[$i]}
    arg_value=${!ii}
    printf -v "$arg_name" "$arg_value"
done

if [ "$target_end" == "" ]; then
	target_end="none"
fi

out_dir=$(dirname "$output")
target_locations="$out_dir/.tmp_target_locations_${tag}.bed"

## although QTL/snp positions are corrected to be 0-based, these have to be fixed for the qtl target. 
if [ "$is_base_one" == "true" ]; then
	base_one_correction=1
else
	base_one_correction=0
fi

echo "Getting target regions."
awk -v target_chrom_col="$target_chrom" \
	-v target_start_col="$target_start" \
	-v target_end_col="$target_end" \
	-v base_one_correction=$base_one_correction 'BEGIN{FS="\t";OFS="\t"}{
	target_chrom=("chr" $target_chrom_col)
	gsub("chrchr", "chr", target_chrom)
	gsub(target_start_col, "-1", "")
	target_start=$target_start_col
	if (target_end_col=="none") {
		target_end=target_start+1
	} else {
		target_end=$target_end_col
	}
	
	print target_chrom,target_start-base_one_correction,target_end-base_one_correction
}' "$input_dat" > "$target_locations"

echo "Zipping..."
bgzip "$target_locations" --force --stdout > "${target_locations}.gz"

echo "Doing Giggle search..."
target_gene_out="$out_dir/.tmp_target_gene_annot_${tag}.bed"
$Giggle search -i "${ref_index}" -q "${target_locations}.gz" -o -v -b > "$target_gene_out"

## process the output
echo "Creating unique anchor and gene keys..."
awk 'BEGIN{FS="\t"; OFS="\t"}{
	get the gene name found
	
	## target position. TODO: check whether we should correct these/add 1
	target_pos=$4":"$5"-"$6":"$9
	
	split($7,partition_attr,";")
	is_split=0
	uniq_idx=""
	del all_sym
	del all_ens
	del all_start
	del all_stop
	for (i in partition_attr) {
		my_attr=partition_attr[i]
		split(my_attr, split_attr, "=")
		if (split_attr[1]=="geneSymbol") {
			# print "my symbol:"split_attr[2]
			split(split_attr[2], split_sym, ",")
			
			if (length(split_sym)>1) { 
				is_split=1 
			}
			
			## get the unique gene symbols
			split("", used_symbols, ".")
			
			for (j in split_sym) {
				new_symbol=split_sym[j]
				if (used_symbols[new_symbol]=="") {
					uniq_idx=uniq_idx","j
					used_symbols[new_symbol]="stored"
				}
			}
			
		}
		
		if (split_attr[1]=="gene_id") all_ens=split_attr[2]
		if (split_attr[1]=="start") all_start=split_attr[2]
		if (split_attr[1]=="end") all_stop=split_attr[2]
		if (split_attr[1]=="geneSymbol") all_sym=split_attr[2]
		if (split_attr[1]=="ID") all_id=split_attr[2]
		
	}
	uniq_idx=substr(uniq_idx, 2, length(uniq_idx)-1)
	split(uniq_idx,idx_split,",")
	split(all_ens, split_ens, ","); split(all_start, split_start, ",")
	split(all_stop, split_stop, ","); split(all_sym, split_sym, ",")
	split(all_id, split_id, ",")
	
	if (length(idx_split)>1) {
		u_ens=""; u_start=""; u_stop=""; u_sym=""; u_coord=""; u_id=""
		for (u_idx in idx_split) {
			my_idx=idx_split[u_idx]
			u_ens=u_ens","split_ens[my_idx]
			u_sym=u_sym","split_sym[my_idx]
			u_coord=u_coord","$4":"split_start[my_idx]"-"split_stop[my_idx]":"$9
			u_id=u_id","split_id[my_idx]
		
		}
		u_ens=substr(u_ens, 2,length(u_ens)-1); u_coord=substr(u_coord, 2,length(u_coord)-1);
		u_sym=substr(u_sym, 2,length(u_sym)-1); u_id=substr(u_id, 2,length(u_id)-1)
	
	} else { # if only one gene symbol used, just pull the first one. 
		u_ens=split_ens[1]
		u_sym=split_sym[1]
		u_coord=$4":"split_start[1]"-"split_stop[1]":"$9
		u_id=split_id[1]

	}	
	
	## get the gene_symbol
	symbol_match=$7
	gsub(".*geneSymbol=", "", symbol_match)
	gsub(";.*", "", symbol_match) # get rid of trailing entries
	
	print $1,$2,$3,u_ens,u_sym,u_coord,u_id
	}' "$target_gene_out" > "$out_dir/.tmp_span_gene_key_${tag}.bed"

cat "$out_dir/.tmp_span_gene_key_${tag}.bed" | sort -u > "$out_dir/.tmp_span_gene_key_${tag}.bed_2"
mv "$out_dir/.tmp_span_gene_key_${tag}.bed_2" "$out_dir/.tmp_span_gene_key_${tag}.bed"

echo "Writing to a new file..."
awk  -v target_chrom_col="$target_chrom" \
    -v target_start_col="$target_start" \
    -v target_end_col="$target_end" \
	-v qtl_chr_col="$qtl_chr" \
	-v qtl_pos_col="$qtl_pos" \
	-v base_one_correction=$base_one_correction 'BEGIN{FS="\t"; OFS="\t"}
FILENAME==ARGV[1]{
	# save the previous target/gene combinations into memory.
    pos_id=($1"-"$2"-"$3)
    if ($4 != "n/a") {
        if (pos_and_gene[pos_id]=="") {
            pos_and_gene[pos_id]=$7
            pos_and_ens[pos_id]=$4
            pos_and_sym[pos_id]=$5
            pos_and_coord[pos_id]=$6
        } else {
            pos_and_gene[pos_id]=(pos_and_gene[pos_id]"|"$7)
            pos_and_ens[pos_id]=(pos_and_ens[pos_id]"|"$4)
            pos_and_sym[pos_id]=(pos_and_sym[pos_id]"|"$5)
            pos_and_coord[pos_id]=(pos_and_coord[pos_id]"|"$6)
        }
    }
}
FILENAME==ARGV[2] {
    ### establish columns and base-0
	target_chrom=("chr" $target_chrom_col)
	gsub("chrchr", "chr", target_chrom)
	target_start=$target_start_col
	if (target_end_col=="none") {
		target_end=target_start+1
	} else {
		target_end=$target_end_col
	}
	
	target_start=target_start-base_one_correction
	target_end=target_end-base_one_correction
	target_pos_id=target_chrom"-"target_start"-"target_end

	## set some default values
	target_strand = "."
	target_coord = "."
	target_sym = "."
	target_ens = "."

	### get the gene(s) assoc with this interval/peak. Summarize. 
	if (pos_and_gene[target_pos_id] != "") {
		target_gene=pos_and_gene[target_pos_id]
		target_sym=pos_and_sym[target_pos_id]
		target_coord=pos_and_coord[target_pos_id]
		target_ens=pos_and_ens[target_pos_id]
	}
	## get the strand(s)
	new_split_coord=""
	split(target_coord,coord_split_vbar, "\\|")
	for (i in coord_split_vbar) {
		split(coord_split_vbar[i], element_split_comma, ",")
		for (j in element_split_comma) {
			new_split_coord=new_split_coord","element_split_comma[j]
		}
	}
	new_split_coord = substr(new_split_coord, 2, length(new_split_coord)-1)
	split(new_split_coord, new_coord_split, ",")
	
	if (length(new_coord_split)>1) {
		for (i in new_coord_split) {
			this_coord=new_coord_split[i]
			split(this_coord, this_coord_split, ":")
			this_strand=this_coord_split[3]
			target_strand=this_strand
			if ((i!=1) && (this_strand!=last_strand)) target_strand="."
			last_strand=this_strand
		}
	} else {
		split(target_coord, this_coord_split, ":")
		target_strand=this_coord_split[3]
	}

	if (target_strand=="") target_strand="."

	## calculate the qtl-to-target distance
	qtl_dist="."
	qtl_chrom=("chr" $qtl_chr_col)
	gsub("chrchr","chr",qtl_chrom)
	qtl_pos=$qtl_pos_col-base_one_correction
	
	if (target_strand==".") {
		qtl_dist="."
	} else {
		if (coord_split[3]=="+") {
			qtl_dist=qtl_pos-target_start
		} else {
			qtl_dist=qtl_pos-target_end
		}
	}

	print target_strand,target_coord,target_sym,target_ens,qtl_dist
	
}' "$out_dir/.tmp_span_gene_key_${tag}.bed" "$input_dat" > "$out_dir/.tmp_add_anchor_genes_${tag}.bed"



## things we want the above section to get: 
# new_awk_dict["target_coord"] = str(max_col+1)
# new_awk_dict["target_ensembl_id"] = str(max_col+2)
# new_awk_dict["target_gene_symbol"] = str(max_col+3)
# new_awk_dict["qtl_dist_to_target"] = str(max_col+4)
# can have two coord targets...only look for mRNA target?

# rm "$out_dir/.tmp_span_gene_key_${tag}.bed"
# rm "$target_locations"
# rm "${target_locations}.gz"
# rm "$target_gene_out"
