#!/bin/bash

SCRIPT_DESCRIPTION="position_splitter.sh: Calls position_splitter.py. Takes in arguments to split one column into a number of required columns to be used by hipFG. If necessary, will carry out the chr:pos format such the next script no longer needs to be called."

in_data=$1
scripts_dir=$2
out_file=$3
splitter_chars=$4
col_to_split=$5
has_header=$6
add_chr_pos=$7
is_base_one=$8
chr_index=$9
start_index=${10}
stop_index=${11}
ref_index=${12}
alt_index=${13}
a1_index=${14}
a2_index=${15}
tested_index=${16}
target_index=${17}
variant_id_index=${18}

python $scripts_dir/column_imputing_scripts/position_splitter.py $in_data $out_file $splitter_chars $col_to_split $add_chr_pos $has_header $is_base_one $chr_index $start_index $stop_index $ref_index $alt_index $a1_index $a2_index $tested_index $target_index $variant_id_index
