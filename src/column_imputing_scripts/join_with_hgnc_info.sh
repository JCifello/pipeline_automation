#!/bin/bash
set -e

SCRIPT_DESCRIPTION="join_with_hgnc_info.sh: Lookup the specified input column in the joined Gencode-HGNC dataset specified in the input table. The lookup column is either your target or target_gene_symbol, with the lookup type in one of the categories provided in resources/target_lookup_types.txt."

in_dat=$1
tag=$2
output_dir=$3
hgnc_dat=$4
hgnc_col_lookup=$5
target_col=$6
target_type=$7
qtl_chr=$8
qtl_start=$9
qtl_chr_prefix=${10}
get_gene_symbol=${11}

echo -e "\nComparing input data target (gene/protein/molecule) to gene information saved in: ${hgnc_dat}\nThis reference file is used to determine strand, gene distance to QTL, and gene symbol.\n"

## if position HAS minus it is already base-one and does not need to be adjusted. Otherwise, save flag to add one later.
add_one=$(echo $qtl_start | awk '{split($1, pos_split, "-"); if (length(pos_split)>1){printf "false"} else {printf "true"}}')
pos_only=$(echo $qtl_start | awk '{split($1, pos_split, "-"); printf pos_split[1]}')

echo -n "" > $output_dir/.new_hgnc_cols_${tag}.bed # make sure file starts empty. 

## determine whether the target is a column or a value:
if [[ $target_col =~ ^[0-9]+$ ]]; then
    target_is_col=1
else
    target_is_col=0
fi

chr_prefix=$(echo $qtl_chr_prefix | sed 's/none//g')

LC_ALL=C awk -v target_col="$target_col" \
-v target_type="$target_type" \
-v get_gene_symbol=$get_gene_symbol \
-v pos_only=$pos_only \
-v qtl_chr=$qtl_chr \
-v add_one=$add_one \
-v target_is_col=$target_is_col \
-v chr_prefix="$chr_prefix" 'BEGIN {FS="\t"; OFS="\t"; over_mil_away=0; not_found_count=0; found_count=0}
{
if (FILENAME==ARGV[1]) {
	## get the column number to lookup in the reference
	# get the column number from the hgnc_dat file needed for joining
	hgnc_col=$1
	hgnc_lookup_type=$2
	if (hgnc_lookup_type==target_type) {
		final_hgnc_col=hgnc_col;
		final_hgnc_lookup_type=hgnc_lookup_type;
	}
	last_dict_line=NR
} 
if (FILENAME==ARGV[2]) {
	## using the column number selected above, build a lookup dictionary for the specified lookup type.
	if (FNR==1){ next }
	my_key=$final_hgnc_col
	chr_dict[my_key]=$1
	start_dict[my_key]=$2
	end_dict[my_key]=$3
	strand_dict[my_key]=$4
	symbol_dict[my_key]=$9

	if (used_key_and_genes[my_key"-"$7]!="stored") {
		if (ensembl_id_dict[my_key]=="") {
			ensembl_id_dict[my_key]=$7
		} else {
			## if the input target is associated with multiple genes, delimit these and save the unique ones.
			ensembl_id_dict[my_key]=ensembl_id_dict[my_key]"|"$7
		}
	}	
	used_key_and_genes[my_key"-"$7] = "stored"
	last_hgnc_line=NR
}
if (FILENAME==ARGV[3]) {
	## use the lookup dictionary assembled above to get strand, qtl_dist, and more.
	if (target_is_col==1) { # target may be a string.
		my_key=$target_col
		if (target_type == "ensembl_id_no_ver") { gsub(/\..*/, "", my_key) } 
	} else {
		my_key=target_col
	}	

	qtl_pos=$pos_only
	if (add_one=="true") {
		qtl_pos += 1
	}
	
	my_dist="."
	
	my_chr=(chr_prefix $qtl_chr)
	if (chr_dict[my_key]==my_chr) {
		if (strand_dict[my_key]=="+") {
			my_dist=qtl_pos-start_dict[my_key]
		}
		if (strand_dict[my_key]=="-"){
			my_dist=qtl_pos-end_dict[my_key]
		}

		if ((my_dist>=1000000)||(my_dist<=-1000000)) {
			over_mil_away += 1
		}
	}

	if (strand_dict[my_key]!="") {
		found_count+=1
		out_strand=strand_dict[my_key]
		out_coord=(chr_dict[my_key]":"start_dict[my_key]-1"-"end_dict[my_key]":"strand_dict[my_key])
		out_ensembl_id=ensembl_id_dict[my_key]
		out_symbol=symbol_dict[my_key]
		out_flag=""
	} else {
		not_found_count+=1
		out_strand="."
		out_coord="NOT_FOUND"
		out_ensembl_id="NOT_FOUND"
		out_symbol="NOT_FOUND"
		out_flag=";NOT_FOUND"
		
		if ((target_type == "ensembl_id_no_ver") || (target_type == "ensembl_gene_id")) {
			gsub(/\..*/, "", my_key)
			out_ensembl_id=(my_key"_NOT_FOUND")
		}
		if (target_type=="symbol") {
			out_symbol=(my_key"_NOT_FOUND")
		}
	}

	if (get_gene_symbol==0) { 
		print out_strand,out_coord,my_dist,out_ensembl_id,out_flag >> "'$output_dir/.new_hgnc_cols_${tag}.bed'"
	} else {
		print out_strand,out_coord,out_symbol,my_dist,out_ensembl_id,out_flag >> "'$output_dir/.new_hgnc_cols_${tag}.bed'" 
	}
}
} END { 
	print over_mil_away" QTLs found where variant is at least 1MB away from target gene."
	print "Genes (rows) found in reference: "found_count " (" found_count/(found_count+not_found_count)*100 "%)"
	print "Genes (rows) NOT found in reference: "not_found_count" (" not_found_count/(found_count+not_found_count)*100 "%)"
	print "Total genes (rows): "found_count+not_found_count
}' $hgnc_col_lookup $hgnc_dat $in_dat

echo ""
