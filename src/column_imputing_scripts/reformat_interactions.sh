#!/bin/bash

SCRIPT_DESCRIPTION="reformat_interactions_2: Following the awk rearrangement, update the output to split each interaction across two lines."
input_dat="$1"
output="$2"

### input fields in order. Some are actually "NR" and will be overwritten
## anchorChrom;anchorStart;anchorEnd;interactionAttr;chrom;chromStart;chromEnd;name;score;value;exp;color;sourceChrom;sourceStart;sourceEnd;sourceName;sourceStrand;targetChrom;targetStart;targetEnd;targetName;targetStrand
## use awk to restructure and print across two lines
awk 'BEGIN { FS="\t"; OFS="\t" }
{
    ## make sure that the anchors are in the correct order. Swap genes and positions if target position < source:
    if (($20<$15) || (($15==$20)&&($21<$16))) {
        # switch the anchor order. Change values of expected anchor columns. 
        new_14=$19; new_15=$20; new_16=$21; new_17=$22; new_18=$23
        new_19=$14; new_20=$15; new_21=$16; new_22=$17; new_23=$18
        $14=new_14; $15=new_15; $16=new_16; $17=new_17; $18=new_18
        $19=new_19; $20=new_20; $21=new_21; $22=new_22; $23=new_23

        ## Swap the order of sourceGene and targetGene
        split($5, interAttr, ";")

        # Initialize variables for sourceGene and targetGene values
        sourceGene = ""
        targetGene = ""
        new_attr=""
        source_seen=0; target_seen=0; genes_added=0

        # Process each key-value pair
        for (i = 1; i <= length(interAttr); i++) {
            # Split the key-value pair into key and value
            split(interAttr[i], kv, "=")

            # Check if key is "sourceGene" or "targetGene"
            if (kv[1] == "sourceGene") {
                source_seen=1
                # Swap the value with targetGene
                targetGene = kv[2]
            }
            if (kv[1] == "targetGene") {
                target_seen=1
                # Swap the value with sourceGene
                sourceGene = kv[2]
            }

            if ((target_seen==1)&&(source_seen==1)&&(genes_added==0)) {
                genes_str=("sourceGene=" sourceGene ";targetGene=" targetGene)
                if (new_attr=="") new_attr=genes_str
                else new_attr=(new_attr";"genes_str)
                genes_added=1
            }

            if ((kv[1] != "sourceGene") && (kv[1]!="targetGene")) {
                if (new_attr=="") new_attr=(kv[1]"="kv[2])
                else new_attr=(new_attr";"kv[1]"="kv[2])
            } 
        }
        $5 = new_attr
    }
    ## get the updated name
    # desired format: 
    name=$9
    split($5,attr_split,";")
    
    for (i in attr_split){
        split(attr_split[i],b,"=")
        if (b[1]=="sourceGene") source_gene_long=b[2];
        if (b[1]=="targetGene") target_gene_long=b[2];
    }

    split(source_gene_long, spl_sg, "|")
    split(target_gene_long, spl_tg, "|")
    delete sgs
    delete tgs
    for (i in spl_sg) {
        split(spl_sg[i],c," ")
        if (sgs[c[1]]=="") sgs[c[1]]=1
    }
    for (i in spl_tg) {
        split(spl_tg[i],c," ")
        if (tgs[c[1]]=="") tgs[c[1]]=1
    }
    short_sg=""; short_tg=""
    for (i in sgs) {
        if (short_sg=="") short_sg=i
        else short_sg=(short_sg"|"i)
    }
    for (i in tgs) {
        if (short_tg=="") short_tg=i
        else short_tg=(short_tg"|"i)
    }
    # print "demo full attr:"$5,"SHORT: ",short_sg,short_tg, "---LONG:",source_gene_long,target_gene_long
    $9=(name"___"short_sg"___"short_tg)
    
    new_attr=$5
    gsub(" ", "_", new_attr)    
    ## get each anchor to the front once, make sure first columns of UCSC bedInteract span both anchors.
    split("14 15 16",source_split," ")
    split("19 20 21",target_split," ")
    split("14 15 21",chrom_split," ")

    if (length(target_split)!=3){
        print "ERROR: Source and target anchor are identical."
        exit 1
    }

    ## print line for source anchor
    for (i in source_split) printf $source_split[i]"\t"
    
    printf NR"A\t"
    printf new_attr"\t"
    
    for (i in chrom_split) printf $chrom_split[i]"\t"
    for (i=9;i<NF;i++) { printf $i"\t"}
    print $NF

    ## print line for target anchor
    for (i in target_split) printf $target_split[i]"\t"

    printf NR"B\t"
    printf new_attr"\t"
    
    for (i in chrom_split) printf $chrom_split[i]"\t"
    for (i=9;i<NF;i++) { printf $i"\t"}
    print $NF

}' "$input_dat" > "$output"

# 1 anchorChrom
# 2 anchorStart
# 3 anchorEnd
# 4 interactionAttr
# 5 chrom
# 6 chromStart
# 7 chromEnd
# 8 name
# 9 score
# 10 value
# 11 exp
# 12 color
# 13 sourceChrom
# 14 sourceStart
# 15 sourceEnd
# 16 sourceName
# 17 sourceStrand
# 18 targetChrom
# 19 targetStart
# 20 targetEnd
# 21 targetName
# 22 targetStrand
