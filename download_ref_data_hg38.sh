#!/bin/bash
## Acquire from the FILER website the reference files necessary to process xQTLs for genome build hg38.

set -e

hipfg_root=$(dirname $(realpath $0))

new_dir=${1:-$(dirname $hipfg_root)/hipfg_large_references}

echo "Attempting to create directory $new_dir ..."
mkdir -p $new_dir

echo -e "\nDownloading dbSNP to $new_dir..."
wget https://tf.lisanwanglab.org/GADB/download/hipFG/dbSnp.b156.hg38.all.sorted_by_chrPosPair.bed -P $new_dir
# wget https://tf.lisanwanglab.org/GADB/download/hipFG/dbSnp.b156.hg38.all.sorted_by_chrPosPair.bed.gz -P $new_dir

echo -e "\nDownloading reference genome to $new_dir..."
wget https://tf.lisanwanglab.org/GADB/download/hipFG/hg38.fa -P $new_dir
wget https://tf.lisanwanglab.org/GADB/download/hipFG/hg38.fa.fai -P $new_dir


echo -e "\nDownloading target/gene information to $new_dir..."
wget https://tf.lisanwanglab.org/GADB/download/hipFG/hg38_joined_with_hgnc.reformatted.bed -P $new_dir

echo -e "\nSuccess: Four resources downloaded to $new_dir"
