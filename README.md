# hipFG
The Harmonization and Integration Pipeline for Functional Genomics (hipFG) is a tool to normalize data and metadata of diverse functional genomics formats.  
  
![hipFG_overview](images/hipFG Overview Figure Widescreen.png)  
  
For a more detailed explanation, please see the manuscript [here](https://www.biorxiv.org/content/10.1101/2023.04.21.537695v1).  
  
To use hipFG in its Docker container, please pull from [the Docker hub](https://hub.docker.com/r/wanglab/hipfg).  
  
Following installation below, try out the hipFG **Jupyter notebook tutorials** in the `notebooks` folder or downloadable [here](https://tf.lisanwanglab.org/GADB/download/hipFG/jupyter_notebooks/).  
  
**Table of Contents**  
  
[TOC]

## System Requirements
hipFG is compatible with both [Linux](#markdown-header-linux-local-install) and [MacOS](#markdown-header-macos-local-install), and is also available as a [Docker container](#markdown-header-docker-based-run):  
  
Linux: tested on Ubuntu 20, 22, Centos 8  
MacOS: tested on MacOS Ventura  
  

**Hardware requirements:**
Memory: 8GB RAM minimum. 5 GB buffer for sorting, etc.; can be customized using MAX_RAM in the `.ini` file [here](https://bitbucket.org/wanglab-upenn/hipfg/src/main/hipFG.ini) (define memory to use in terms of MB).  
Storage: 150 GB available recommended, less is OK for non-QTL sets. (67GB for QTLs + 5x the size of input data data.)  

## Usage
hipFG is run by executing `hipFG.sh` and specifying, at minimum `--mdt` to indicate the MDT:  
```
bash <your path>/hipFG.sh --mdt mdt.tsv
```
Additional options are available:
```
Usage:
bash hipFG.sh --mdt <your TSV>
        options:
                --mdt A TSV pointing to input files, output and temporary directories, and input metadata.
                --run_parallel Optional. true/false. Whether to run multiple samples at a time. Default false.
                --num_jobs Optional. Integer. How many samples to run at a time. Default 0/non-parallel.
                --testing Optional. true/false. If true, only write code for the first two samples. Default false.
                --run_now Optional. true/false. If true, run auto-generated scripts immediately. This may initiate parallel run if specified.
                --metadata_only Optional. true/false. If true, re-generate sample scripts but only execute metadata portions. Useful when MDT is updated but output data does not need to be re-generated. Default false.
                --replace_metadata Optional. true/false. Default behavior is for newest generated metadata to be appended to project-level metadata. If true, do not append to a master output metadata, and instead replace it. Use caution when calling! Default false.
				--simple_mdt Optional. true/false. If true, hipFG requires only 7 fields in MDT but skips metadata steps. Default false (20 required fields).
```
For additional information on inputs, please visit the [data descriptors guide](https://bitbucket.org/wanglab-upenn/hipfg/src/main/tutorials/data-descriptors-guide/).

## hipFG examples
Following [installation](#markdown-header-quick-install) and [set-up](#markdown-header-local-set-up) (for local installation), hipFG examples can be executed to validate hipFG functionality.  
Navigate to a testing directory and call `run_me.sh`. 
```
cd hipFG/examples/xQTLs/
bash run_me.sh
```  
Yielding:
```
Input table found. Using input_df.tsv
Setting /hipFG as the hipFG root directory. This is defined as the directory containing hipFG.sh.
Using hipFG_tmp.ini to select Python version to run script generation.
Using /usr/bin/python3 to run hipFG script generation.

Generating scripts for sample #1 (1/2)
Generating scripts for sample #2 (2/2)
************
Script generation complete.
Check run logs in /hipFG/examples/xQTLs/out_scripts/run_logs for messages, warnings, and log output.

Executing now.
Saving temporary files in /hipFG/examples/xQTLs/out_scripts.

Will run Giggle indexing of directory: /hipFG/examples/xQTLs/out_files/all/OTHER/eQTL/bedxQTL/hg38
Will run Giggle indexing of directory: /hipFG/examples/xQTLs/out_files/all/SNP/eQTL/bedxQTL/hg38
Will run Giggle indexing of directory: /hipFG/examples/xQTLs/out_files/significant/OTHER/eQTL/bedxQTL/hg38
Will run Giggle indexing of directory: /hipFG/examples/xQTLs/out_files/significant/SNP/eQTL/bedxQTL/hg38
Skipping header
Skipping header
Indexed 501 intervals.
Skipping header
Skipping header
Indexed 18 intervals.
Skipping header
Skipping header
Indexed 5352 intervals.
Skipping header
Skipping header
Indexed 95 intervals.

Standardized data outputs saved in:
out_files
```
You can perform checks for `examples/intervals` and `examples/interactions` as well. However, `examples/xQTLs` requires the most dependencies and indicates a more thorough hipFG installation.  
  
## Installation
### Local Installation
hipFG can be installed via a git clone:
```
git clone https://bitbucket.org/wanglab-upenn/hipFG.git hipFG
```
hipFG requires each of the following software/tools to be installed and accessible to hipFG:  
  
1. htslib (tabix)  
2. samtools  
3. FILER_giggle  
4. Python  
5. Python package: pandas  
6. Python package: statsmodels  
  
Please see the provided instructions for [MacOS](#markdown-header-macos-local-install) and [Linux](#markdown-header-linux-local-install).  

### Docker-based run
hipFG is available via the [Docker hub](https://hub.docker.com/r/wanglab/hipfg).
```
docker pull wanglab/hipfg
```  
The Docker container can be applied to external data by:  
  
1. Providing absolute paths to the mounted directories indicating (i) input data, (ii) the temporary working directory, (iii) the output directory, and (iv) the project directory containing the File Config and MDT. An optional (v) reference files directory must be provided for normalizing QTLs.  
2. Executing `hipFG.sh` with desired options.  

For example, following the `docker pull`, interval annotations are normalized via hipFG using the local directories:  
```
docker run -v /home/jcifello/input_data:/home/jcifello/input_data \
		-v /home/jcifello/test_docker/my_output:/home/jcifello/test_docker/my_output \
		-v /home/jcifello/test_docker/my_temp:/home/jcifello/test_docker/my_temp \
		-v /home/jcifello/test_docker/project_dir:/home/jcifello/test_docker/project_dir \
		-w /app hipfg_mount bash /app/hipFG/hipFG.sh --mdt /home/jcifello/test_docker/project_dir/mdt.tsv --run_now true
```
And QTLs can be executed similarly, noting an additional mounted directory for the reference files:
```
docker run -v /home/jcifello/input_qtls:/home/jcifello/input_qtls \
		-v /home/jcifello/test_docker/my_output:/home/jcifello/test_docker/my_output \
		-v /home/jcifello/test_docker/my_temp:/home/jcifello/test_docker/my_temp \
		-v /home/jcifello/test_docker/project_dir:/home/jcifello/test_docker/project_dir \
		-v /home/jcifello/hipfg_ref_files:/home/jcifello/hipfg_ref_files \
		-w /app hipfg_mount bash /app/hipFG/hipFG.sh --mdt /home/jcifello/test_docker/project_dir/mdt_qtl.tsv --run_now true"
```
  
As a template, define the following variables for your system, and execute the docker command below:
```
input_dir=</abs/path/>
output_dir=</abs/path/>
project_dir=</abs/path/>

docker run -v $input_dir:$input_dir \
           -v $output_dir:$output_dir \
           -v $project_dir:$project_dir \
           -w /app hipfg/wanglab bash /app/hipFG/hipFG.sh --mdt $project_dir/mdt.tsv --run_now true
```
**NOTE:** Any paths provided in the MDT must be _absolute_ and _downstream_ of a mounted directory.  
For application to QTLs, carry this out with an additional mount for `$qtl_references_dir`.  
  
#### Example set-up with Docker
To practice running the Docker container on external data, we can use files available in this repo. Download the xQTL related files here:
```
## download the input data
wget https://bitbucket.org/wanglab-upenn/hipfg/raw/9984f730fb6486133348ec3e233eb2128aa009cf/examples/xQTLs/inputs/Alasoo_2018_ge_macrophage_IFNg.all.tsv.gz
wget https://bitbucket.org/wanglab-upenn/hipfg/raw/9984f730fb6486133348ec3e233eb2128aa009cf/examples/xQTLs/inputs/GTEx_ge_brain_spinal_cord.all.tsv.gz

## download data descriptors
wget https://bitbucket.org/wanglab-upenn/hipfg/raw/9984f730fb6486133348ec3e233eb2128aa009cf/examples/xQTLs/config.txt
wget https://bitbucket.org/wanglab-upenn/hipfg/raw/9984f730fb6486133348ec3e233eb2128aa009cf/examples/xQTLs/mdt.tsv

## Init Config
wget https://bitbucket.org/wanglab-upenn/hipfg/raw/be41a5dcce4e5726e4a457c9f5876438a80f1996/hipFG.ini
```
Relocate the input files to an input directory of your choice. Update the following fields in this MDT to point to the recent downloads:  
  
1. Input file  
2. File config 
3. Init config
4. Output directory  
5. Temp directory  
  
**Note**: All paths must be absolute when using the Docker container.  
  
Lastly, update these paths of the new `.ini` file:
```
dbSNP_hg38=/app/hipFG/examples/xQTLs/reference/dbSNP_custom_example_reference.txt
hgnc_gencode_hg38=/app/hipFG/examples/xQTLs/reference/hg38_reference_genes.txt
reference_genome_hg38=/app/hipFG/examples/xQTLs/reference/hg38_short_reference_genome.fa
```
With the absolute paths used in the MDT, mount the necessary (absolute) paths and run hipFG via the Docker:  
```
input_dir=</abs/path/>
output_dir=</abs/path/>
project_dir=</abs/path/>

docker run -v $input_dir:$input_dir \
           -v $output_dir:$output_dir \
           -v $project_dir:$project_dir \
           -w /app hipfg/wanglab bash /app/hipFG/hipFG.sh --mdt $project_dir/mdt.tsv --run_now true
```


#### Running built-in examples with Docker  
The examples carried out to verify successful install [above](#markdown-header-hipfg-examples) can also be executed within the Docker container:
```
docker run -w /app hipfg_mount sh -c "cd /app/hipFG/examples/intervals/ && bash run_me.sh"
docker run -w /app hipfg_mount sh -c "cd /app/hipFG/examples/interactions/ && bash run_me.sh"
docker run -w /app hipfg_mount sh -c "cd /app/hipFG/examples/xQTLs/ && bash run_me.sh"
```

## Linux Local Install
hipFG has a number of dependencies including common packages and some bioinformatics tools. The commands below will install these.  
```
sudo apt-get update
sudo apt-get install libbz2-dev liblzma-dev lbzip2 bc gcc make autoconf zlib1g-dev libcurl4-openssl-dev libssl-dev ruby python3-pip parallel
```
The tool dependencies are below. Note that `--prefix=<your path>` may be used in the `./configure` commands to save these tools outside of shared directories.  
```
wget https://github.com/samtools/htslib/releases/download/1.17/htslib-1.17.tar.bz2; tar -xvf htslib-1.17.tar.bz2; cd htslib-1.17/; ./configure; make; sudo make install; cd ..
wget https://github.com/samtools/samtools/releases/download/1.17/samtools-1.17.tar.bz2; tar -xvf samtools-1.17.tar.bz2; cd samtools-1.17/; ./configure --without-curses; make; sudo make install; cd ..
```
Python and will also be required, and two Python packages:  
```
sudo apt-get install python3
pip install pandas statsmodels
```
Lastly, install one Github repository in the same folder as hipFG to make use of [FILER Giggle](https://github.com/pkuksa/FILER_giggle/) for rapid indexing and querying. This repo is selected to make use of patches for compatibility with [FILER](https://tf.lisanwanglab.org/FILER/): 
```
git clone https://github.com/pkuksa/FILER_giggle.git; cd FILER_giggle; make; cd ..
```
Consult the [hipFG set-up](#markdown-header-local-set-up) to point hipFG to all necessary tools/dependencies.  
  
hipFG can then be installed:  
```
git clone https://JCifello@bitbucket.org/wanglab-upenn/hipfg.git hipFG
```  

## MacOS Local Install
hipFG has been specifically designed to not require `sudo` permissions on MacOS systems. hipFG was installed and tested using conda. Conda was installed via the Conda MacOS [install guide](https://docs.conda.io/projects/conda/en/latest/user-guide/install/macos.html).  
  
Following the Conda install, the following commands can be run to get the necessary packages:  
```
xcode-select --install

conda config --add channels bioconda
conda config --add channels conda-forge

conda install -c anaconda autoconf=2.69
conda install -c anaconda wget

conda install -c bioconda tabix
conda install -c bioconda samtools=1.17
conda install -c anaconda openssl
```
Then install install one Github repository in the same folder as hipFG to make use of [FILER Giggle](https://github.com/pkuksa/FILER_giggle) for rapid indexing and querying. This repo is selected to make use of patches for compatibility with [FILER](https://tf.lisanwanglab.org/FILER/): 
```
git clone https://github.com/pkuksa/FILER_giggle.git; cd FILER_giggle; make -f Makefile.macos; cd ..
```

If you see an error where openssl cannot be found, try changing `/usr/local/opt/openssl/lib/` to `/usr/local/opt/openssl@1.11/lib/` in the definition of `INCLUDES` in `./src/Makefile` and/or `./src/Makefile.macos`.

Lastly, install Python and two necessary packages:
```
conda install -c anaconda python
conda install -c anaconda pandas
conda install -c anaconda statsmodels
```
Consult the [hipFG set-up](#markdown-header-local-set-up) to point hipFG to all necessary tools/dependencies.  
  
hipFG can then be installed:  
```
git clone https://JCifello@bitbucket.org/wanglab-upenn/hipfg.git hipFG
```  

## Local set-up
Local set-up requires the above dependencies somewhere on your system, and for `hipFG.ini` to point to them.
Navigate to your cloned `hipFG` directory. Execute the command `check_install.sh`, and edit `hipFG.ini` until you receive the message `Success: Provided paths appear to exist.`.  
  
For example, executing `check_install.sh` may yield:
```
ls: cannot access '/usr/local/bin/samtools': No such file or directory
```
In this case, edit the line assigning `samtools_use` to **your** local copy instead of the default, e.g.:
```
samtools_use="/mnt/data/bin/centos/samtools-1.14/samtools"
```
Running `check_install.sh` again, you may receive:
```
ls: cannot access '/your/large/resource/dir/': No such file or directory
```
To circumvent this and similar issues within the `## large reference files` section, these paths may be replaced with a period `.` **only** if you are not planning on processing QTLs with hipFG.  
  
Repeat executing `check_install.sh` and making corrections until the success message is obtained.  


### Set-up for Chromatin Interactions
To provide gene-context of interaction anchors, a genomic partition utilized in the tool [INFERNO](https://bitbucket.org/wanglab-upenn/INFERNO/) is used. This can be rapidly queried with Giggle. To download and set up, there are two options which are described below:  
  
1. Download this partition and symbolically link the `inferno_genomic_partition` folder to `/project/wang4/GADB/download/hipFG/inferno_genomic_partition`.  
2. Download this partition and manually re-index it at it's new location. This is recommended if you do not have `sudo` permissions.  

However, before either solution, both builds can be downloaded (58 MB) with the following script:
```
bash download_genomics_partition.sh <your download path>
```
This has an optional argument for a new download location (default `./`).

#### 1. Symbolic link
This directory is pre-indexed via Giggle for `/project/wang4/GADB/download/hipFG`. You can successfully carry out gene annotation if you use a symbolic link:
```
mkdir -p /project/wang4/GADB/download/hipFG
ln -s /your/absolute/path/inferno_genomic_partition /project/wang4/GADB/download/hipFG/inferno_genomic_partition
```
Note: This may require `sudo` permissions to create the `/project/...` folder.

#### 2. Re-index
Following download, rapid Giggle searches can be allowed by re-indexing. Locate the directory containing the new `giggle` executable downloaded in the [Mac](#markdown-header-macos-local-install) or [Linux](#markdown-header-linux-local-install) local install sections. Locate this executable and run Giggle on the absolute path of your recent downloads:
```
inferno_path=/your/absolute/path/inferno_genomic_partition
/your/absolute/path/FILER_giggle/bin/giggle index -i "$inferno_path/bed6/hg19/*gz" -o $inferno_path/bed6/hg19/giggle_index -s -f
/your/absolute/path/FILER_giggle/bin/giggle index -i "$inferno_path/bed6/hg38/*gz" -o $inferno_path/bed6/hg38/giggle_index -s -f
```
Giggle requires special permissions to carry out searched, so you may need to carry out these additional commands:
```
chmod a+rx $inferno_path/bed6/hg19/giggle_index
chmod a+r $inferno_path/bed6/hg19/giggle_index/*
chmod a+rw $inferno_path/bed6/hg19/giggle_index/cache.*

chmod a+rx $inferno_path/bed6/hg38/giggle_index
chmod a+r $inferno_path/bed6/hg38/giggle_index/*
chmod a+rw $inferno_path/bed6/hg38/giggle_index/cache.*
```

### Set-up for QTLs
Unlike genomic interval annotations, QTLs require large reference resources. These resources can be downloaded using one/both of the following commands:
```
bash download_ref_data_hg19.sh <your download path>
bash download_ref_data_hg38.sh <your download path>
```
These have an optional argument (default=`./`) for where to save the outputs.  

Following download, update `hipFG.ini` such that `large_resources_dir` indicates the absolute path to these downloads:
```
large_resources_dir=<your download path>
```
Each build requires the following storage:  
hg19: 64 GB  
hg38: 67 GB  
  
Note: Zipped versions of these references can be downloaded instead via wget on these links: [hg19](https://tf.lisanwanglab.org/GADB/download/hipFG/dbSnp.b156.hg19.all.sorted_by_chrPosPair.bed.gz) [hg38](https://tf.lisanwanglab.org/GADB/download/hipFG/dbSnp.b156.hg38.all.sorted_by_chrPosPair.bed.gz).

## Contact
Have questions or feedback? If you have a bitbucket account, you are more than welcome to post issues using the issues tab of the panel on the left-hand side. Alternatively, this repository is primarily maintained by Jeffrey Cifello, reachable at <jeffrey.cifello@pennmedicine.upenn.edu>.  
