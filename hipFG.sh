#!/bin/bash

# By Jeffrey Cifello 8-25-23

if [ $# -lt 2 ]; then
	echo "ERROR: Please assign the minimal descriptor table (MDT) using --mdt <your tsv>."
	echo -e "Usage:\nbash hipFG_create_scripts.sh --mdt <your TSV>\n\toptions:\n\t\t--mdt The minimal descriptor table: a TSV pointing to input files, output and temporary directories, and input metadata.\n\t\t--run_parallel Optional. true/false. Whether to run multiple samples at a time. Default false. \n\t\t--num_jobs Optional. Integer. How many samples to run at a time. Default 0/non-parallel.\n\t\t--testing Optional. true/false. If true, only write code for the first two samples. Default false.\n\t\t--run_now Optional. true/false. If true, run auto-generated scripts immediately. This may initiate parallel run if specified.\n\t\t--metadata_only Optional. true/false. If true, re-generate sample scripts but only execute metadata portions. Useful when MDT is updated but output data does not need to be re-generated. Default false.\n\t\t--replace_metadata Optional. true/false. Default behavior is for newest generated metadata to be appended to project-level metadata. If true, do not append to a master output metadata, and instead replace it. Use caution when calling! Default false.\n\t\t--simple_mdt Optional. true/false. If true, hipFG requires only 7 fields in MDT but skips metadata steps. Default false (20 required fields)."
	exit
fi

while [ $# -gt 0 ]; do            
    if [[ $1 == *"--"* ]]; then
      param="${1/--/}"
      declare $param="$2"
    fi       
  shift
done

if test -f "$mdt"; then
	echo "Input table found. Using" "$mdt"
else
	echo "ERROR: MDT cannot be found. If there are spaces, please add quotes around your filepath."
	exit
fi

hipFG_root=$(cd $(dirname $0) && pwd)
echo "Setting $hipFG_root as the hipFG root directory. This is defined as the directory containing hipFG.sh."
declare -x hipFG_root

echo "Running hipFG v$(cat $hipFG_root/VERSION)"

init_config=$(awk 'BEGIN{FS="\t"}NR==1{for (i=1;i<=NF;i++) { if (tolower($i)=="init config") {config_idx=i}}} NR==2{ print $config_idx}' "$mdt")
echo "Using $init_config to select Python version to run script generation."
source $init_config

echo -e "Using $python_use to run hipFG script generation.\n"

run_parallel=${run_parallel:-"false"}
num_jobs=${num_jobs:-0}
testing=${testing:-"false"}
run_now=${run_now:-"false"}
running_for_filer=${for_filer:-"false"}
metadata_only=${metadata_only:-"false"}
simple_mdt=${simple_mdt:-"false"}
replace_metadata=${replace_metadata:-"false"}
script_dir=$(dirname $(realpath ${BASH_SOURCE[0]}))

mdt=$(realpath "$mdt")

if [ $num_jobs -eq 0 ] && [ $run_parallel = "true" ]; then
	echo "ERROR: To run samples in parallel, please specify the number of jobs you want to use"
	exit
fi

i=0
my_date=$(date)
for line_num in $(seq 2 $(cat "$mdt" | wc -l)); do 
	if [ $testing = "true" ] && [ $line_num -gt 2 ]; then
		break
	fi
	
	echo Generating scripts for sample \#$(echo $line_num-1 | bc) "("$(echo $line_num-1 | bc)/$(echo $(cat "$mdt" | wc -l)-1 | bc)")"
	$python_use $script_dir/src/filer_xqtl.py --mdt "$mdt" --date_key "$my_date" --sample_line $line_num --for_filer $running_for_filer --simple_mdt $simple_mdt
	if [ $? -gt 0 ]; then
		echo "BREAKING RUN SCRIPT! Error detected."
		exit
	fi
done   

$python_use $script_dir/src/get_script_list.py "$mdt" $run_parallel $num_jobs $run_now $metadata_only $replace_metadata $simple_mdt $testing
